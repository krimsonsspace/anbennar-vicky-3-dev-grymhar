﻿
# This file is causing a ton of errors right now. It's some really cool content ideas,
# but they're all half-finished, so I'm commenting it until someone is ready to take it on 
# add_nl_urbanisation_progress = {
# 	if = {
# 		limit = {
# 			owner = {
# 				has_variable = nl_focus_gerudia
# 			}
# 			state_region = {
# 				is_homeland = cu:dalr
# 			}
# 		}
# 		nl_add_je_progress_from_building = yes
# 	}
# 	else_if = {
# 		limit = {
# 			owner = {
# 				has_variable = nl_focus_the_reach
# 			}
# 			state_region = {
# 				is_homeland = cu:blue_reachman
# 			}
# 		}
# 		nl_add_je_progress_from_building = yes
# 	}
# 	else_if = {
# 		limit = {
# 			owner = {
# 				has_variable = nl_focus_gawed
# 			}
# 			state_region = {
# 				is_homeland = cu:gawedi
# 			}
# 		}
# 		nl_add_je_progress_from_building = yes
# 	}
# }

# nl_add_je_progress_from_building = {
# 	if = {
# 		limit = {
# 			OR = {
# 				is_building_group = bg_heavy_industry
# 				is_building_group = bg_manufacturing
# 			}
# 		}
# 	}
# 	owner = {
# 		change_variable = { name = nl_construction_var add = 5 }
# 	}
# }

# nl_clear_focus = {
# 	every_scope_state = {
# 		limit = {
# 			has_modifier = nl_focused_region
# 		}
# 		remove_modifier = nl_focused_region
# 	}
	
# 	remove_variable = nl_focus_the_reach
# 	remove_variable = nl_focus_gawed
# 	remove_variable = nl_focus_gerudia
	
# 	change_variable = {
# 		name = nl_construction_var
# 		value = 0
# 	}
# }

# nl_set_focus = {
# 	if = {
# 		limit = {
# 			c:A04 ?= {
# 				has_variable = nl_focus_gerudia
# 			}
# 		}
# 		c:A04 = {
# 			nl_focus_gerudia = yes
# 		}
# 	}
# 	else_if = {
# 		limit = {
# 			c:A04 ?= {
# 				has_variable = nl_focus_the_reach
# 			}
# 		}
# 		c:A04 = {
# 			nl_focus_the_reach = yes
# 		}
# 	}
# 	else_if = {
# 		limit = {
# 			c:A04 ?= {
# 				has_variable = nl_focus_gawed
# 			}
# 		}
# 		c:A04 = {
# 			nl_focus_gawedi = yes
# 		}
# 	}
# 	else = {
# 		remove_modifier = nl_focused_region
# 	}
# }

# nl_focus_regions_effect = {
# 	every_scope_state = {
# 		nl_set_focus = yes
# 	}
# }

# nl_focus_culture = {
# 	every_scope_state = {
# 		limit = {
# 			state_region = {
# 				is_homeland = $CULTURE$
# 			}
# 		}
# 		add_modifier = {
# 			name = nl_focused_region
# 		}
# 	}
# }

# nl_set_focus_gerudia = {
# 	hidden_effect = {
# 		nl_clear_focus = yes
# 		set_variable = nl_focus_gerudia
# 		nl_focus_culture = {
# 			CULTURE = cu:dalr
# 		}
# 	}
	
# 	custom_tooltip = nl_set_focus_gerudia
# }

# nl_set_focus_the_reach = {
# 	hidden_effect = {
# 		nl_clear_focus = yes
# 		set_variable = nl_focus_the_reach
# 		nl_focus_culture = {
# 			CULTURE = cu:blue_reachman
# 		}
# 	}
# 	custom_tooltip = nl_set_focus_the_reach
# }

# nl_set_focus_gawed = {	
# 	hidden_effect = {
# 		nl_clear_focus = yes
# 		set_variable = nl_focus_gawed
# 		nl_focus_culture = {
# 			CULTURE = cu:gawedi
# 		}
# 	}
	
# 	custom_tooltip = nl_set_focus_gawed
# }

# nl_may_upset_gawedi = {
# 	if = {
# 		limit = {
# 			NOT = { has_variable = nl_urbanized_gawed }
# 		}
# 		custom_tooltip = may_upset_gawedi_tt
# 	}
# }