﻿# To ensure the game rule 'monument_effects' works properly, new monument buildings added should be given the optional base methods
# 	pm_monument_prestige_only
#	pm_monument_no_effects
# 
# 'monument_effects' additionally needs to be amended with flags disabling the new monument's Base method
# PM Groups for Monuments should have the 'is_hidden_when_unavailable' flag to ensure only one alternative is visible at any time (unless the player actually has an option they can switch to)

pmg_base_building_st_torrieths_bastion_of_the_god_fragment = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	ai_selection = most_productive
	is_hidden_when_unavailable = yes

	production_methods = {
		pm_default_building_st_torrieths_bastion_of_the_god_fragment
		pm_monument_prestige_only_st_torrieths_bastion_of_the_god_fragment
		pm_monument_no_effects
	}
}

pmg_base_building_great_tower_of_vis = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	ai_selection = most_productive
	is_hidden_when_unavailable = yes

	production_methods = {
		pm_default_building_great_tower_of_vis
		pm_monument_prestige_only
		pm_monument_no_effects
	}
}