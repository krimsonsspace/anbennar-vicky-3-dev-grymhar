BUILDINGS={
	s:STATE_NOGRUD={
		region_state:C03={
			
			create_building={
				building="building_fishing_wharf"
				level=1
				reserves=1
				activate_production_methods={ "pm_simple_fishing" "pm_unrefrigerated" "pm_merchant_guilds_building_fishing_wharf" }
			}
			create_building={
				building="building_logging_camp"
				level=2
				reserves=1
				activate_production_methods={ "pm_simple_forestry" "pm_hardwood" "pm_no_equipment" "pm_road_carts" "pm_privately_owned_building_logging_camp" }
			}
			create_building={
				building="building_cotton_plantation"
				level=1
				reserves=1
				activate_production_methods={ "default_building_cotton_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_maize_farm"
				level=2
				reserves=1
				activate_production_methods={ "pm_simple_farming" "pm_tools_disabled" "pm_privately_owned" }
			}
			create_building={
				building="building_vineyard_plantation"
				level=1
				reserves=1
				activate_production_methods={ "default_building_vineyard_plantation" "pm_privately_owned_plantation" }
			}
		}
	}

	s:STATE_YARUHOL={
		region_state:C03={
			
			create_building={
				building="building_port"
				level=1
				reserves=1
				activate_production_methods={ "pm_basic_port" }
			}
			create_building={
				building="building_paper_mills"
				level=2
				reserves=1
				activate_production_methods={ "pm_pulp_pressing" "pm_automation_disabled" "pm_privately_owned_building_paper_mills" }
			}
			create_building={
				building="building_arms_industry"
				level=1
				reserves=1
				activate_production_methods={ "pm_muskets" "pm_merchant_guilds_building_arms_industry" }
			}
			create_building={
				building="building_maize_farm"
				level=3
				reserves=1
				activate_production_methods={ "pm_simple_farming""pm_tools_disabled" "pm_privately_owned" }
			}
			create_building={
				building="building_vineyard_plantation"
				level=2
				reserves=1
				activate_production_methods={ "default_building_vineyard_plantation" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_banana_plantation"
				level=1
				reserves=1
				activate_production_methods={ "default_building_banana_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_sugar_plantation"
				level=1
				reserves=1
				activate_production_methods={ "default_building_sugar_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_coffee_plantation"
				level=3
				reserves=1
				activate_production_methods={ "default_building_coffee_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_livestock_ranch"
				level=2
				reserves=1
				activate_production_methods={ "pm_simple_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_butchering_tools" }
			}
			create_building={
				building="building_logging_camp"
				level=3
				reserves=1
				activate_production_methods={ "pm_simple_forestry" "pm_no_hardwood" "pm_no_equipment" "pm_road_carts" "pm_privately_owned_building_logging_camp" }
			}
		}
	}

	s:STATE_FASHTUG={
		region_state:C03={
			create_building={
				building="building_government_administration"
				level=3
				reserves=1
				activate_production_methods={ "pm_simple_organization" "pm_secular_bureaucrats" }
			}
			create_building={
				building="building_construction_sector"
				level=1
				reserves=1
				activate_production_methods={ "pm_wooden_buildings" }
			}
			create_building={
				building="building_university"
				level=1
				reserves=1
				activate_production_methods={ "pm_scholastic_education" }
			}
			
			create_building={
				building="building_port"
				level=2
				reserves=1
				activate_production_methods={ "pm_basic_port" }
			}
			create_building={
				building="building_naval_base"
				level=5
				reserves=1
				activate_production_methods={ "pm_no_naval_theory" }
			}
			create_building={
				building="building_shipyards"
				level=1
				reserves=1
				activate_production_methods={ "pm_basic_shipbuilding" "pm_merchant_guilds_building_shipyards"  }
			}
			create_building={
				building="building_military_shipyards"
				level=1
				reserves=1
				activate_production_methods={ "pm_military_shipbuilding_wooden" "pm_merchant_guilds_building_military_shipyards" }
			}
			create_building={
				building="building_paper_mills"
				level=1
				reserves=1
				activate_production_methods={ "pm_pulp_pressing" "pm_automation_disabled" "pm_privately_owned_building_paper_mills" }
			}
			create_building={
				building="building_tooling_workshops"
				level=1
				reserves=1
				activate_production_methods={ "pm_crude_tools" "pm_automation_disabled" "pm_privately_owned_building_tooling_workshops" }
			}
			create_building={
				building="building_furniture_manufacturies"
				level=1
				reserves=1
				activate_production_methods={ "pm_handcrafted_furniture" "pm_no_luxuries" "pm_automation_disabled" "pm_merchant_guilds_building_furniture_manufacturies" }
			}
			create_building={
				building="building_coffee_plantation"
				level=4
				reserves=1
				activate_production_methods={ "default_building_coffee_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_sugar_plantation"
				level=1
				reserves=1
				activate_production_methods={ "default_building_sugar_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_banana_plantation"
				level=3
				reserves=1
				activate_production_methods={ "default_building_banana_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_maize_farm"
				level=6
				reserves=1
				activate_production_methods={ "pm_simple_farming" "pm_no_secondary" "pm_tools_disabled" "pm_privately_owned" }
			}
			create_building={
				building="building_livestock_ranch"
				level=6
				reserves=1
				activate_production_methods={ "pm_simple_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_butchering_tools" }
			}
			create_building={
				building="building_logging_camp"
				level=4
				reserves=1
				activate_production_methods={ "pm_simple_forestry" "pm_no_hardwood" "pm_no_equipment" "pm_road_carts" "pm_privately_owned_building_logging_camp" }
			}
			create_building={
				building="building_iron_mine"
				level=1
				reserves=1
				activate_production_methods={ "pm_picks_and_shovels_building_iron_mine" "pm_no_explosives" "pm_no_steam_automation" "pm_road_carts" "pm_merchant_guilds_building_iron_mine" }
			}
			create_building={
				building="building_fishing_wharf"
				level=2
				reserves=1
				activate_production_methods={ "pm_simple_fishing" "pm_unrefrigerated" "pm_merchant_guilds_building_fishing_wharf" }
			}
		}
	}

	s:STATE_OZGAR={
		region_state:C03={
			create_building={
				building="building_government_administration"
				level=4
				reserves=1
				activate_production_methods={ "pm_simple_organization" "pm_secular_bureaucrats" }
			}
			create_building={
				building="building_construction_sector"
				level=1
				reserves=1
				activate_production_methods={ "pm_wooden_buildings" }
			}
			create_building={
				building="building_university"
				level=1
				reserves=1
				activate_production_methods={ "pm_scholastic_education" }
			}
			
			create_building={
				building="building_tooling_workshops"
				level=1
				reserves=1
				activate_production_methods={ "pm_pig_iron" "pm_automation_disabled" "pm_privately_owned_building_tooling_workshops" }
			}
			create_building={
				building="building_paper_mills"
				level=2
				reserves=1
				activate_production_methods={ "pm_pulp_pressing" "pm_automation_disabled" "pm_privately_owned_building_paper_mills" }
			}
			create_building={
				building="building_iron_mine"
				level=1
				reserves=1
				activate_production_methods={ "pm_picks_and_shovels_building_iron_mine" "pm_no_explosives" "pm_no_steam_automation" "pm_road_carts" "pm_merchant_guilds_building_iron_mine" }
			}
			create_building={
				building="building_gold_mine"
				level=1
				reserves=1
				activate_production_methods={ "pm_picks_and_shovels_building_gold_mine" "pm_no_explosives" "pm_no_steam_automation" "pm_road_carts" "pm_merchant_guilds_building_gold_mine" }
			}
			create_building={
				building="building_logging_camp"
				level=4
				reserves=1
				activate_production_methods={ "pm_simple_forestry" "pm_no_hardwood" "pm_no_equipment" "pm_road_carts" "pm_privately_owned_building_logging_camp" }
			}
			create_building={
				building="building_maize_farm"
				level=6
				reserves=1
				activate_production_methods={ "pm_simple_farming" "pm_no_secondary" "pm_tools_disabled" "pm_privately_owned" }
			}
			create_building={
				building="building_banana_plantation"
				level=3
				reserves=1
				activate_production_methods={ "default_building_banana_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_sugar_plantation"
				level=2
				reserves=1
				activate_production_methods={ "default_building_sugar_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_livestock_ranch"
				level=2
				reserves=1
				activate_production_methods={ "pm_simple_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_butchering_tools" }
			}
			create_building={
				building="building_livestock_ranch"
				level=1
				reserves=1
				activate_production_methods={ "pm_simple_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_butchering_tools" }
			}
		}
	}

	s:STATE_BRAMMYAR={
		region_state:C03={
			create_building={
				building="building_government_administration"
				level=1
				reserves=1
				activate_production_methods={ "pm_simple_organization" "pm_secular_bureaucrats" }
			}
			
			create_building={
				building="building_naval_base"
				level=5
				reserves=1
				activate_production_methods={ "pm_no_naval_theory" }
			}
			create_building={
				building="building_port"
				level=1
				reserves=1
				activate_production_methods={ "pm_basic_port" }
			}
			create_building={
				building="building_textile_mills"
				level=3
				reserves=1
				activate_production_methods={ "pm_handsewn_clothes" "pm_merchant_guilds_building_textile_mills" "pm_traditional_looms" "pm_no_luxury_clothes" }
			}
			create_building={
				building="building_logging_camp"
				level=3
				reserves=1
				activate_production_methods={ "pm_simple_forestry" "pm_no_hardwood" "pm_no_equipment" "pm_road_carts" "pm_privately_owned_building_logging_camp" }
			}
			create_building={
				building="building_fishing_wharf"
				level=3
				reserves=1
				activate_production_methods={ "pm_simple_fishing" "pm_unrefrigerated" "pm_merchant_guilds_building_fishing_wharf" }
			}
			create_building={
				building="building_maize_farm"
				level=4
				reserves=1
				activate_production_methods={ "pm_simple_farming" "pm_no_secondary" "pm_tools_disabled" "pm_privately_owned" }
			}
			create_building={
				building="building_cotton_plantation"
				level=5
				reserves=1
				activate_production_methods={ "default_building_cotton_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_tobacco_plantation"
				level=1
				reserves=1
				activate_production_methods={ "default_building_tobacco_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_banana_plantation"
				level=1
				reserves=1
				activate_production_methods={ "default_building_banana_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
		}
	}

	s:STATE_JIBIRAEN={
		region_state:C04={
			create_building={
				building="building_logging_camp"
				level=1
				reserves=1
				activate_production_methods={ "pm_simple_forestry" "pm_no_hardwood" "pm_no_equipment" "pm_road_carts" "pm_merchant_guilds_building_logging_camp" }
			}
			
			create_building={
				building="building_port"
				level=1
				reserves=1
				activate_production_methods={ "pm_basic_port" }
			}
			create_building={
				building="building_maize_farm"
				level=2
				reserves=1
				activate_production_methods={ "pm_simple_farming" "pm_no_secondary" "pm_tools_disabled" "pm_privately_owned" }
			}
			create_building={
				building="building_livestock_ranch"
				level=2
				reserves=1
				activate_production_methods={ "pm_simple_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_butchering_tools" }
			}
		}
	}

	s:STATE_MARUKHAN={
		region_state:C03={
			
			create_building={
				building="building_logging_camp"
				level=1
				reserves=1
				activate_production_methods={ "pm_simple_forestry" "pm_no_hardwood" "pm_no_equipment" "pm_road_carts" "pm_privately_owned_building_logging_camp" }
			}
			create_building={
				building="building_banana_plantation"
				level=1
				reserves=1
				activate_production_methods={ "default_building_banana_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_rice_farm"
				level=2
				reserves=1
				activate_production_methods={ "pm_simple_farming_building_rice_farm" "pm_no_secondary" "pm_tools_disabled" "pm_privately_owned" }
			}
		}
	}

	s:STATE_THE_MIDDANS={
		region_state:A08={
			# create_building={
			# 	building="building_sugar_plantation"
			# 	level=1
			# 	reserves=1
			# 	activate_production_methods={ "default_building_sugar_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			# }
		}
		region_state:C06={
			
		}
	}
	
	s:STATE_MUSHROOM_FOREST = {
		region_state:C16 = {
			create_building={
				building="building_opium_plantation"
				level=1
				reserves=1
				activate_production_methods={ "default_building_opium_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			
		}
	}
	
	s:STATE_SORFEN = {
		region_state:C06 = {
			create_building={
				building="building_rice_farm"
				level=1
				reserves=1
				activate_production_methods={ "pm_simple_farming_building_rice_farm" "pm_no_secondary" "pm_tools_disabled" "pm_privately_owned" }
			}
			create_building={
				building="building_logging_camp"
				level=1
				reserves=1
				activate_production_methods={ "pm_simple_forestry" "pm_no_hardwood" "pm_no_equipment" "pm_road_carts" "pm_privately_owned_building_logging_camp" }
			}
			
			create_building={
				building="building_port"
				level=1
				reserves=1
				activate_production_methods={ "pm_basic_port" }
			}
		}
	}
	
	s:STATE_SEINAINE = {
		region_state:C05 = {
			create_building={
				building="building_banana_plantation"
				level=3
				reserves=1
				activate_production_methods={ "default_building_banana_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_maize_farm"
				level=8
				reserves=1
				activate_production_methods={ "pm_simple_farming" "pm_citrus_orchards" "pm_tools_disabled" "pm_privately_owned" }
			}
			create_building={
				building="building_livestock_ranch"
				level=1
				reserves=1
				activate_production_methods={ "pm_simple_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_butchering_tools" }
			}
			create_building={
				building="building_sugar_plantation"
				level=3
				reserves=1
				activate_production_methods={ "default_building_sugar_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_logging_camp"
				level=1
				reserves=1
				activate_production_methods={ "pm_simple_forestry" "pm_hardwood" "pm_no_equipment" "pm_road_carts" "pm_privately_owned_building_logging_camp" }
			}
			create_building={
				building="building_fishing_wharf"
				level=1
				reserves=1
				activate_production_methods={ "pm_simple_fishing" "pm_unrefrigerated" "pm_merchant_guilds_building_fishing_wharf" }
			}
			create_building={
				building="building_arms_industry"
				level=1
				reserves=1
				activate_production_methods={ "pm_muskets" "pm_merchant_guilds_building_arms_industry"}
			}
			create_building={
				building="building_government_administration"
				level=1
				reserves=1
				activate_production_methods={ "pm_simple_organization" "pm_secular_bureaucrats" }
			}
			
		}
		region_state:C06 = {
			create_building={
				building="building_sugar_plantation"
				level=1
				reserves=1
				activate_production_methods={ "default_building_sugar_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_fishing_wharf"
				level=1
				reserves=1
				activate_production_methods={ "pm_simple_fishing" "pm_unrefrigerated" "pm_merchant_guilds_building_fishing_wharf" }
			}
		}
		region_state:A01 = {
			create_building={
				building="building_sugar_plantation"
				level=1
				reserves=1
				activate_production_methods={ "default_building_sugar_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
		}
	}
	
	s:STATE_DALAINE = {
		region_state:C05 = {
			create_building={
				building="building_banana_plantation"
				level=3
				reserves=1
				activate_production_methods={ "default_building_banana_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_maize_farm"
				level=8
				reserves=1
				activate_production_methods={ "pm_simple_farming" "pm_citrus_orchards" "pm_tools_disabled" "pm_privately_owned" }
			}
			create_building={
				building="building_sugar_plantation"
				level=2
				reserves=1
				activate_production_methods={ "default_building_sugar_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_coffee_plantation"
				level=1
				reserves=1
				activate_production_methods={ "default_building_coffee_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_iron_mine"
				level=1
				reserves=1
				activate_production_methods={ "pm_picks_and_shovels_building_iron_mine" "pm_no_explosives" "pm_no_steam_automation" "pm_road_carts" "pm_merchant_guilds_building_iron_mine" }
			}
			create_building={
				building="building_government_administration"
				level=1
				reserves=1
				activate_production_methods={ "pm_simple_organization" "pm_secular_bureaucrats" }
			}
			
			create_building={
				building="building_port"
				level=1
				reserves=1
				activate_production_methods={ "pm_basic_port" }
			}
			create_building={
				building="building_furniture_manufacturies"
				level=1
				reserves=1
				activate_production_methods={ "pm_handcrafted_furniture" "pm_no_luxuries" "pm_automation_disabled" "pm_merchant_guilds_building_furniture_manufacturies" }
			}
			create_building={
				building="building_tooling_workshops"
				level=1
				reserves=1
				activate_production_methods={ "pm_pig_iron" "pm_automation_disabled" "pm_privately_owned_building_tooling_workshops" }
			}
			create_building={
				building="building_logging_camp"
				level=1
				reserves=1
				activate_production_methods={ "pm_simple_forestry" "pm_no_hardwood" "pm_no_equipment" "pm_road_carts" "pm_privately_owned_building_logging_camp" }
			}
		}
	}
	
	s:STATE_THALASARAN = {
		region_state:C08 = {
			create_building={
				building="building_sugar_plantation"
				level=1
				reserves=1
				activate_production_methods={ "default_building_sugar_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_coffee_plantation"
				level=1
				reserves=1
				activate_production_methods={ "default_building_coffee_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			
		}
		region_state:A03 = {
			create_building={
				building="building_sugar_plantation"
				level=1
				reserves=1
				activate_production_methods={ "default_building_sugar_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_coffee_plantation"
				level=1
				reserves=1
				activate_production_methods={ "default_building_coffee_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
		}
	}
	
	s:STATE_KIOHALEN = {
		region_state:C07 = {
			create_building={
				building="building_maize_farm"
				level=2
				reserves=1
				activate_production_methods={ "pm_simple_farming" "pm_citrus_orchards" "pm_tools_disabled" "pm_privately_owned" }
			}
			create_building={
				building="building_cotton_plantation"
				level=1
				reserves=1
				activate_production_methods={ "default_building_cotton_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_tobacco_plantation"
				level=2
				reserves=1
				activate_production_methods={ "default_building_tobacco_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_sugar_plantation"
				level=1
				reserves=1
				activate_production_methods={ "default_building_sugar_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_dye_plantation"
				level=2
				reserves=1
				activate_production_methods={ "default_building_dye_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_government_administration"
				level=1
				reserves=1
				activate_production_methods={ "pm_simple_organization" "pm_secular_bureaucrats" }
			}
			create_building={
				building="building_paper_mills"
				level=1
				reserves=1
				activate_production_methods={ "pm_pulp_pressing" "pm_automation_disabled" "pm_privately_owned_building_paper_mills" }
			}
			
			create_building={
				building="building_arts_academy"
				level=1
				reserves=1
				activate_production_methods={ "pm_traditional_art" }
			}
		}
	}
	
	s:STATE_KIINDTIR = {
		region_state:C07 = {
			create_building={
				building="building_government_administration"
				level=1
				reserves=1
				activate_production_methods={ "pm_simple_organization" "pm_secular_bureaucrats" }
			}
			
			create_building={
				building="building_arts_academy"
				level=1
				reserves=1
				activate_production_methods={ "pm_traditional_art" }
			}
			# create_building={
			# 	building="building_wheat_farm"
			# 	level=1
			# 	reserves=1
			# 	activate_production_methods={ "pm_simple_farming" "pm_citrus_orchards" "pm_tools_disabled" "pm_privately_owned" }
			# }
			create_building={
				building="building_logging_camp"
				level=1
				reserves=1
				activate_production_methods={ "pm_simple_forestry" "pm_no_hardwood" "pm_no_equipment" "pm_road_carts" "pm_privately_owned_building_logging_camp" }
			}
		}
	}
	
	s:STATE_DAZINKOST = {
		region_state:C01 = {
			create_building={
				building="building_livestock_ranch"
				level=1
				reserves=1
				activate_production_methods={ "pm_simple_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_butchering_tools" }
			}
			create_building={
				building="building_iron_mine"
				level=1
				reserves=1
				activate_production_methods={ "pm_picks_and_shovels_building_iron_mine" "pm_no_explosives" "pm_no_steam_automation" "pm_road_carts" "pm_merchant_guilds_building_iron_mine" }
			}
			create_building={
				building="building_fishing_wharf"
				level=1
				reserves=1
				activate_production_methods={ "pm_simple_fishing" "pm_unrefrigerated" "pm_merchant_guilds_building_fishing_wharf" }
			}
			
			create_building={
				building="building_naval_base"
				level=2
				reserves=1
				activate_production_methods={ "pm_no_naval_theory" }
			}
		}
	}
	
	s:STATE_REZANOAN = {
		region_state:C01 = {
			create_building={
				building="building_livestock_ranch"
				level=1
				reserves=1
				activate_production_methods={ "pm_simple_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_butchering_tools" }
			}
			create_building={
				building="building_wheat_farm"
				level=3
				reserves=1
				activate_production_methods={ "pm_simple_farming" "pm_citrus_orchards" "pm_tools_disabled" "pm_privately_owned" }
			}
			create_building={
				building="building_fishing_wharf"
				level=1
				reserves=1
				activate_production_methods={ "pm_simple_fishing" "pm_unrefrigerated" "pm_merchant_guilds_building_fishing_wharf" }
			}
			create_building={
				building="building_glassworks"
				level=1
				reserves=1
				activate_production_methods={ "pm_forest_glass"  "pm_disabled_ceramics" "pm_manual_glassblowing" "pm_privately_owned_building_glassworks" }
			}
			create_building={
				building="building_paper_mills"
				level=1
				reserves=1
				activate_production_methods={ "pm_pulp_pressing" "pm_automation_disabled" "pm_privately_owned_building_paper_mills" }
			}
			create_building={
				building="building_tooling_workshops"
				level=1
				reserves=1
				activate_production_methods={ "pm_pig_iron" "pm_automation_disabled" "pm_privately_owned_building_tooling_workshops" }
			}
			create_building={
				building="building_furniture_manufacturies"
				level=1
				reserves=1
				activate_production_methods={ "pm_handcrafted_furniture" "pm_no_luxuries" "pm_automation_disabled" "pm_merchant_guilds_building_furniture_manufacturies" }
			}
			create_building={
				building="building_military_shipyards"
				level=1
				reserves=1
				activate_production_methods={ "pm_military_shipbuilding_wooden" "pm_merchant_guilds_building_military_shipyards" }
			}
			create_building={
				building="building_university"
				level=2
				reserves=1
				activate_production_methods={ "pm_scholastic_education" }
			}
			create_building={
				building="building_government_administration"
				level=2
				reserves=1
				activate_production_methods={ "pm_simple_organization" "pm_secular_bureaucrats" }
			}
			
			create_building={
				building="building_naval_base"
				level=5
				reserves=1
				activate_production_methods={ "pm_no_naval_theory" }
			}
			create_building={
				building="building_port"
				level=1
				reserves=1
				activate_production_methods={ "pm_basic_port" }
			}
			create_building={
				building="building_construction_sector"
				level=1
				reserves=1
				activate_production_methods={ "pm_wooden_buildings" }
			}
		}
	}
	
	s:STATE_BROAN_KEIR = {
		region_state:C01 = {
			create_building={
				building="building_livestock_ranch"
				level=3
				reserves=1
				activate_production_methods={ "pm_simple_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_butchering_tools" }
			}
			create_building={
				building="building_wheat_farm"
				level=1
				reserves=1
				activate_production_methods={ "pm_simple_farming" "pm_citrus_orchards" "pm_tools_disabled" "pm_privately_owned" }
			}
			create_building={
				building="building_arms_industry"
				level=1
				reserves=1
				activate_production_methods={ "pm_muskets" "pm_merchant_guilds_building_arms_industry" }
			}
			create_building={
				building="building_arts_academy"
				level=1
				reserves=1
				activate_production_methods={ "pm_traditional_art" }
			}
			create_building={
				building="building_government_administration"
				level=1
				reserves=1
				activate_production_methods={ "pm_simple_organization" "pm_secular_bureaucrats" }
			}
			
			create_building={
				building="building_naval_base"
				level=3
				reserves=1
				activate_production_methods={ "pm_no_naval_theory" }
			}
		}
	}
	
	s:STATE_NUREL = {
		region_state:C01 = {
			create_building={
				building="building_wheat_farm"
				level=4
				reserves=1
				activate_production_methods={ "pm_simple_farming" "pm_citrus_orchards" "pm_tools_disabled" "pm_privately_owned" }
			}
			create_building={
				building="building_sugar_plantation"
				level=1
				reserves=1
				activate_production_methods={ "default_building_sugar_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_cotton_plantation"
				level=1
				reserves=1
				activate_production_methods={ "default_building_cotton_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_logging_camp"
				level=2
				reserves=1
				activate_production_methods={ "pm_simple_forestry" "pm_hardwood" "pm_no_equipment" "pm_road_carts" "pm_privately_owned_building_logging_camp" }
			}
			# create_building={
			# 	building="building_iron_mine"
			# 	level=1
			# 	reserves=1
			# 	activate_production_methods={ "pm_picks_and_shovels_building_iron_mine" "pm_no_explosives" "pm_no_steam_automation" "pm_road_carts" "pm_merchant_guilds_building_iron_mine" }
			# }
			create_building={
				building="building_shipyards"
				level=1
				reserves=1
				activate_production_methods={ "pm_basic_shipbuilding" "pm_merchant_guilds_building_shipyards"  }
			}
			
			create_building={
				building="building_naval_base"
				level=2
				reserves=1
				activate_production_methods={ "pm_no_naval_theory" }
			}
		}
	}
	
	s:STATE_ARENEL = {
		region_state:C01 = {
			# create_building={
			# 	building="building_maize_farm"
			# 	level=2
			# 	reserves=1
			# 	activate_production_methods={ "pm_simple_farming" "pm_citrus_orchards" "pm_tools_disabled" "pm_privately_owned" }
			# }
			create_building={
				building="building_sugar_plantation"
				level=1
				reserves=1
				activate_production_methods={ "default_building_sugar_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_fishing_wharf"
				level=1
				reserves=1
				activate_production_methods={ "pm_simple_fishing" "pm_unrefrigerated" "pm_merchant_guilds_building_fishing_wharf" }
			}
			create_building={
				building="building_logging_camp"
				level=2
				reserves=1
				activate_production_methods={ "pm_simple_forestry" "pm_hardwood" "pm_no_equipment" "pm_road_carts" "pm_privately_owned_building_logging_camp" }
			}
			create_building={
				building="building_iron_mine"
				level=1
				reserves=1
				activate_production_methods={ "pm_picks_and_shovels_building_iron_mine" "pm_no_explosives" "pm_no_steam_automation" "pm_road_carts" "pm_merchant_guilds_building_iron_mine" }
			}
			create_building={
				building="building_textile_mills"
				level=1
				reserves=1
				activate_production_methods={ "pm_handsewn_clothes" "pm_merchant_guilds_building_textile_mills" "pm_traditional_looms" "pm_no_luxury_clothes" }
			}
			
			create_building={
				building="building_naval_base"
				level=2
				reserves=1
				activate_production_methods={ "pm_no_naval_theory" }
			}
			create_building={
				building="building_port"
				level=1
				reserves=1
				activate_production_methods={ "pm_basic_port" }
			}
			create_building={
				building="building_construction_sector"
				level=1
				reserves=1
				activate_production_methods={ "pm_wooden_buildings" }
			}
		}
	}
	
	s:STATE_NUR_ELIZNA = {
		region_state:C01 = {
			# create_building={
			# 	building="building_fishing_wharf"
			# 	level=1
			# 	reserves=1
			# 	activate_production_methods={ "pm_simple_fishing" "pm_unrefrigerated" "pm_merchant_guilds_building_fishing_wharf" }
			# }
			create_building={
				building="building_vineyard_plantation"
				level=1
				reserves=1
				activate_production_methods={ "default_building_vineyard_plantation" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_naval_base"
				level=4
				reserves=1
				activate_production_methods={ "pm_no_naval_theory" }
			}
		}
	}
	
	s:STATE_NEWSHORE = {
		region_state:C10 = {
			create_building={
				building="building_logging_camp"
				level=1
				reserves=1
				activate_production_methods={ "pm_simple_forestry" "pm_hardwood" "pm_no_equipment" "pm_road_carts" "pm_privately_owned_building_logging_camp" }
			}
			create_building={
				building="building_vineyard_plantation"
				level=1
				reserves=1
				activate_production_methods={ "default_building_vineyard_plantation" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_government_administration"
				level=1
				reserves=1
				activate_production_methods={ "pm_simple_organization" "pm_secular_bureaucrats" }
			}
			create_building={
				building="building_port"
				level=1
				reserves=1
				activate_production_methods={ "pm_basic_port" }
			}
			create_building={
				building="building_whaling_station"
				level=1
				reserves=1
				activate_production_methods={ "pm_wooden_whaling_ships" "pm_unrefrigerated" "pm_merchant_guilds_building_whaling_station" }
			}
			
		}
	}
	
	s:STATE_TIMBERNECK = {
		region_state:C10 = {
			create_building={
				building="building_logging_camp"
				level=4
				reserves=1
				activate_production_methods={ "pm_simple_forestry" "pm_no_hardwood" "pm_no_equipment" "pm_road_carts" "pm_privately_owned_building_logging_camp" }
			}
			create_building={
				building="building_whaling_station"
				level=1
				reserves=1
				activate_production_methods={ "pm_wooden_whaling_ships" "pm_unrefrigerated" "pm_merchant_guilds_building_whaling_station" }
			}
			
		}
	}
	
	s:STATE_VRENDIN = {
		region_state:C10 = {
			create_building={
				building="building_logging_camp"
				level=1
				reserves=1
				activate_production_methods={ "pm_simple_forestry" "pm_hardwood" "pm_no_equipment" "pm_road_carts" "pm_privately_owned_building_logging_camp" }
			}
			create_building={
				building="building_whaling_station"
				level=2
				reserves=1
				activate_production_methods={ "pm_wooden_whaling_ships" "pm_unrefrigerated" "pm_merchant_guilds_building_whaling_station" }
			}
			
		}
		region_state:A01 = {
			create_building={
				building="building_whaling_station"
				level=1
				reserves=1
				activate_production_methods={ "pm_wooden_whaling_ships" "pm_unrefrigerated" "pm_merchant_guilds_building_whaling_station" }
			}
		}
	}
	
	s:STATE_LAIPOINT_ARCHIPELAGO = { #Empty by design
		region_state:A01 = {
			
		}
		region_state:A08 = {
			
		}
	}
	
	s:STATE_WEST_TURTLEBACK = {
		region_state:C09 = {
			create_building={
				building="building_rice_farm"
				level=3
				reserves=1
				activate_production_methods={ "pm_simple_farming_building_rice_farm" "pm_no_secondary" "pm_tools_disabled" "pm_privately_owned" }
			}
			create_building={
				building="building_banana_plantation"
				level=1
				reserves=1
				activate_production_methods={ "default_building_banana_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_sugar_plantation"
				level=1
				reserves=1
				activate_production_methods={ "default_building_sugar_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_fishing_wharf"
				level=1
				reserves=1
				activate_production_methods={ "pm_simple_fishing" "pm_unrefrigerated" "pm_merchant_guilds_building_fishing_wharf" }
			}
			create_building={
				building="building_food_industry"
				level=1
				reserves=1
				activate_production_methods={ "pm_disabled_canning" "pm_bakery" "pm_manual_dough_processing" "pm_disabled_distillery" }
			}
			
			create_building={
				building="building_port"
				level=1
				reserves=1
				activate_production_methods={ "pm_basic_port" }
			}
		}
	}
	
	s:STATE_EAST_TURTLEBACK = {
		region_state:C09 = {
			create_building={
				building="building_rice_farm"
				level=3
				reserves=1
				activate_production_methods={ "pm_simple_farming_building_rice_farm" "pm_no_secondary" "pm_tools_disabled" "pm_privately_owned" }
			}
			create_building={
				building="building_coffee_plantation"
				level=1
				reserves=1
				activate_production_methods={ "default_building_coffee_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_fishing_wharf"
				level=1
				reserves=1
				activate_production_methods={ "pm_simple_fishing" "pm_unrefrigerated" "pm_merchant_guilds_building_fishing_wharf" }
			}
			create_building={
				building="building_food_industry"
				level=1
				reserves=1
				activate_production_methods={ "pm_disabled_canning" "pm_bakery" "pm_manual_dough_processing" "pm_disabled_distillery" }
			}
			
			create_building={
				building="building_port"
				level=1
				reserves=1
				activate_production_methods={ "pm_basic_port" }
			}
		}
	}
}