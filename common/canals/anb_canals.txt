﻿
canal_prikoyol = {
	texture = "gfx/interface/icons/building_icons/panama_canal.dds"

	possible = {
		
	owns_treaty_port_in = STATE_PRIKOYOL

		#custom_tooltip = {
		#	text = panama_survey_complete_tooltip
		#	has_variable = panama_survey_complete
		#}
	}

	state_region = STATE_PRIKOYOL
}

canal_dhal_nikhuvad = {
	texture = "gfx/interface/icons/building_icons/panama_canal.dds"

	possible = {
		
		has_technology_researched = colonization
 		owns_treaty_port_in = STATE_DHAL_NIKHUVAD
		custom_tooltip = {
			text = dhal_nikhuvad_survey_complete_tooltip
			has_variable = dhal_nikhuvad_survey_complete
		}
	}

	state_region = STATE_DHAL_NIKHUVAD
}
