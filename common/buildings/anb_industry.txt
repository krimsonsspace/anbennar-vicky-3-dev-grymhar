﻿building_magical_reagents_workshop = {
	building_group = bg_magical_reagents_workshop
	texture = "gfx/interface/icons/building_icons/magical_reagents_workshop.dds"
	city_type = city
	levels_per_mesh = 5
	
	unlocking_technologies = {
		fractional_distillation
	}

	production_method_groups = {
		pmg_acquisition_magical_reagents_workshop
        pmg_treatments_magical_reagents_workshop
		pmg_storage_magical_reagents_workshop
		pmg_ownership_capital_building_magical_reagents_workshop
	}

	required_construction = construction_cost_high
}

building_skyship_yards = {
	building_group = bg_skyship_yards
	texture = "gfx/interface/icons/building_icons/skyship_yards.dds"
	city_type = city
	levels_per_mesh = 5
	
	unlocking_technologies = {
		atmospheric_engine
	}

	production_method_groups = {
		pmg_base_building_skyship_yards
        pmg_military_base_building_skyship_yards
		pmg_ownership_capital_building_skyship_yards
	}

	required_construction = construction_cost_high
}

building_damesoil_refinery = {
	building_group = bg_damesoil_refinery
	texture = "gfx/interface/icons/building_icons/damesoil_refinery.dds"
	city_type = city
	levels_per_mesh = 5

	unlocking_technologies = {
		essence_extraction
	}

	production_method_groups = {
		pmg_base_building_damesoil_refinery
		pmg_storage_building_damesoil_refinery
		pmg_transmutation_building_damesoil_refinery
		pmg_ownership_capital_building_damesoil_refinery
	}

	required_construction = construction_cost_very_high
}

building_blueblood_plants = {
	building_group = bg_blueblood_plants
	texture = "gfx/interface/icons/building_icons/blueblood_plants.dds"
	city_type = city
	levels_per_mesh = 5

	unlocking_technologies = {
		pharmaceuticals
	}

	production_method_groups = {
		pmg_collection_procedure_building_blueblood_plants
		pmg_patient_acquisition_building_blueblood_plants
		pmg_refrigeration_building_blueblood_plants
		pmg_ownership_capital_building_blueblood_plants
	}

	required_construction = construction_cost_high
}

building_doodad_manufacturies = {
	building_group = bg_doodad_manufacturies
	texture = "gfx/interface/icons/building_icons/doodad_manufacturies.dds"
	city_type = city
	levels_per_mesh = 5

	unlocking_technologies = {
		punch_card_artificery
	}

	production_method_groups = {
		pmg_power_supply_building_doodad_manufacturies
		pmg_casing_building_doodad_manufacturies
		pmg_wiring_building_doodad_manufacturies
		pmg_automation_building_doodad_manufacturies
		pmg_ownership_capital_building_doodad_manufacturies
	}

	required_construction = construction_cost_very_high
}

building_automatories = {
	building_group = bg_automatories
	texture = "gfx/interface/icons/building_icons/doodad_manufacturies.dds"
	city_type = city
	levels_per_mesh = 5

	unlocking_technologies = {
		insyaan_analytical_engine
	}

	production_method_groups = {
		pmg_processor_building_automatories
		pmg_power_source_building_automatories
		pmg_frame_building_automatories
		pmg_automation_building_automatories
		pmg_ownership_capital_building_automatories
	}

	required_construction = construction_cost_very_high
}