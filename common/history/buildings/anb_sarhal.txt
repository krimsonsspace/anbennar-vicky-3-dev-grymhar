﻿BUILDINGS={
	s:STATE_DESHAK={
		region_state:A11={
			create_building={
				building="building_barracks"
				level=20
				reserves=1
			}
			# create_building={
			# 	building="building_iron_mine"
			# 	level=1
			# 	reserves=1
			# }
			create_building={
				building="building_wheat_farm"
				level=2
				reserves=1
				activate_production_methods={ "pm_privately_owned" "pm_simple_farming" "pm_tools_disabled" }
			}
			create_building={
				building="building_livestock_ranch"
				level=2
				reserves=1
				activate_production_methods={ "pm_privately_owned" "pm_intensive_grazing_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_slaughterhouses" }
			}
			#To connect the markets
			create_building={
				building="building_port"
				level=1
				reserves=1
				activate_production_methods={ "pm_basic_port" }
			}
			create_building={
				building="building_naval_base"
				level=5
				reserves=1
				activate_production_methods={ "pm_no_naval_theory" }
			}
		}
	}

	s:STATE_AKASIK={
		region_state:A11={
			create_building={
				building="building_iron_mine"
				level=4
				reserves=1
				activate_production_methods={ "pm_atmospheric_engine_pump_building_iron_mine" "pm_no_explosives" "pm_no_steam_automation" "pm_road_carts" "pm_privately_owned_building_iron_mine" }
			}
			create_building={
				building="building_wheat_farm"
				level=1
				reserves=1
				activate_production_methods={ "pm_privately_owned" "pm_simple_farming" "pm_tools_disabled" }
			}
		}
	}

	s:STATE_KHASA={
		region_state:A11={
			#PAPER
			create_building={
				building="building_paper_mills"
				level=2
				reserves=1
			}
			#BREADBASKET - GRAIN
			create_building={
				building="building_wheat_farm"
				level=6
				reserves=1
				activate_production_methods={ "pm_privately_owned" "pm_simple_farming" "pm_tools_disabled" }
			}
			#breadbasket - meat
			create_building={
				building="building_livestock_ranch"
				level=2
				reserves=1
				activate_production_methods={ "pm_privately_owned" "pm_intensive_grazing_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_slaughterhouses" }
			}
		}
	}

	s:STATE_EKHA={
		region_state:A13={
			create_building={
				building="building_barracks"
				level=10
				reserves=1
			}
			create_building={
				building="building_sugar_plantation"
				level=4
				reserves=1
				activate_production_methods={ "default_building_sugar_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_shipyards"
				level=1
				reserves=1
				activate_production_methods={ "pm_basic_shipbuilding" "pm_merchant_guilds_building_shipyards"  }
			}
			create_building={
				building="building_naval_base"
				level=5
				reserves=1
			}
		}
	}

	s:STATE_DHAL_NIKHUVAD={
		region_state:L05={
			create_building={
				building="building_barracks"
				level=1
				reserves=1
			}
			# create_building={
			# 	building="building_iron_mine"
			# 	level=1
			# 	reserves=1
			# }
			create_building={
				building="building_millet_farm"
				level=1
				reserves=1
				activate_production_methods={ "pm_privately_owned" "pm_simple_farming" "pm_tools_disabled" }
			}
			create_building={
				building="building_port"
				level=1
				reserves=1
				activate_production_methods={ "pm_basic_port" }
			}
		}
	}

	s:STATE_SUHRATBA_YAJ={
		region_state:L05={
			create_building={
				building="building_barracks"
				level=1
				reserves=1
			}
			# create_building={
			# 	building="building_iron_mine"
			# 	level=1
			# 	reserves=1
			# }
			create_building={
				building="building_millet_farm"
				level=1
				reserves=1
				activate_production_methods={ "pm_privately_owned" "pm_simple_farming" "pm_tools_disabled" }
			}
			create_building={
				building="building_port"
				level=1
				reserves=1
				activate_production_methods={ "pm_basic_port" }
			}
		}
	}

	s:STATE_QASRIYINGI={
		region_state:L05={
			create_building={
				building="building_barracks"
				level=1
				reserves=1
			}
			# create_building={
			# 	building="building_iron_mine"
			# 	level=1
			# 	reserves=1
			# }
			create_building={
				building="building_millet_farm"
				level=1
				reserves=1
				activate_production_methods={ "pm_privately_owned" "pm_simple_farming" "pm_tools_disabled" }
			}
			create_building={
				building="building_port"
				level=1
				reserves=1
				activate_production_methods={ "pm_basic_port" }
			}
		}
	}

	s:STATE_DHEBIJ_JANAB={
		region_state:L05={
			create_building={
				building="building_barracks"
				level=1
				reserves=1
			}
			# create_building={
			# 	building="building_iron_mine"
			# 	level=1
			# 	reserves=1
			# }
			create_building={
				building="building_millet_farm"
				level=1
				reserves=1
				activate_production_methods={ "pm_privately_owned" "pm_simple_farming" "pm_tools_disabled" }
			}
			create_building={
				building="building_port"
				level=1
				reserves=1
				activate_production_methods={ "pm_basic_port" }
			}
		}
	}

	s:STATE_BAASHI_BADDA={
		region_state:L05={
			create_building={
				building="building_barracks"
				level=1
				reserves=1
			}
			# create_building={
			# 	building="building_iron_mine"
			# 	level=1
			# 	reserves=1
			# }
			create_building={
				building="building_millet_farm"
				level=1
				reserves=1
				activate_production_methods={ "pm_privately_owned" "pm_simple_farming" "pm_tools_disabled" }
			}
			create_building={
				building="building_port"
				level=1
				reserves=1
				activate_production_methods={ "pm_basic_port" }
			}
		}
	}

	s:STATE_SUHRATBA_YAHIN={
		region_state:L05={
			create_building={
				building="building_barracks"
				level=1
				reserves=1
			}
			# create_building={
			# 	building="building_iron_mine"
			# 	level=1
			# 	reserves=1
			# }
			create_building={
				building="building_millet_farm"
				level=1
				reserves=1
				activate_production_methods={ "pm_privately_owned" "pm_simple_farming" "pm_tools_disabled" }
			}
			create_building={
				building="building_port"
				level=1
				reserves=1
				activate_production_methods={ "pm_basic_port" }
			}
		}
	}

	s:STATE_DHAL_TANIZUUD={
		region_state:L06={
			create_building={
				building="building_barracks"
				level=1
				reserves=1
			}
			# create_building={
			# 	building="building_iron_mine"
			# 	level=1
			# 	reserves=1
			# }
			create_building={
				building="building_millet_farm"
				level=1
				reserves=1
				activate_production_methods={ "pm_privately_owned" "pm_simple_farming" "pm_tools_disabled" }
			}
			create_building={
				building="building_port"
				level=1
				reserves=1
				activate_production_methods={ "pm_basic_port" }
			}
		}
	}
}