﻿COUNTRIES = {
	c:B05 = {
		effect_starting_technology_tier_1_tech = yes
		effect_starting_artificery_tier_2_tech = yes

		effect_starting_politics_conservative = yes
		
		activate_law = law_type:law_presidential_republic
		activate_law = law_type:law_oligarchy
		activate_law = law_type:law_racial_segregation
		activate_law = law_type:law_freedom_of_conscience
		activate_law = law_type:law_appointed_bureaucrats
		activate_law = law_type:law_professional_army
		activate_law = law_type:law_no_home_affairs
		
		activate_law = law_type:law_interventionism
		activate_law = law_type:law_mercantilism
		activate_law = law_type:law_per_capita_based_taxation
		activate_law = law_type:law_no_colonial_affairs
		activate_law = law_type:law_no_police
		activate_law = law_type:law_private_schools
		activate_law = law_type:law_no_health_system
		
		activate_law = law_type:law_right_of_assembly
		activate_law = law_type:law_tenant_farmers
		activate_law = law_type:law_child_labor_allowed
		activate_law = law_type:law_women_own_property
		activate_law = law_type:law_poor_laws
		activate_law = law_type:law_no_migration_controls
		activate_law = law_type:law_legacy_slavery
		
		activate_law = law_type:law_non_monstrous_only
		activate_law = law_type:law_dark_arts_banned
		activate_law = law_type:law_pragmatic_artifice
		activate_law = law_type:law_artifice_encouraged

		ig:ig_industrialists = {
			add_ruling_interest_group = yes
		}
		
		# set_institution_investment_level = {
		# 	institution = institution_colonial_affairs
		# 	level = 2
		# }
		set_institution_investment_level = {
			institution = institution_schools
			level = 1
		}
	}
}