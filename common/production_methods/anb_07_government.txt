﻿
## Ports
pm_self_regulating_megaport = {
	texture = "gfx/interface/icons/production_method_icons/modern_port.dds"
	unlocking_technologies = {
		concrete_dockyards
	}
	
	building_modifiers = {
		workforce_scaled = {	
			goods_input_steamers_add = 10	
			goods_input_oil_add = 5
			goods_input_automata_add = 4
		}
		level_scaled = {
			building_employment_machinists_add = 500
			building_employment_bureaucrats_add = 500
			building_employment_engineers_add = 750
		}
	}

	country_modifiers = {
		workforce_scaled = {
			country_convoys_capacity_add = 200
		}
	}
	state_modifiers = {
		workforce_scaled = {
			state_infrastructure_add = 10
		}
	}
}	

## Government Administrations
# pm_sending_stone_communications = {
# 	texture = "gfx/interface/icons/production_method_icons/vertical_filing_cabinets.dds"
# 	unlocking_technologies = {
# 		central_archives
# 	}
	
# 	country_modifiers = {
# 		workforce_scaled = {
# 			country_bureaucracy_add = 65
# 		}
# 	}	
	
# 	state_modifiers = {
# 		workforce_scaled = {
# 			state_tax_capacity_add = 15
# 		}		
# 	}	

# 	building_modifiers = {
# 		workforce_scaled = {
# 			goods_input_paper_add = 15
# 			goods_input_telephones_add = 3
# 		}	
	
# 		level_scaled = {
# 			building_employment_bureaucrats_add = 1000
# 			building_employment_clerks_add = 3000
# 		}
# 	}	
# }

pm_apparitional_communications = {
	texture = "gfx/interface/icons/production_method_icons/telephone_switchboards.dds"
	unlocking_technologies = {
		central_planning
	}
	
	country_modifiers = {
		workforce_scaled = {
			country_bureaucracy_add = 140
		}
	}	
	
	state_modifiers = {
		workforce_scaled = {
			state_tax_capacity_add = 45
		}		
	}	

	building_modifiers = {
		workforce_scaled = {
			goods_input_paper_add = 35
			goods_input_telephones_add = 10
			#goods_input_radios_add = 5
		}	
	
		level_scaled = {
			building_employment_clerks_add = 1250
			building_employment_machinists_add = 1250
			building_employment_bureaucrats_add = 2500
		}
	}	
}

pm_divined_tax_returns = {
	texture = "gfx/interface/icons/production_method_icons/ownership_academics.dds"

	disallowing_laws = {
		law_artifice_only
		law_mundane_production
	}

	state_modifiers = {
		workforce_scaled = {
			state_tax_capacity_add = 10
		}		
	}	

	building_modifiers = {
		workforce_scaled = {
			goods_input_magical_reagents_add = 1
		}

		level_scaled = {
			building_employment_mages_add = 250
		}
	}		
}

pm_automated_translators = {
	texture = "gfx/interface/icons/production_method_icons/ownership_academics.dds"

	unlocking_technologies = {
		automated_translators
	}

	country_modifiers = {
		workforce_scaled = {
			country_influence_add = 0.01
		}
	}

	state_modifiers = {
		workforce_scaled = {
			state_tax_capacity_add = 10
		}		
	}	

	building_modifiers = {
		workforce_scaled = {
			goods_input_artificery_doodads_add = 5
		}
		
		level_scaled = {
			building_employment_clerks_add = 250
			building_employment_machinists_add = 250
		}
	}		
}

pm_magical_bureaucrats = {
	texture = "gfx/interface/icons/production_method_icons/ownership_bureacrats.dds"

	disallowing_laws = {
		law_state_religion
		law_state_atheism
	}

	building_modifiers = {
		level_scaled = {
			building_employment_clergymen_add = 250
			building_employment_mages_add = 250
		}
	}	

}

pm_deputized_familiars = {	#Traditional Magic
	texture = "gfx/interface/icons/production_method_icons/automated_bakery.dds"
	
	disallowing_laws = {
		law_artifice_only
		law_mundane_production
	}
	
	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_magical_reagents_add = 5
		}

		level_scaled = {
			building_employment_clerks_add = -1250
			building_employment_mages_add = 50
		}
	}

}

pm_self_cleaning_parchment_government = {	#Artificery variant
	texture = "gfx/interface/icons/production_method_icons/automated_bakery.dds"
	
	disallowing_laws = {
		law_industry_banned
	}
	unlocking_technologies = {
		multimaterial_imbuement
	}
	
	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_magical_reagents_add = 5
			goods_input_artificery_doodads_add = 5
		}

		level_scaled = {
			building_employment_clerks_add = -1250
		}
	}

}

pm_arithmaton_calculators_government = {
	texture = "gfx/interface/icons/production_method_icons/automated_bakery.dds"
	
	disallowing_laws = {
		law_industry_banned
	}
	unlocking_technologies = {
		advanced_mechanim			
	}

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_automata_add = 11	
		}

		level_scaled = {
			building_employment_clerks_add = -1750
			building_employment_machinists_add = -1000
			building_employment_bureaucrats_add = -1000
		}
	}
}

## Universities

pm_planar_philosophy_department = {
	texture = "gfx/interface/icons/production_method_icons/analytical_philosophy_department.dds"
	
	unlocking_technologies = {
		planar_philosophy
	}
	
	country_modifiers = {
		workforce_scaled = { 
			country_weekly_innovation_add = 6	#TODO rebalance all this
		}
	}
	
	building_modifiers = {
		workforce_scaled = { 
			goods_input_paper_add = 20
		}
		
		level_scaled = {
			building_employment_clerks_add = 2000
			building_employment_laborers_add = 1000
		}					
	}
	
	state_modifiers = {
		workforce_scaled = { 
			state_pop_qualifications_mult = 0.25
		}	
	}
}	

pm_no_magical_studies = {
	texture = "gfx/interface/icons/production_method_icons/no_automation.dds"
}

pm_spell_components_studies = {
	texture = "gfx/interface/icons/production_method_icons/scholastic_education.dds"
	
	country_modifiers = {
		workforce_scaled = { 
			country_weekly_innovation_add = 1.5
		}
	}
	
	building_modifiers = {
		workforce_scaled = {
			goods_input_magical_reagents_add = 5
		}	
	}
	
	# state_modifiers = {
	# 	workforce_scaled = { 
	# 		state_pop_qualifications_mult = 0.075
	# 	}	
	# }
	# required_input_goods = magical_reagents
}

pm_relics_reverse_enginering = {
	texture = "gfx/interface/icons/production_method_icons/philosophy_dept.dds"
	
	unlocking_technologies = {
		dialectics
	}

	country_modifiers = {
		workforce_scaled = { 
			country_weekly_innovation_add = 3
		}
	}
	
	building_modifiers = {
		workforce_scaled = {
			goods_input_magical_reagents_add = 5
			goods_input_relics_add = 5
		}	
	}
	
	# state_modifiers = {
	# 	workforce_scaled = { 
	# 		state_pop_qualifications_mult = 0.15
	# 	}	
	# }
	# required_input_goods = magical_reagents
	# required_input_goods = relics
}

pm_precursor_immersion_studies = {
	texture = "gfx/interface/icons/production_method_icons/analytical_philosophy_department.dds"
	
	unlocking_technologies = {
		analytical_philosophy
	}

	country_modifiers = {
		workforce_scaled = { 
			country_weekly_innovation_add = 4
		}
	}
	
	building_modifiers = {
		workforce_scaled = {
			goods_input_relics_add = 10
		}	
	}
	
	# state_modifiers = {
	# 	workforce_scaled = { 
	# 		state_pop_qualifications_mult = 0.2
	# 	}	
	# }
	#required_input_goods = relics
}

pm_magical_academia = {
	texture = "gfx/interface/icons/production_method_icons/ownership_academics.dds"

	disallowing_laws = {
		law_state_religion
		law_state_atheism
	}

	building_modifiers = {
		level_scaled = {
			building_employment_mages_add = 1000
			building_employment_academics_add = 1000
		}
	}	
}

pm_self_cleaning_parchment_universities = {
	texture = "gfx/interface/icons/production_method_icons/automated_bakery.dds"
	
	disallowing_laws = {
		law_industry_banned
	}
	unlocking_technologies = {
		multimaterial_imbuement
	}
	
	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_magical_reagents_add = 5
			goods_input_artificery_doodads_add = 5
		}

		level_scaled = {	#TODO REBALANCE
			building_employment_laborers_add = -1000
			building_employment_clerks_add = -250
		}
	}

}

pm_arithmaton_calculators_universities = {
	texture = "gfx/interface/icons/production_method_icons/automated_bakery.dds"
	
	disallowing_laws = {
		law_industry_banned
	}
	unlocking_technologies = {
		advanced_mechanim			
	}
	unlocking_production_methods = {
		pm_philosophy_department
		pm_analytical_philosophy_department
	}

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_automata_add = 5
		}

		level_scaled = {
			building_employment_clerks_add = -1000
			building_employment_laborers_add = -1000
		}
	}
}