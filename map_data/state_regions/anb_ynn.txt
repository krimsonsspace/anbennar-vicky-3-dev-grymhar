﻿STATE_NIZVELS = {
    id = 248
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0AFDFD" "x175237" "x26CBFD" "x2A7820" "x46FDFD" "x565C7E" "x6921C7" "xAB7A4F" "xB34059" "xC9C9ED" }
    traits = { state_trait_ynn_river }
    city = "x2a7820" #Vels Domfan

    farm = "xab7a4f" #Stenurynn

    wood = "x565c7e" #Sardobnn

    arable_land = 50
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_tobacco_plantations bg_vineyard_plantations }
    capped_resources = {
        bg_logging = 22
        bg_lead_mining = 38
    }
}
STATE_HRADAPOLERE = {
    id = 249
    subsistence_building = "building_subsistence_farms"
    provinces = { "x20FDFD" "x2B7C7E" "x2B8020" "x2C8820" "x8268E0" "xAB7EAD" "xB938AE" "xC3285E" "xD54EAD" "xF3EBCE" }
    traits = { state_trait_ynn_river }
    city = "x2C8820" #Stanyrhrada

    farm = "xf3ebce" #Trompolere

    wood = "xb938ae" #Vels Amsto

    arable_land = 90
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_tobacco_plantations bg_vineyard_plantations }
    capped_resources = {
        bg_logging = 15
        bg_iron_mining = 21
    }
}
STATE_YRISRAD = {
    id = 250
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0A64FD" "x1D5462" "x22FDFD" "x2B847E" "x57A6A9" "xAB824F" "xAB86AD" "xB8E085" "xCA8971" "xE89084" }
    traits = { state_trait_ynn_river }
    city = "xab824f" #Arverynn

    farm = "xab86ad" #Munamsto

    wood = "x0a64fd" #Vels Bacar

    arable_land = 120
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_tobacco_plantations bg_vineyard_plantations }
    capped_resources = {
        bg_logging = 12
        bg_coal_mining = 18
    }
}
STATE_VIZANIRZAG = {
    id = 251
    subsistence_building = "building_subsistence_farms"
    provinces = { "x08DC28" "x125E8D" "x253252" "x2C9020" "x2D9820" "x370DE9" "x4664FD" "x5550B3" "x9BAA44" "xAC924F" }
    traits = {}
    city = "x7a3051" #Virizerno

    farm = "x2c9020" #Orsiarinn

    wood = "x9BAA44" #Random

    arable_land = 200
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_tobacco_plantations bg_vineyard_plantations }
    capped_resources = {
        bg_logging = 17
        bg_iron_mining = 31
    }
}
STATE_CORINSFIELD = {
    id = 252
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0000E8" "x0E3347" "x269C24" "x2E2C7E" "x548259" "x598A2B" "x86914A" "x8D08C8" "x8E97D7" "x93C6F9" "xAF324F" "xDC7A3B" "xFB4936" "xFFF880" }
    traits = {}
    city = "x598a2b" #Rohaves

    farm = "x8e97d7" #Poppyspring

    mine = "x2b847e" #Thromshana

    wood = "x0e3347" #Bedork

    arable_land = 175
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_opium_plantations bg_dye_plantations bg_tobacco_plantations bg_vineyard_plantations }
    capped_resources = {
        bg_logging = 8
        bg_coal_mining = 19
    }
}
STATE_WEST_TIPNEY = {
    id = 253
    subsistence_building = "building_subsistence_farms"
    provinces = { "x1FC47C" "x2E2820" "x389C26" "x534F21" "x74EC5D" "xAD224F" "xAE26AD" "xD7220A" "xD7E30A" }
    traits = {}
    city = "xad224f" #Amstoynn

    farm = "xd7220a" #Tipneyshire

    wood = "xae26ad" #Velencestor

    mine = "xaef6ad" #Adirna

    arable_land = 200
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_tobacco_plantations bg_vineyard_plantations }
    capped_resources = {
        bg_logging = 12
        bg_coal_mining = 23
    }
}
STATE_OSINDAIN = {
    id = 254
    subsistence_building = "building_subsistence_farms"
    provinces = { "x2E247E" "x41362A" "x43D9C7" "x4CAC90" "x6ACA62" "x74035F" "x79CAD1" "x873B3F" "x8C099B" "xCF5860" "xEC6E77" }
    traits = {}
    city = "xad9a4f" #Vathres

    farm = "x86c279" #Szoholar

    mine = "x74035F" #Rezbainn

    wood = "x41362a" #Vyrgalykar

    arable_land = 125
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_tobacco_plantations bg_vineyard_plantations }
    capped_resources = {
        bg_logging = 13
        bg_coal_mining = 25
    }
}
STATE_ARGEZVALE = {
    id = 255
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0ACAFC" "x1ACAFC" "x2E3020" "x304C7E" "x30FDFD" "x4078AC" "x562E86" "x5D1B47" "x6C8C8C" "xA19253" "xA79D17" "xAE2A4F" "xAEF6AD" "xC95F9C" "xCDE3A6" }
    traits = {}
    city = "xae2a4f" #Neyholar

    farm = "x2e3020" #Talmsild

    wood = "x304c7e" #Sethcalrm

    mine = "xcde3a6" #Ujorfalon

    arable_land = 125
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_tobacco_plantations bg_vineyard_plantations }
    capped_resources = {
        bg_logging = 11
        bg_coal_mining = 35
        bg_iron_mining = 43
        bg_gem_mining = 6
    }
}
STATE_NIZELYNN = {
    id = 256
    subsistence_building = "building_subsistence_farms"
    provinces = { "x1DE298" "x30731E" "x4F7C7E" "x508020" "x72B8C1" "x77D750" "xB8B2B8" "xC24335" "xCF76AD" "xCF7A4F" "xCF7EAD" }
    traits = { state_trait_ynn_river }
    city = "x77d750" #Nizamsto

    farm = "xcf7a4f" #NEW PLACE

    mine = "xcf76ad" #Selocshana

    wood = "xcf7ead" #Kamhubi

    arable_land = 60
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_tobacco_plantations bg_vineyard_plantations }
    capped_resources = {
        bg_logging = 20
    }
}
STATE_CHIPPENGARD = {
    id = 257
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0100EC" "x47FADC" "x4F747E" "x6E9539" "x839ACC" "xCF724F" }
    traits = { state_trait_ynn_river }
    city = "x4f747e" #Ynngard

    farm = "x839acc" #Vuhsbya

    wood = "xcf724f" #Epadarkan

    arable_land = 100
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_vineyard_plantations }
    capped_resources = {
        bg_logging = 6
        bg_iron_mining = 10
    }
}
STATE_BEGGASLAND = {
    id = 258
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0DCACA" "x353ADA" "x4F7820" "x6A1AA0" "x6F282B" "x73279D" "x739102" "xAF9235" "xB575E1" "xC815DF" "xD0824F" "xD18EAD" "xD1924F" }
    traits = {}
    city = "x4f7820" #Beggaston

    farm = "x6A1AA0" #NEW PLACE

    mine = "xd1924f" #NEW PLACE

    wood = "x6f282b" #Yanumpa

    arable_land = 200
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_vineyard_plantations }
    capped_resources = {
        bg_logging = 8
    }
}
STATE_PLUMSTEAD = {
    id = 259
    subsistence_building = "building_subsistence_farms"
    provinces = { "x21CFF3" "x2736FF" "x50D3E7" "x51947E" "x74485D" "x823C32" "x825AA5" "x9D4E94" "xB63D44" "xB64102" "xB68A65" "xD54A4F" "xD65EAD" "xEABA36" "xEEF132" "xEFA010" "xFFCAC6" }
    traits = { state_trait_lady_isobel }
    city = "x9d4e94" #Plumstead

    farm = "x823C32" #Ubatap

    mine = "x51947E" #NEW PLACE

    wood = "x21cff3" #Cathfei

    arable_land = 200
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_vineyard_plantations }
    capped_resources = {
        bg_logging = 10
        bg_iron_mining = 16
    }
}
STATE_TUSNATA = {
    id = 260
    subsistence_building = "building_subsistence_farms"
    provinces = { "x027523" "x02A7E0" "x02E9B6" "x04F586" "x0A8769" "x358793" "x39657F" "x4300F0" "x565820" "x5E37F5" "x64C2A9" "x6D655D" "x72344D" "x7F647A" "x81CD1A" "x8DFAB2" "x9212AD" "x92226D" "x977065" "x994148" "xA063E3" "xA4E315" "xA67C52" "xB8C657" "xB97601" "xBFA411" "xC01412" "xCBEE81" "xCC2229" "xCC4EAD" "xCC800D" "xCEA621" "xD2AD4C" "xD2F3C2" "xDEAC95" "xEA9580" "xF25F96" "xF39C21" }
    traits = {}
    city = "x6d655d" #Drenn Springs

    farm = "x7f647a" #Snake City

    mine = "x81cd1a" #Drybone

    wood = "xa063e3" #Altiri

    arable_land = 120
    arable_resources = { bg_maize_farms bg_livestock_ranches }
    resource = {
        type = "bg_oil_extraction"
        undiscovered_amount = 5
    }
    resource = {
        type = "bg_gold_fields"
        depleted_type = "bg_gold_mining"
        undiscovered_amount = 6
    }
}
STATE_BORUCKY = {
    id = 261
    subsistence_building = "building_subsistence_farms"
    provinces = { "x06CB06" "x0DA207" "x15DC6A" "x1B47CD" "x3F684C" "x4A00F8" "x4F8AB8" "x58F01D" "x7175F1" "x83F924" "x8C7DA9" "x95955A" "x9EE214" "xAFD5DB" "xB407F5" "xC35D22" "xE7A6D4" "xF781E3" }
    traits = {}
    city = "xf639ee" #NEW PLACE

    farm = "x32A3EF" #random

    mine = "xd336ad" #Taslamo

    wood = "x15dc6a" #Tampu

    arable_land = 50
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 2
        bg_coal_mining = 14
    }
    resource = {
        type = "bg_gold_fields"
        depleted_type = "bg_gold_mining"
        undiscovered_amount = 5
    }
    resource = {
        type = "bg_oil_extraction"
        undiscovered_amount = 8
    }
}
STATE_ARANTAS = {
    id = 262
    subsistence_building = "building_subsistence_farms"
    provinces = { "x32A3EF" "x4C8AAE" "x4F3CAC" "x52DB23" "x533020" "x53347E" "x543820" "x59D59D" "x62B483" "x67124D" "x702501" "x7419DC" "xA13270" "xC4097C" "xD336AD" "xD52C89" "xDDD3D2" "xF639EE" }
    traits = { state_trait_lady_isobel }
    city = "x67124d" #NEW PLACE

    farm = "x53347e" #Sanatsiha

    mine = "xafc854" #Cianith

    wood = "x533020" #Orontas

    arable_land = 100
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 4
    }
    resource = {
        type = "bg_oil_extraction"
        undiscovered_amount = 18
    }
}
STATE_POSKAWA = {
    id = 263
    subsistence_building = "building_subsistence_pastures"
    provinces = { "x00DBA3" "x01DEC2" "x05425C" "x0D244F" "x12934C" "x1B6394" "x29A196" "x2EED5C" "x3A5AA8" "x3ACBB0" "x492954" "x5245BA" "x6B57B7" "x724807" "x7581D9" "x7852C7" "x939325" "x99AF78" "x9B88F7" "x9E647D" "xB45149" "xB52187" "xB8A52A" "xCE714C" "xD41EC5" "xDA2BD3" "xE44832" "xEA1567" "xECEF7C" "xF2E18C" }
    traits = { state_trait_lady_isobel state_trait_ekyunimoy_range }
    city = "xDA2BD3" #NEW PLACE

    farm = "x3A5AA8" #NEW PLACE

    mine = "x492954" #NEW PLACE

    wood = "x62B483" #NEW PLACE

    arable_land = 50
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 4
    }
    resource = {
        type = "bg_gold_fields"
        depleted_type = "bg_gold_mining"
        undiscovered_amount = 8
    }
    resource = {
        type = "bg_oil_extraction"
        undiscovered_amount = 4
    }
}
STATE_ELATHAEL = {
    id = 264
    subsistence_building = "building_subsistence_farms"
    provinces = { "x03DD22" "x0B7628" "x10756B" "x11F1FD" "x239726" "x281166" "x288A73" "x2E9DA6" "x38606F" "x452F46" "x508C7E" "x519020" "x61EEE0" "x656643" "x710529" "x76F197" "x803E63" "x8CB45D" "x95E064" "xA85510" "xAFC854" "xC261EE" "xCBABE7" "xD3324F" "xEE6190" "xFF3D0A" }
    traits = {}
    city = "xEE6190" #Elathael

    farm = "xd32ead" #Jerarur

    mine = "x0DF97C" #Boruonn

    wood = "xD90036" #Bronmas

    arable_land = 200
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 4
        bg_sulfur_mining = 16
    }
    resource = {
        type = "bg_oil_extraction"
        undiscovered_amount = 5
    }
}
STATE_EPADARKAN = {
    id = 266
    subsistence_building = "building_subsistence_farms"
    provinces = { "x05C757" "x32A3E3" "x4E647E" "x4E6C7E" "x4E7020" "x82ABE7" "x879E21" "xBBDCD7" "xCE66AD" "xCE6EAD" }
    prime_land = { "xCE6EAD" "x4E7020" }
    traits = { state_trait_ynn_river }
    city = "x82abe7" #Minata

    farm = "xd656ad" #Adbraseloc

    wood = "xce66ad" #Epadobnn

    arable_land = 60
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 10
        bg_iron_mining = 19
    }
}
STATE_UZOO = {
    id = 267
    subsistence_building = "building_subsistence_pastures"
    provinces = { "x01618D" "x1031A8" "x123C89" "x1C8912" "x522020" "x61EEB3" "x631289" "x6A72A5" "x6DEE61" "x7691F1" "x91F5D1" "x92E3DF" "xA11441" "xA2E00C" "xA7400B" "xB11739" "xB578D2" "xB6510D" "xBCEC41" "xD27897" "xE6A848" }
    prime_land = { xD27897 }
    traits = {}
    city = "xe6a848" #Eagle Fork

    farm = "xb578d2" #Dusthaven

    mine = "xb6510d" #Leadton

    wood = "x01618d" #Yeehaw Junction

    arable_land = 125
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 2
        bg_lead_mining = 16
        bg_coal_mining = 28
        bg_gold_mining = 2
    }
    resource = {
        type = "bg_gold_fields"
        depleted_type = "bg_gold_mining"
        undiscovered_amount = 4
    }
    resource = {
        type = "bg_oil_extraction"
        undiscovered_amount = 5
    }
}
STATE_ARGANJUZORN = {
    id = 268
    subsistence_building = "building_subsistence_farms"
    provinces = { "x3E636E" "x3FB373" "x4D6020" "x50847E" "x705EEF" "x918823" "xB2624F" "xC16892" "xCD624F" "xD656AD" "xE9106A" "xF7B2F5" }
    traits = { state_trait_ynn_river }
    city = "x918823" #Velikvab

    farm = "xcd624f" #Arganjuzorn

    mine = "x3FB373" #Random

    wood = "x4d6020" #NEW PLACE

    arable_land = 120
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 8
        bg_coal_mining = 20
    }
}
STATE_EBENMAS = {
    id = 269
    subsistence_building = "building_subsistence_farms"
    provinces = { "x21AB8C" "x28DF24" "x2E999C" "x4A06B0" "x4CB25A" "x4D5C7E" "x4E6820" "x51617F" "x96A842" "x9AD448" "xCA2941" "xCD5EAD" "xCD9ACC" "xCE6A4F" "xF01382" }
    traits = {}
    city = "x4e6820" #Ebenmas

    farm = "xcd5ead" #Tuhotyr

    mine = "xca2941" #Gosnahyr

    wood = "x4CB25A" #Random

    arable_land = 150
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 7
        bg_coal_mining = 23
    }
    resource = {
        type = "bg_oil_extraction"
        undiscovered_amount = 12
    }
}
STATE_TELLUMTIR = {
    id = 927
    subsistence_building = "building_subsistence_farms"
    provinces = { "x09941C" "x226609" "x2CA69D" "x508820" "x751F10" "x76710B" "x84CCA4" "x91AB9A" "xC54262" "xCC761D" "xCD03F7" "xD086AD" "xD08A4F" "xF24325" "xF2E825" "xF3A3F9" "xF78BB7" "xFC3DA5" "xFF664F" }
    traits = {}
    city = "xFF664F" #Mara Luar

    farm = "xc54262" #Random

    arable_land = 175
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 6
        bg_iron_mining = 36
        bg_coal_mining = 29
    }
    resource = {
        type = "bg_oil_extraction"
        undiscovered_amount = 5
    }
}
STATE_YECKUNIA = {
    id = 274
    subsistence_building = "building_subsistence_pastures"
    provinces = { "x0423EC" "x107756" "x147502" "x1782AF" "x186D61" "x272C16" "x33807A" "x4031B9" "x4FDE31" "x5EF03F" "x65C27F" "x6AC9DC" "x6FA266" "x7C3A65" "x7F8525" "x92C193" "x92FD41" "x933AD2" "x939F1B" "x9B0DB2" "x9B5562" "x9D360A" "x9E64FD" "xA56FA1" "xB08090" "xCD6C45" "xD29EAD" "xDDE5A5" "xDE52B4" "xF00110" "xF10B85" }
    traits = { state_trait_ekyunimoy_range }
    city = "x8113CC" #NEW PLACE

    farm = "x519820" #Khiberseri

    mine = "x1C63CA" #Ciasuhul

    wood = "xD19A99" #NEW PLACE

    arable_land = 50
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 2
        bg_iron_mining = 25
    }
    resource = {
        type = "bg_gold_fields"
        depleted_type = "bg_gold_mining"
        undiscovered_amount = 8
    }
    resource = {
        type = "bg_oil_extraction"
        undiscovered_amount = 5
    }
}
STATE_CASNAROG = {
    id = 275
    subsistence_building = "building_subsistence_farms"
    provinces = { "x010000" "x0B75A6" "x21EC8E" "x7AC1C6" "x8DE06D" "xA67606" "xA77CD4" "xBBEA29" "xC61D1A" "xCC4A4F" "xDBCDFD" "xDE5B96" "xE0D7C4" "xE799E2" }
    traits = { state_trait_ekyunimoy_range }
    city = "xc61d1a" #Varlenkrego

    farm = "xcb3a4f" #Drozmavarlena

    mine = "xcb0db2" #NEW PLACE

    wood = "xde5b96" #NEW PLACE

    arable_land = 60
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 8
        bg_iron_mining = 39
    }
    resource = {
        type = "bg_oil_extraction"
        undiscovered_amount = 5
    }
}
STATE_POMVASONN = {
    id = 276
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0280B6" "x14FC80" "x16BFF0" "x210F72" "x3065B0" "x376932" "x4D5820" "x5C5A32" "x6C5ABF" "x73E3D7" "x780EB7" "x7C8DD6" "x9903F3" "xA5F360" "xCB0DB2" "xCB3A4F" "xCD5A4F" "xF4AD90" "xFB6977" "xFD0582" "xFD2346" }
    traits = { state_trait_ynn_river }
    city = "x4D5820" #Pomvasonn

    farm = "x0280B6" #Drozmaynn

    wood = "x3065B0" #Random

    arable_land = 120
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 10
    }
    resource = {
        type = "bg_oil_extraction"
        undiscovered_amount = 5
    }
}
STATE_MOCEPED = {
    id = 277
    subsistence_building = "building_subsistence_farms"
    provinces = { "x326020" "x32647E" "xCED94D" "xEBC506" }
    traits = { state_trait_ynn_river }
    city = "x326020" #Drozma Mors

    farm = "x32647E" #Random

    arable_land = 60
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 17
        bg_sulfur_mining = 14
    }
}
STATE_VIZKALADR = {
    id = 278
    subsistence_building = "building_subsistence_farms"
    provinces = { "x168EBF" "x326820" "x336C7E" "x44647E" "x52AF0D" "x5ECD49" "x635DF1" "x6B4507" "x8CBC59" "x8F8585" "xA5B2DA" "xA964D8" "xB36A4F" "xBC808F" "xC1CACE" "xC5724F" "xEF695A" "xFA3446" }
    traits = { state_trait_ynn_river }
    city = "xB36A4F" #Svemel

    farm = "x326820" #Ynnkmanja

    wood = "x44647E" #Gozhar Urthid

    arable_land = 120
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 16
        bg_coal_mining = 26
    }
}
STATE_GOMOSENGHA = {
    id = 279
    subsistence_building = "building_subsistence_farms"
    provinces = { "x015723" "x172A15" "x1D1D4B" "x2FE076" "x30E010" "x33D420" "x46747E" "x47847E" "x57B39E" "x61497F" "x632058" "x80C622" "x8A5623" "xABCC60" "xAE7647" "xAE89CA" "xB07C3A" "xB36EAD" "xC7824F" "xC92A4F" "xDACD5C" "xDCD800" "xE6C93D" "xE8FDB0" "xEA9444" "xFC0582" }
    traits = {}
    city = "x33d420" #Gomosengha

    farm = "x57B39E" #Velrekstepa

    mine = "x47847E" #Drozmagomod

    wood = "xb36ead" #Sengha Urthid

    arable_land = 50
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 14
        bg_iron_mining = 65
    }
}
STATE_ARSERGOZHAR = {
    id = 280
    subsistence_building = "building_subsistence_farms"
    provinces = { "x017FE3" "x029422" "x073EDA" "x25D0EA" "x457020" "x467820" "x5A2781" "x811259" "x88AB54" "x8C10F7" "x8F7C86" "xA67B4E" "xC18F6B" "xC2F9FC" "xC56EAD" "xE6882E" "xEA960E" "xFC561E" }
    traits = {}
    city = "x8c10f7" #Random

    farm = "x467820" #Random

    wood = "x457020" #Random

    arable_land = 50
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 1
    }
}
STATE_JUZONDEZAN = {
    id = 281
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0BB524" "x1ECBB0" "x2D00FA" "x446020" "x8765C9" "xA9B0FB" "xB2CAAD" "xB451D6" "xC4624F" "xC566AD" "xCFA79F" }
    traits = { state_trait_ynn_river }
    city = "xB451D6" #Ladjetrevo

    farm = "xB2CAAD" #Ynn Urthid

    wood = "xC4624F" #random

    arable_land = 60
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 17
        bg_coal_mining = 41
    }
}
STATE_USLAD = {
    id = 284
    subsistence_building = "building_subsistence_farms"
    provinces = { "x2238F2" "x27BD4B" "x2FEC20" "x3B1A0D" "x445DAE" "x497957" "x68D1F9" "x8462B0" "xA4C38B" "xAF36AD" "xAF3A4F" "xC47ACB" "xDE18A2" "xE4FCDC" }
    prime_land = { "xAF36AD" }
    traits = { state_trait_ynn_river }
    city = "x445dae" #Bosancovac

    farm = "x2fec20" #Grebshalas

    mine = "x27bd4b" #Dregyljeim

    wood = "xc47acb" #Odmorovca

    arable_land = 70
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 11
        bg_iron_mining = 23
        bg_coal_mining = 38
        bg_sulfur_mining = 35
    }
}
STATE_ROGAIDHA = {
    id = 285
    subsistence_building = "building_subsistence_farms"
    provinces = { "x1C4DD9" "x304020" "x315820" "x33A2E6" "x3677CF" "x42E16E" "x50B101" "x76A4B1" "xA8BD44" "xAA60BB" "xAF3EAD" "xAFA3ED" "xB0424F" "xC066EA" "xD4CBA5" "xF1E0A2" "xF4A0DA" }
    traits = {}
    city = "xaf36ad" #Vareynn

    farm = "x42e16e" #Stantirshalas

    mine = "x1c4dd9" #Morsynn

    wood = "xc066ea" #Nuv Dkandrozma

    arable_land = 120
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 19
        bg_iron_mining = 58
    }
}
STATE_BRELAR = {
    id = 286
    subsistence_building = "building_subsistence_farms"
    provinces = { "x01BF3A" "x029AC8" "x0433F6" "x1B8D91" "x234A11" "x43547E" "x516444" "x668ABB" "x6E1281" "x6E7D70" "x7E8FCE" "x7F712C" "x90F2A4" "xAA85EE" "xCB314E" "xE953F1" "xFB4F0D" "xFC375D" }
    prime_land = { "x6E7D70" "x668ABB" "xCB314E" }
    traits = {}
    city = "x0433f6" #Hrilar

    farm = "xcb314e" #Gozhar Gomod

    wood = "x43547e" #Shwarnadh

    arable_land = 40
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 12
        bg_iron_mining = 50
    }
}
STATE_VERZEL = {
    id = 287
    subsistence_building = "building_subsistence_farms"
    provinces = { "x18B860" "x3F5CFC" "x456820" "x4F1447" "x603A9F" "x848A36" "x918A72" "xA78819" "xC45EAD" "xC676AD" "xE08DF2" "xE9DFE9" "xF6D626" }
    traits = {}
    city = "x456820" #Random

    farm = "xa78819" #Random

    wood = "xc45ead" #Random

    arable_land = 25
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 14
        bg_coal_mining = 35
    }
}
STATE_VITREYNN = {
    id = 288
    subsistence_building = "building_subsistence_farms"
    provinces = { "x09B4DF" "x1D78C6" "x2F347E" "x30447E" "x31547E" "x8953E4" "x8C193E" "x9153EF" "xA24080" "xB0E010" "xB156AD" "xC36E27" "xC9CE36" "xDFEC70" }
    traits = {}
    city = "x30447E" #Amacimst

    farm = "x31547E" #Odmoreoch

    wood = "xB156AD" #Adkyanzabr

    arable_land = 80
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 15
        bg_iron_mining = 28
        bg_coal_mining = 21
    }
}
STATE_VARBUKLAND = {
    id = 289
    subsistence_building = "building_subsistence_farms"
    provinces = { "x2BBF2E" "x30800E" "x30AC20" "x315020" "x553B3F" "x592AB4" "x5D6B95" "x8830BD" "x8E85EB" "xB04A4F" "xB0ED0F" "xB14EAD" "xC7DD44" "xE7F68A" }
    traits = {}
    city = "xb04a4f" #Random

    farm = "x30ac20" #Random

    wood = "x315020" #Random

    arable_land = 50
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 25
        bg_iron_mining = 14
        bg_coal_mining = 16
    }
}
STATE_KEWDHEMR = {
    id = 290
    subsistence_building = "building_subsistence_farms"
    provinces = { "x02C052" "x144683" "x325C7E" "x33747E" "x347820" "x3574EE" "x3B77C3" "x45155F" "x4A805F" "x4CF080" "x4EAC2D" "x521B0A" "x5888F2" "x58ECB9" "x90C0D0" "x9294FB" "x9A65E7" "xA28E8D" "xA86344" "xB1524F" "xB25EAD" "xB376AD" "xB6AC81" "xC42462" "xC896AD" "xCC30AA" "xE4630F" "xEFB409" "xF5D732" "xF690F1" }
    traits = {}
    city = "xb1524f" #Random

    farm = "x325c7e" #Random

    wood = "x9a65e7" #Random

    arable_land = 16
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 20
        bg_sulfur_mining = 8
    }
}
STATE_ZEMSHOGEN = {
    id = 294
    subsistence_building = "building_subsistence_farms"
    provinces = { "x350B0F" "x435020" "x47BA2C" "x8A9598" "x8BF39D" "x94AA52" "xB15A4F" "xBE17B4" "xD88028" "xDAC04E" "xF6490B" }
    traits = {}
    city = "x94aa52" #Random

    farm = "x8a9598" #Random

    wood = "x47ba2c" #Random

    arable_land = 19
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 1
    }
}
STATE_WHETJUL = {
    id = 291
    subsistence_building = "building_subsistence_farms"
    provinces = { "x07F5A7" "x0BD1D3" "x0C81D4" "x1CAB38" "x26952C" "x3618F8" "x37EB2E" "x424820" "x5BA7C2" "x774C99" "x8E9293" "x9EF738" "xAA1F3D" "xB0E4AA" "xB3A09E" "xB47A4F" "xB79F3F" "xBC5F9C" "xC246AD" "xC544F3" "xD2DD34" "xD2FCCD" "xF063FA" }
    traits = {}
    city = "xc544f3" #Random

    farm = "xb47a4f" #Random

    wood = "x424820" #Random

    arable_land = 18
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 22
        bg_sulfur_mining = 10
    }
}
STATE_NEFRNMEHN = {
    id = 292
    subsistence_building = "building_subsistence_farms"
    provinces = { "x10C0D0" "x1205C7" "x12C24F" "x18A0E9" "x1BC85D" "x42447E" "x5F5474" "x62F8C0" "x706090" "x735799" "x8AA282" "x972B02" "xA26430" "xA44376" "xAB76FA" "xB180F2" "xC34A4F" "xC34EAD" "xCF5848" "xEC5162" }
    traits = {}
    city = "xc34a4f" #Random

    farm = "x8aa282" #Random

    wood = "xa44376" #Random

    arable_land = 14
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 16
    }
}
STATE_HEHADEDPAR = {
    id = 293
    subsistence_building = "building_subsistence_farms"
    provinces = { "x3AB420" "x435071" "x438067" "x4BFE37" "x5A7430" "x5B6292" "x9C5DD8" "xA427EB" "xB04111" "xB41CB3" "xB9424F" "xBC83B9" "xF1F9BA" "xFA990A" }
    traits = {}
    city = "x3ab420" #Random

    farm = "x435071" #Random

    wood = "xb04111" #Random

    arable_land = 15
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 18
        bg_lead_mining = 20
    }
}
STATE_NEW_HAVORAL = {
    id = 923
    subsistence_building = "building_subsistence_farms"
    provinces = { "x20610E" "x2D2020" "x3675E1" "x4A3479" "x572ADE" "x77CE39" "x797F05" "x86C279" "x89DF1B" "x959BCA" "xAD9EAD" "xCE1A2C" "xD0CC1A" "xDF69C6" "xE4E0A7" "xF56178" }
    traits = {}
    city = "x4a3479" #Random

    farm = "x572ade" #Random

    mine = "xad9ead" #Random

    wood = "xe4e0a7" #Random

    arable_land = 75
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_tobacco_plantations bg_vineyard_plantations }
    capped_resources = {
        bg_logging = 17
        bg_iron_mining = 20
        bg_lead_mining = 24
    }
}
STATE_LETHIR = {
    id = 924
    subsistence_building = "building_subsistence_farms"
    provinces = { "x09CACA" "x0B902C" "x2D9C7E" "x4B7938" "x68A4CA" "x79BFE0" "x7A3051" "x94775B" "xAD9A4F" "xC1B877" "xC26FFD" "xE02C91" }
    traits = {}
    city = "xe02c91" #Random

    farm = "x2d9c7e" #Random

    mine = "xad9a4f" #Random

    wood = "x79bfe0" #Random

    arable_land = 150
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_tobacco_plantations bg_vineyard_plantations }
    capped_resources = {
        bg_logging = 20
    }
}
STATE_VYCHVELS = {
    id = 925
    subsistence_building = "building_subsistence_farms"
    provinces = { "x2C8C7E" "x2C947E" "x584E06" "x8AA90D" "xA162DA" "xAC8A4F" "xAD96AD" "xB77DB2" "xD14535" }
    traits = {}
    city = "xd14535" #Random

    farm = "x8aa90d" #Random

    wood = "x2c8c7e" #Random

    arable_land = 50
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_tobacco_plantations bg_vineyard_plantations }
    capped_resources = {
        bg_logging = 23
        bg_iron_mining = 26
    }
}
STATE_YNNPADH = {
    id = 926
    subsistence_building = "building_subsistence_farms"
    provinces = { "x2D500A" "x38CA10" "x3B2518" "x567442" "x7A0796" "x933054" "xAA76AD" "xAC8EAD" "xF654C9" }
    traits = { state_trait_ynn_river state_trait_venaans_tears }
    city = "xac8ead" #Random

    farm = "x38ca10" #Random

    arable_land = 90
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_tobacco_plantations bg_vineyard_plantations }
    capped_resources = {
        bg_logging = 18
    }
}
STATE_ESIMOINE = {
    id = 270
    subsistence_building = "building_subsistence_pastures"
    provinces = { "x02FD3B" "x02FD84" "x19F81E" "x1F5555" "x1F55C5" "x22F6CA" "x2B9230" "x37B0C4" "x397FCF" "x43591B" "x55453C" "x564A37" "x567637" "x625A03" "x78B789" "x7C70AB" "x86E3B5" "x9AA4B9" "x9BC4D6" "xA45452" "xA5E729" "xA71D2B" "xA8029D" "xA810C9" "xA86DC9" "xB02050" "xB22178" "xC9478C" "xCE75F6" "xD43A4F" "xD7C0DF" "xE32C31" "xE8124F" "xE98C0B" "xE9FFED" "xEABEF5" }
    prime_land = { "xa8029d" "xa71d2b" }
    traits = { state_trait_ekyunimoy_range }
    city = "x02fd3b" #Random

    farm = "x78b789" #Random

    wood = "xb02050" #Random

    mine = "xa810c9" #Random

    arable_land = 50
    arable_resources = { bg_livestock_ranches }
    capped_resources = {
        bg_logging = 3
        bg_coal_mining = 22
        bg_gold_mining = 4
    }
    resource = {
        type = "bg_gold_fields"
        depleted_type = "bg_gold_mining"
        undiscovered_amount = 6
    }
}
STATE_MORGANIA = {
    id = 271
    subsistence_building = "building_subsistence_pastures"
    provinces = { "x0962C2" "x1828CA" "x4B542E" "x57A085" "x7A4665" "x7B3847" "x851A82" "x87C05C" "x8A65F0" "xA89044" "xADB202" "xB1895F" "xD05A2E" "xD312AC" "xD3F738" "xD493AE" "xDD52B4" "xEB2460" "xF00190" "xF0E0B3" "xFABD16" "xFE90A7" "xFFEEFC" "xE0374B" }
    traits = { state_trait_ekyunimoy_range state_trait_harafe_desert }
    city = "xa45452" #Random

    farm = "xa5e729" #Random

    wood = "x99af78" #Random

    mine = "xb8a52a" #Random

    arable_land = 50
    arable_resources = { bg_livestock_ranches }
}
STATE_TASPAS = {
    id = 272
    subsistence_building = "building_subsistence_pastures"
    provinces = { "x0EE70B" "x1080D0" "x1803D0" "x18C0CB" "x1F1368" "x21B59A" "x245E2A" "x39D7C8" "x413E51" "x455C7F" "x50ADEE" "x528D86" "x5ECE79" "x8C9A5C" "x9068BD" "x9D4993" "xAA1982" "xC71E45" "xC9C900" "xCCAD63" "xCE1B54" "xF39243" "xF42C38" }
    traits = { state_trait_ekyunimoy_range }
    city = "x39d7c8" #Random

    farm = "x9d4993" #Random

    wood = "x528d86" #Random

    mine = "x1803d0" #Random

    arable_land = 50
    arable_resources = { bg_livestock_ranches }
    capped_resources = {
        bg_logging = 2
        bg_iron_mining = 33
    }
}
STATE_EKRSOKA = {
    id = 273
    subsistence_building = "building_subsistence_pastures"
    provinces = { "x1620A9" "x1C63CA" "x1DB8ED" "x478C17" "x519820" "x529C7E" "x569559" "x6FFD2D" "x73CD08" "x8113CC" "x835A64" "xB06090" "xC5D272" "xD196AD" "xD19A4F" "xD19A99" "xD1F150" "xF508CE" }
    traits = {}
    city = "xcc4a4f" #Random

    farm = "x9b5562" #Random

    wood = "x0b75a6" #Random

    mine = "xa77cd4" #Random

    arable_land = 50
    arable_resources = { bg_livestock_ranches }
    capped_resources = {
        bg_logging = 4
        bg_coal_mining = 31
    }
    resource = {
        type = "bg_oil_extraction"
        undiscovered_amount = 25
    }
}
