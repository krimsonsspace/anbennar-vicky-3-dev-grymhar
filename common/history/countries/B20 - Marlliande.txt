﻿COUNTRIES = {
	c:B20 ?= {
		effect_starting_technology_tier_2_tech = yes

		add_technology_researched = egalitarianism
		
		activate_law = law_type:law_parliamentary_republic
		activate_law = law_type:law_landed_voting #Can vote, but only for vampires
		activate_law = law_type:law_national_supremacy
		activate_law = law_type:law_freedom_of_conscience
		activate_law = law_type:law_elected_bureaucrats
		activate_law = law_type:law_national_militia
		# No home affairs
		activate_law = law_type:law_agrarianism
		activate_law = law_type:law_per_capita_based_taxation
		# No colonial affairs
		# No police
		# No school system
		activate_law = law_type:law_no_health_system
		activate_law = law_type:law_traditional_magic_encouraged
		
		activate_law = law_type:law_censorship
		activate_law = law_type:law_tenant_farmers
		activate_law = law_type:law_child_labor_allowed
		activate_law = law_type:law_women_own_property
		activate_law = law_type:law_no_social_security
		# No migration controls
		activate_law = law_type:law_legacy_slavery
		
		activate_law = law_type:law_non_monstrous_only

		ig:ig_landowners = {
			add_ruling_interest_group = yes
		}
	}
}