﻿BUILDINGS= {
	s:STATE_BRONRIN = {
		region_state:C02 = {
			create_building={
				building="building_maize_farm"
				level=1
				reserves=1
				activate_production_methods={ "pm_simple_farming" "pm_citrus_orchards" "pm_tools_disabled" "pm_privately_owned" }
			}
			create_building={
				building="building_livestock_ranch"
				level=1
				reserves=1
				activate_production_methods={ "pm_simple_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_butchering_tools" }
			}
			# create_building={
			# 	building="building_coffee_plantation"
			# 	level=1
			# 	reserves=1
			# 	activate_production_methods={ "default_building_coffee_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			# }
			create_building={
				building="building_logging_camp"
				level=1
				reserves=1
				activate_production_methods={ "pm_simple_forestry" "pm_no_hardwood" "pm_no_equipment" "pm_road_carts" "pm_privately_owned_building_logging_camp" }
			}
			create_building={
				building="building_whaling_station"
				level=1
				reserves=1
				activate_production_methods={ "pm_wooden_whaling_ships" "pm_unrefrigerated" "pm_merchant_guilds_building_whaling_station" }
			}
			create_building={
				building="building_fishing_wharf"
				level=1
				reserves=1
				activate_production_methods={ "pm_simple_fishing" "pm_unrefrigerated" "pm_merchant_guilds_building_fishing_wharf" }
			}
			create_building={
				building="building_textile_mills"
				level=1
				reserves=1
				activate_production_methods={ "pm_handsewn_clothes" "pm_merchant_guilds_building_textile_mills" "pm_traditional_looms" "pm_no_luxury_clothes" }
			}
			create_building={
				building="building_furniture_manufacturies"
				level=1
				reserves=1
				activate_production_methods={ "pm_handcrafted_furniture" "pm_no_luxuries" "pm_automation_disabled" "pm_merchant_guilds_building_furniture_manufacturies" }
			}
			create_building={
				building="building_tooling_workshops"
				level=1
				reserves=1
				activate_production_methods={ "pm_pig_iron" "pm_automation_disabled" "pm_privately_owned_building_tooling_workshops" }
			}
			create_building={
				building="building_paper_mills"
				level=1
				reserves=1
				activate_production_methods={ "pm_pulp_pressing" "pm_automation_disabled" "pm_privately_owned_building_paper_mills" }
			}
			create_building={
				building="building_shipyards"
				level=1
				reserves=1
				activate_production_methods={ "pm_basic_shipbuilding" "pm_merchant_guilds_building_shipyards"  }
			}
			create_building={
				building="building_university"
				level=1
				reserves=1
				activate_production_methods={ "pm_scholastic_education" }
			}
			create_building={
				building="building_arts_academy"
				level=1
				reserves=1
				activate_production_methods={ "pm_traditional_art" }
			}
			create_building={
				building="building_government_administration"
				level=2
				reserves=1
				activate_production_methods={ "pm_simple_organization" "pm_secular_bureaucrats" }
			}
			
			create_building={
				building="building_naval_base"
				level=2
				reserves=1
				activate_production_methods={ "pm_no_naval_theory" }
			}
			create_building={
				building="building_port"
				level=2
				reserves=1
				activate_production_methods={ "pm_basic_port" }
			}
			create_building={
				building="building_construction_sector"
				level=1
				reserves=1
				activate_production_methods={ "pm_wooden_buildings" }
			}
		}
	}
	s:STATE_NUR_ISTRALORE = {
		region_state:C02 = {
			create_building={
				building="building_livestock_ranch"
				level=3
				reserves=1
				activate_production_methods={ "pm_simple_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_butchering_tools" }
			}
			create_building={
				building="building_fishing_wharf"
				level=1
				reserves=1
				activate_production_methods={ "pm_simple_fishing" "pm_unrefrigerated" "pm_merchant_guilds_building_fishing_wharf" }
			}
			
		}
	}
	s:STATE_MUNASIN = {
		region_state:C02 = {
			create_building={
				building="building_banana_plantation"
				level=1
				reserves=1
				activate_production_methods={ "default_building_banana_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_maize_farm"
				level=1
				reserves=1
				activate_production_methods={ "pm_simple_farming" "pm_citrus_orchards" "pm_tools_disabled" "pm_privately_owned" }
			}
			create_building={
				building="building_tobacco_plantation"
				level=1
				reserves=1
				activate_production_methods={ "default_building_tobacco_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_sugar_plantation"
				level=1
				reserves=1
				activate_production_methods={ "default_building_sugar_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			# create_building={
			# 	building="building_coffee_plantation"
			# 	level=1
			# 	reserves=1
			# 	activate_production_methods={ "default_building_coffee_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			# }
		}
	}
	s:STATE_SILVEGOR = {
		region_state:C02 = {
			create_building={
				building="building_maize_farm"
				level=1
				reserves=1
				activate_production_methods={ "pm_simple_farming" "pm_citrus_orchards" "pm_tools_disabled" "pm_privately_owned" }
			}
			create_building={
				building="building_cotton_plantation"
				level=1
				reserves=1
				activate_production_methods={ "default_building_cotton_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_tobacco_plantation"
				level=1
				reserves=1
				activate_production_methods={ "default_building_tobacco_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_dye_plantation"
				level=1
				reserves=1
				activate_production_methods={ "default_building_dye_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_iron_mine"
				level=2
				reserves=1
				activate_production_methods={ "pm_picks_and_shovels_building_iron_mine" "pm_no_explosives" "pm_no_steam_automation" "pm_road_carts" "pm_merchant_guilds_building_iron_mine" }
			}
			
			create_building={
				building="building_naval_base"
				level=2
				reserves=1
				activate_production_methods={ "pm_no_naval_theory" }
			}
			create_building={
				building="building_port"
				level=1
				reserves=1
				activate_production_methods={ "pm_basic_port" }
			}
		}
	}
	s:STATE_CLAMGUINN = {
		region_state:C02 = {
			create_building={
				building="building_maize_farm"
				level=1
				reserves=1
				activate_production_methods={ "pm_simple_farming" "pm_citrus_orchards" "pm_tools_disabled" "pm_privately_owned" }
			}
			create_building={
				building="building_sugar_plantation"
				level=1
				reserves=1
				activate_production_methods={ "default_building_sugar_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_dye_plantation"
				level=1
				reserves=1
				activate_production_methods={ "default_building_dye_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_logging_camp"
				level=1
				reserves=1
				activate_production_methods={ "pm_simple_forestry" "pm_hardwood" "pm_no_equipment" "pm_road_carts" "pm_privately_owned_building_logging_camp" }
			}
			
		}
	}
	s:STATE_URANCESTIR = {
		region_state:C02 = {
			create_building={
				building="building_banana_plantation"
				level=1
				reserves=1
				activate_production_methods={ "default_building_banana_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			# create_building={
			# 	building="building_cotton_plantation"
			# 	level=1
			# 	reserves=1
			# 	activate_production_methods={ "default_building_cotton_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			# }
			create_building={
				building="building_logging_camp"
				level=2
				reserves=1
				activate_production_methods={ "pm_simple_forestry" "pm_hardwood" "pm_no_equipment" "pm_road_carts" "pm_privately_owned_building_logging_camp" }
			}
			create_building={
				building="building_gold_mine"
				level=5
				reserves=1
				activate_production_methods={ "pm_picks_and_shovels_building_gold_mine" "pm_no_explosives" "pm_no_steam_automation" "pm_road_carts" "pm_merchant_guilds_building_gold_mine" }
			}
		}
	}
	s:STATE_CALILVAR = {
		region_state:C02 = {
			create_building={
				building="building_maize_farm"
				level=2
				reserves=1
				activate_production_methods={ "pm_simple_farming" "pm_citrus_orchards" "pm_tools_disabled" "pm_privately_owned" }
			}
			create_building={
				building="building_livestock_ranch"
				level=2
				reserves=1
				activate_production_methods={ "pm_simple_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_butchering_tools" }
			}
		}
	}
	s:STATE_CYMBEAHN = {
		region_state:C02 = {
			create_building={
				building="building_maize_farm"
				level=1
				reserves=1
				activate_production_methods={ "pm_simple_farming" "pm_citrus_orchards" "pm_tools_disabled" "pm_privately_owned" }
			}
			create_building={
				building="building_livestock_ranch"
				level=2
				reserves=1
				activate_production_methods={ "pm_simple_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_butchering_tools" }
			}
			create_building={
				building="building_tobacco_plantation"
				level=1
				reserves=1
				activate_production_methods={ "default_building_tobacco_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_fishing_wharf"
				level=1
				reserves=1
				activate_production_methods={ "pm_simple_fishing" "pm_unrefrigerated" "pm_merchant_guilds_building_fishing_wharf" }
			}
			create_building={
				building="building_textile_mills"
				level=1
				reserves=1
				activate_production_methods={ "pm_handsewn_clothes" "pm_merchant_guilds_building_textile_mills" "pm_traditional_looms" "pm_no_luxury_clothes" }
			}
			create_building={
				building="building_tooling_workshops"
				level=1
				reserves=1
				activate_production_methods={ "pm_pig_iron" "pm_automation_disabled" "pm_privately_owned_building_tooling_workshops" }
			}
			create_building={
				building="building_arms_industry"
				level=1
				reserves=1
				activate_production_methods={ "pm_muskets" "pm_merchant_guilds_building_arms_industry"}
			}
			create_building={
				building="building_university"
				level=1
				reserves=1
				activate_production_methods={ "pm_scholastic_education" }
			}
			create_building={
				building="building_government_administration"
				level=2
				reserves=1
				activate_production_methods={ "pm_simple_organization" "pm_secular_bureaucrats" }
			}
			
			create_building={
				building="building_naval_base"
				level=4
				reserves=1
				activate_production_methods={ "pm_no_naval_theory" }
			}
			create_building={
				building="building_port"
				level=1
				reserves=1
				activate_production_methods={ "pm_basic_port" }
			}
			create_building={
				building="building_construction_sector"
				level=1
				reserves=1
				activate_production_methods={ "pm_wooden_buildings" }
			}
			create_building={
				building="building_military_shipyards"
				level=1
				reserves=1
				activate_production_methods={ "pm_military_shipbuilding_wooden" "pm_merchant_guilds_building_military_shipyards" }
			}
		}
	}
	s:STATE_OOMU_NELIR = {
		region_state:C02 = {
			create_building={
				building="building_livestock_ranch"
				level=1
				reserves=1
				activate_production_methods={ "pm_simple_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_butchering_tools" }
			}
			create_building={
				building="building_gold_mine"
				level=8
				reserves=1
				activate_production_methods={ "pm_picks_and_shovels_building_gold_mine" "pm_no_explosives" "pm_no_steam_automation" "pm_road_carts" "pm_merchant_guilds_building_gold_mine" }
			}
			create_building={
				building="building_sulfur_mine"
				level=1
				reserves=1
				activate_production_methods={ "pm_picks_and_shovels_building_sulfur_mine" "pm_no_explosives" "pm_no_steam_automation" "pm_road_carts" "pm_merchant_guilds_building_sulfur_mine" }
			}
			create_building={
				building="building_government_administration"
				level=1
				reserves=1
				activate_production_methods={ "pm_simple_organization" "pm_secular_bureaucrats" }
			}
			
		}
	}
	s:STATE_CARA_LAFQUEN = {
		region_state:C02 = {
			create_building={
				building="building_maize_farm"
				level=2
				reserves=1
				activate_production_methods={ "pm_simple_farming" "pm_citrus_orchards" "pm_tools_disabled" "pm_privately_owned" }
			}
			create_building={
				building="building_livestock_ranch"
				level=1
				reserves=1
				activate_production_methods={ "pm_simple_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_butchering_tools" }
			}
			create_building={
				building="building_tobacco_plantation"
				level=2
				reserves=1
				activate_production_methods={ "default_building_tobacco_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_logging_camp"
				level=1
				reserves=1
				activate_production_methods={ "pm_simple_forestry" "pm_no_hardwood" "pm_no_equipment" "pm_road_carts" "pm_privately_owned_building_logging_camp" }
			}
			create_building={
				building="building_government_administration"
				level=1
				reserves=1
				activate_production_methods={ "pm_simple_organization" "pm_secular_bureaucrats" }
			}
			
		}
	}
	s:STATE_HARENAINE = {
		region_state:C11 = {
			
		}
	}
	s:STATE_ARANLAS = {
		region_state:C02 = {
			create_building={
				building="building_livestock_ranch"
				level=2
				reserves=1
				activate_production_methods={ "pm_simple_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_butchering_tools" }
			}
			create_building={
				building="building_government_administration"
				level=1
				reserves=1
				activate_production_methods={ "pm_simple_organization" "pm_secular_bureaucrats" }
			}
			create_building={
				building="building_damestear_mine"
				level=10 #Biggest in world
				reserves=1
				activate_production_methods={ "pm_picks_and_shovels_building_damestear_mine" "pm_no_explosives" "pm_no_steam_automation" "pm_road_carts" "pm_privately_owned_building_damestear_mine" }
			}
			
		}
		region_state:C11 = {
			create_building={
				building="building_damestear_mine"
				level=2
				reserves=1
				activate_production_methods={ "pm_picks_and_shovels_building_damestear_mine" "pm_no_explosives" "pm_no_steam_automation" "pm_road_carts" "pm_privately_owned_building_damestear_mine" }
			}
		}
	}
}