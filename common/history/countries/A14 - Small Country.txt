﻿COUNTRIES = {
	c:A14 = {
		effect_starting_technology_tier_3_tech = yes
		add_technology_researched = intensive_agriculture # So they can use all of GHs excess fertilizer
		
		activate_law = law_type:law_parliamentary_republic
		activate_law = law_type:law_landed_voting	#farmers man
		activate_law = law_type:law_national_supremacy
		activate_law = law_type:law_freedom_of_conscience
		activate_law = law_type:law_elected_bureaucrats
		activate_law = law_type:law_national_militia
		
		activate_law = law_type:law_agrarianism
		activate_law = law_type:law_mercantilism	#exporting grain
		activate_law = law_type:law_consumption_based_taxation	#halfling like to eat, so why shouldnt we base our taxes on what one consumes
		activate_law = law_type:law_homesteading
		
		activate_law = law_type:law_right_of_assembly
		#activate_law = law_type:law_serfdom_banned	#farmers and labourers are free tenants
		activate_law = law_type:law_child_labor_allowed
		activate_law = law_type:law_no_womens_rights
		activate_law = law_type:law_slavery_banned
		
		activate_law = law_type:law_same_race_only
		activate_law = law_type:law_dark_arts_banned
		activate_law = law_type:law_amoral_artifice_banned
		activate_law = law_type:law_mundane_production	

		#Consumption-based Taxation
		add_taxed_goods = g:grain
		add_taxed_goods = g:tobacco	#weed tax bruh

		ig:ig_rural_folk = {
			add_ruling_interest_group = yes
		}

		ig:ig_landowners = { 
			add_ruling_interest_group = yes
		}	

	}
}