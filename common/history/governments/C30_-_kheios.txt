GOVERNMENT = {
	c:C30 = {
		if = {
			limit = { exists = py:conservative_party }
			py:conservative_party = {
				set_ruling_party = yes
			}				
		}	
	}
}