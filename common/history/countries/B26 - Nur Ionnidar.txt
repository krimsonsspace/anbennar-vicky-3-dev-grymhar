﻿COUNTRIES = {
	c:B26 = {
		effect_starting_technology_tier_2_tech = yes

		effect_starting_politics_conservative = yes

		activate_law = law_type:law_presidential_republic
		activate_law = law_type:law_autocracy
		activate_law = law_type:law_national_supremacy
		activate_law = law_type:law_freedom_of_conscience
		activate_law = law_type:law_appointed_bureaucrats
		activate_law = law_type:law_national_militia
		# No home affairs
		activate_law = law_type:law_mercantilism
		activate_law = law_type:law_interventionism
		activate_law = law_type:law_per_capita_based_taxation
		activate_law = law_type:law_closed_borders
		# No colonial affairs
		activate_law = law_type:law_local_police
		activate_law = law_type:law_no_schools
		# No health system
		activate_law = law_type:law_traditional_magic_encouraged
		
		activate_law = law_type:law_right_of_assembly
		activate_law = law_type:law_homesteading
		activate_law = law_type:law_child_labor_allowed
		activate_law = law_type:law_women_own_property
		# No social security
		# No migration controls
		activate_law = law_type:law_slave_trade
		
		activate_law = law_type:law_non_monstrous_only


	}
}