﻿COUNTRIES = {
	c:C69 = {
		effect_starting_technology_tier_4_tech = yes
		
		effect_starting_politics_traditional = yes

		#Laws
		activate_law = law_type:law_amoral_artifice_embraced
		
		activate_law = law_type:law_tenant_farmers
		activate_law = law_type:law_artifice_encouraged
		activate_law = law_type:law_no_migration_controls
		activate_law = law_type:law_slave_trade
	}
}