﻿CHARACTERS = {
	c:C30 = {
		create_character = {
			first_name = "Takes"
			last_name = "Aldanos"
			ruler = yes
            ig_leader = yes
			age = 35
			interest_group = ig_landowners
			ideology = ideology_jingoist
			traits = {
				direct
                persistent
			}
		}
	}
}
