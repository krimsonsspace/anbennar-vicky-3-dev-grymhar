﻿COUNTRIES = {
	c:B47 = {
		effect_starting_technology_tier_4_tech = yes
		add_technology_researched = academia
		add_technology_researched = urban_planning
		add_technology_researched = law_enforcement

		effect_starting_politics_traditional = yes

		activate_law = law_type:law_oligarchy
		activate_law = law_type:law_racial_segregation #Nestrin's policies of Ynnic-Eordan unity against cannorians
		activate_law = law_type:law_censorship
		activate_law = law_type:law_no_migration_controls
		activate_law = law_type:law_serfdom
		activate_law = law_type:law_slavery_banned
		
		activate_law = law_type:law_non_monstrous_only
		activate_law = law_type:law_traditional_magic_only
		activate_law = law_type:law_pragmatic_artifice #Bloodrunes

		set_ruling_interest_groups = {
			ig_landowners
		}
	}
}
