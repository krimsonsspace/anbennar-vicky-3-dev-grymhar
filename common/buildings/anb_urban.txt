﻿building_mage_academy = {
	building_group = bg_mage_academy
	texture = "gfx/interface/icons/building_icons/mage_academies.dds"
	city_type = city
	levels_per_mesh = 5
	
	unlocking_technologies = { 
		locked 
	}

	production_method_groups = {
		pmg_base_building_mage_academy
		pmg_damestear_mage_academy
		pmg_ownership_building_mage_academy
	}
	
	required_construction = construction_cost_very_high
}
