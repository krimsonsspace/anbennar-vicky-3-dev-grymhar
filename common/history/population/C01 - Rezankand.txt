﻿POPULATION = { #Colonial SA
	c:C01 = { #Rezankand
		
		effect_starting_pop_wealth_low = yes
		effect_starting_pop_literacy_middling = yes
	}
	
	c:C02 = { #Amadia
		
		effect_starting_pop_wealth_medium = yes
		effect_starting_pop_literacy_middling = yes
	}
	
	c:C03 = { #Ozgarom
		
		effect_starting_pop_wealth_medium = yes
		effect_starting_pop_literacy_low = yes
	}
	
	c:C04 = { #Jibirae'n
		
		effect_starting_pop_wealth_medium = yes
		effect_starting_pop_literacy_low = yes
	}
	
	c:C05 = { #Nur Dhanaenn
		
		effect_starting_pop_wealth_medium = yes
		effect_starting_pop_literacy_low = yes
	}
	
	c:C06 = { #Swamp Territories
		
		effect_starting_pop_wealth_medium = yes
		effect_starting_pop_literacy_low = yes
	}
	
	c:C07 = { #Kiohalen
		
		effect_starting_pop_wealth_medium = yes
		effect_starting_pop_literacy_low = yes
	}
	
	c:C08 = { #Drakesguard
		
		effect_starting_pop_wealth_medium = yes
		effect_starting_pop_literacy_low = yes
	}
	
	c:C09 = { #Turtleback
		
		effect_starting_pop_wealth_medium = yes
		effect_starting_pop_literacy_low = yes
	}
	
	c:C10 = { #Malatel
		
		effect_starting_pop_wealth_medium = yes
		effect_starting_pop_literacy_low = yes
	}
	
	c:C11 = { #Amadian Territory
		
		effect_starting_pop_wealth_low = yes
		effect_starting_pop_literacy_low = yes
	}
	
	c:C16 = { #Pouwhi'i
		
		effect_starting_pop_wealth_low = yes
		effect_starting_pop_literacy_low = yes
	}
}