﻿
pm_local_adventuring_parties_building_adventurers_wanted = {
	#texture = "gfx/interface/icons/production_method_icons/gold_mining.dds"
	
	building_modifiers = {
		level_scaled = {
			building_employment_adventurers_add = 1000
		}
	}
}

pm_hire_adventurer_companies_building_adventurers_wanted = {
	#texture = "gfx/interface/icons/production_method_icons/gold_mining.dds"
	
	building_modifiers = {
		level_scaled = {
			building_employment_adventurers_add = 2000
		}
		workforce_scaled = {
			# input goods
			goods_input_paper_add = 3	#its quest posters bro
		}
	}
}

pm_military_operation_building_adventurers_wanted = {
	#texture = "gfx/interface/icons/production_method_icons/gold_mining.dds"
	
	building_modifiers = {
		level_scaled = {
			building_employment_soldiers_add = 1500
			building_employment_officers_add = 500
		}
		workforce_scaled = {
			# input goods
			goods_input_small_arms_add = 1
			goods_input_ammunition_add = 1
		}
	}
}

pm_excavation_crew_building_adventurers_wanted = {
	#texture = "gfx/interface/icons/production_method_icons/gold_mining.dds"
	
	building_modifiers = {
		level_scaled = {
			building_employment_laborers_add = 1500
			building_employment_machinists_add = 500
		}
		workforce_scaled = {
			# input goods
			goods_input_explosives_add = 1
			goods_input_tools_add = 2
		}
	}
}

pm_internal_agents_building_adventurers_wanted = {
	#texture = "gfx/interface/icons/production_method_icons/gold_mining.dds"
	
	building_modifiers = {
		level_scaled = {
			building_employment_bureaucrats_add = 500
			building_employment_soldiers_add = 1000
			building_employment_officers_add = 500
		}
		workforce_scaled = {
			# input goods
			goods_input_paper_add = 1
			goods_input_tools_add = 2
		}
	}
}

pm_local_support_building_adventurers_wanted = {
	#texture = "gfx/interface/icons/production_method_icons/gold_mining.dds"
	
	building_modifiers = {
		level_scaled = {
			building_employment_adventurers_add = 250
			building_employment_soldiers_add = 500
			building_soldiers_mortality_mult = 0.5
		}
	}
}

pm_internal_agents_building_adventurers_wanted = {
	#texture = "gfx/interface/icons/production_method_icons/gold_mining.dds"
	
	building_modifiers = {
		level_scaled = {
			building_employment_bureaucrats_add = 250
			building_employment_soldiers_add = 500
			building_employment_officers_add = 50
		}
	}
}



pm_unregulated_loot_building_adventurers_wanted = {
	texture = "gfx/interface/icons/production_method_icons/privately_owned.dds" 

	building_modifiers = {
		level_scaled = {
		}
		unscaled = {
			building_adventurers_shares_add = 5
		}
	}
}

pm_regulated_distribution_building_adventurers_wanted = {
	texture = "gfx/interface/icons/production_method_icons/publicly_traded.dds"

	building_modifiers = {
		level_scaled = {
			building_employment_clerks_add = 200
		}
		unscaled = {
			building_adventurers_shares_add = 2
			building_clerks_shares_add = 2
		}
	}
}

pm_government_red_tape_building_adventurers_wanted = {
	texture = "gfx/interface/icons/production_method_icons/government_run.dds"

	building_modifiers = {
		level_scaled = {
			building_employment_clerks_add = 200
			building_employment_bureaucrats_add = 200
		}
		unscaled = {
			building_bureaucrats_shares_add = 5
		}
	}
}


#List of Adventurers Wanted type

#Default Template
default_building_adventurers_wanted = {
	#texture = "gfx/interface/icons/production_method_icons/gold_mining.dds"
	
	building_modifiers = {
		workforce_scaled = {
			goods_output_damestear_add = 5
		}
		#level_scaled = {
		#	building_employment_adventurers_add = 500
		#}
		unscaled = {
			building_adventurers_mortality_mult = 0.25
			building_soldiers_mortality_mult = 0.25
		}
	}

	country_modifiers = {
		#workforce_scaled = {
		#	country_minting_add = 100
		#}
	}

	state_modifiers = {
		unscaled = {
			state_infrastructure_add = -10	#standard malus is these two
			state_tax_capacity_add = -10
		}
	}
}

#Combat Type (influences generic pms above)

combat_ancient_catacombs_building_adventurers_wanted = {
	#texture = "gfx/interface/icons/production_method_icons/gold_mining.dds"
	
	building_modifiers = {
		workforce_scaled = {
			goods_output_gold_add = 5
			goods_output_relics_add = 5
			goods_output_magical_reagents_add = 30
			goods_output_damestear_add = 5
		}
		#level_scaled = {
		#	building_employment_adventurers_add = 500
		#}
		unscaled = {
			building_adventurers_mortality_mult = 0.25
			building_soldiers_mortality_mult = 0.25
		}
	}

	country_modifiers = {
		workforce_scaled = {
			country_minting_add = 100
		}
	}

	state_modifiers = {
		unscaled = {
			state_infrastructure_add = -10	#standard malus is these two
			state_tax_capacity_add = -10
		}
	}
}

combat_dragon_building_adventurers_wanted = {
	#texture = "gfx/interface/icons/production_method_icons/gold_mining.dds"
	
	building_modifiers = {
		workforce_scaled = {
			goods_output_gold_add = 10	#dragon hoard
			goods_output_magical_reagents_add = 100
		}
		level_scaled = {
			building_employment_adventurers_add = 1000
		}
		unscaled = {
			building_adventurers_mortality_mult = 2
		}
	}

	country_modifiers = {
		workforce_scaled = {
			country_minting_add = 200
		}
	}

	state_modifiers = {
		workforce_scaled = {
			state_mortality_mult = -0.5
		}
		unscaled = {
			state_infrastructure_add = -30	#standard malus is these two
			state_tax_capacity_add = -30
			state_migration_pull_mult = -0.75
			state_mortality_mult = 1
		}
	}
}