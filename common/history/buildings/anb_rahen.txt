﻿BUILDINGS={
	s:STATE_AVHAVUBHIYA = {
		region_state:R01 = {
		
			create_building={
				building="building_port"
				level=3
				reserves=1
				activate_production_methods={ "pm_basic_port" }
			}
			create_building={
				building="building_fishing_wharf"
				level=2
				reserves=1
				activate_production_methods={ "pm_fishing_trawlers" "pm_unrefrigerated" "pm_merchant_guilds_building_fishing_wharf" }
			}
			create_building={
				building="building_naval_base"
				level=10
				reserves=1
				activate_production_methods={ "pm_no_naval_theory" }
			}
			create_building={
				building="building_shipyards"
				level=3
				reserves=1
				activate_production_methods={ "pm_basic_shipbuilding" "pm_merchant_guilds_building_shipyards" }
			}
			create_building={
				building="building_sugar_plantation"
				level=1
				reserves=1
				activate_production_methods={ "default_building_sugar_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
		}
	}
	
	s:STATE_IYARHASHAR = {
		region_state:R01 = {
		
			create_building={
				building="building_fishing_wharf"
				level=1
				reserves=1
				activate_production_methods={ "pm_merchant_guilds_building_fishing_wharf" "pm_unrefrigerated" "pm_fishing_trawlers" }
			}
			create_building={
				building="building_sugar_plantation"
				level=2
				reserves=1
				activate_production_methods={ "default_building_sugar_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
		}
	}
	
	s:STATE_SOUTH_GHANKEDHEN = {
		region_state:R02 = {
			create_building={
				building="building_government_administration"
				level=5
				reserves=1
				activate_production_methods={ "pm_horizontal_drawer_cabinets" }
			}
			create_building={
				building="building_iron_mine"
				level=2
				reserves=1
				activate_production_methods={ "pm_picks_and_shovels_building_iron_mine" "pm_no_explosives" "pm_no_steam_automation" "pm_road_carts" "pm_merchant_guilds_building_iron_mine" }
			}
			create_building={
				building="building_construction_sector"
				level=2
				reserves=1
				activate_production_methods={ "pm_iron_frame_buildings" }
			}
			create_building={
				building="building_coal_mine"
				level=1
				reserves=1
				activate_production_methods={ "pm_picks_and_shovels_building_coal_mine" "pm_no_explosives" "pm_no_steam_automation" "pm_road_carts" "pm_merchant_guilds_building_coal_mine" }
			}
			create_building={
				building="building_arms_industry"
				level=2
				reserves=1
				activate_production_methods={ "pm_muskets" "pm_merchant_guilds_building_arms_industry" }
			}
			create_building={
				building="building_livestock_ranch"
				level=1
				reserves=1
				activate_production_methods={ "pm_simple_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_open_air_stockyards" }
			}
			create_building={
				building="building_opium_plantation"
				level=2
				reserves=1
				activate_production_methods={ "default_building_opium_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_barracks"
				level=10
				reserves=1
				activate_production_methods={ "pm_no_organization" }
			}
		}
	}
	
	s:STATE_NORTH_GHANKEDHEN = {
		region_state:R02 = {
			create_building={
				building="building_government_administration"
				level=2
				reserves=1
				activate_production_methods={ "pm_horizontal_drawer_cabinets" }
			}
			create_building={
				building="building_textile_mills"
				level=2
				reserves=1
				activate_production_methods={ "pm_dye_workshops" "pm_merchant_guilds_building_textile_mills" "pm_traditional_looms" "pm_craftsman_sewing" }
			}
			create_building={
				building="building_iron_mine"
				level=3
				reserves=1
				activate_production_methods={ "pm_picks_and_shovels_building_iron_mine" "pm_no_explosives" "pm_no_steam_automation" "pm_road_carts" "pm_merchant_guilds_building_iron_mine" }
			}
			create_building={
				building="building_lead_mine"
				level=1
				reserves=1
				activate_production_methods={ "pm_picks_and_shovels_building_lead_mine" "pm_no_explosives" "pm_no_steam_automation" "pm_road_carts" "pm_merchant_guilds_building_lead_mine" }
			}
			create_building={
				building="building_livestock_ranch"
				level=2
				reserves=1
				activate_production_methods={ "pm_simple_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_open_air_stockyards" }
			}
			create_building={
				building="building_barracks"
				level=15
				reserves=1
				activate_production_methods={ "pm_no_organization" }
			}
		}
	}
	
	s:STATE_TUDHINA = {
		region_state:R03 = {
			create_building={
				building="building_livestock_ranch"
				level=1
				reserves=1
				activate_production_methods={ "pm_simple_ranch" "pm_open_air_stockyards" "pm_standard_fences" "pm_unrefrigerated" "pm_privately_owned" }
			}
			create_building={
				building="building_barracks"
				level=10
				reserves=1
				activate_production_methods={ "pm_no_organization" }
			}
		}
	}
	
	s:STATE_PASIRAGHA = {
		region_state:R01 = {
			create_building={
				building="building_government_administration"
				level=6
				reserves=1
				activate_production_methods={ "pm_horizontal_drawer_cabinets" }
			}
			create_building={
				building="building_university"
				level=1
				reserves=1
				activate_production_methods={ "pm_religious_academia" }
			}
			create_building={
				building="building_iron_mine"
				level=1
				reserves=1
				activate_production_methods={ "pm_picks_and_shovels_building_iron_mine" "pm_no_explosives" "pm_no_steam_automation" "pm_road_carts" "pm_merchant_guilds_building_iron_mine" }
			}
			create_building={
				building="building_textile_mills"
				level=1
				reserves=1
				activate_production_methods={ "pm_dye_workshops" "pm_craftsman_sewing" "pm_traditional_looms" "pm_merchant_guilds_building_textile_mills" }
			}
			create_building={
				building="building_paper_mills"
				level=2
				reserves=1
				activate_production_methods={ "pm_pulp_pressing" "pm_automation_disabled" "pm_merchant_guilds_building_paper_mills" }
			}
			create_building={
				building="building_cotton_plantation"
				level=2
				reserves=1
				activate_production_methods={ "default_building_cotton_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_tobacco_plantation"
				level=1
				reserves=1
				activate_production_methods={ "default_building_tobacco_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_barracks"
				level=15
				reserves=1
				activate_production_methods={ "pm_no_organization" }
			}
			create_building={
				building="building_barracks"
				level=5
				reserves=1
				activate_production_methods={ "pm_no_organization" }
			}
		}
	}
	
	s:STATE_UPPER_DHENBASANA = {
		region_state:R01 = {
			create_building={
				building="building_government_administration"
				level=8
				reserves=1
				activate_production_methods={ "pm_horizontal_drawer_cabinets" }
			}
			create_building={
				building="building_paper_mills"
				level=5
				reserves=1
				activate_production_methods={ "pm_pulp_pressing" "pm_automation_disabled" "pm_merchant_guilds_building_paper_mills" }
			}
			create_building={
				building="building_dye_plantation"
				level=2
				reserves=1
				activate_production_methods={ "default_building_dye_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_cotton_plantation"
				level=5
				reserves=1
				activate_production_methods={ "default_building_cotton_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_textile_mills"
				level=1
				reserves=1
				activate_production_methods={ "pm_dye_workshops" "pm_craftsman_sewing" "pm_traditional_looms" "pm_merchant_guilds_building_textile_mills" }
			}
			create_building={
				building="building_tobacco_plantation"
				level=1
				reserves=1
				activate_production_methods={ "default_building_tobacco_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_logging_camp"
				level=1
				reserves=1
				activate_production_methods={ "pm_simple_forestry" "pm_merchant_guilds_building_logging_camp" "pm_no_equipment" "pm_road_carts" "pm_no_hardwood" }
			}
			create_building={
				building="building_barracks"
				level=20
				reserves=1
				activate_production_methods={ "pm_no_organization" }
			}
		}
	}
	
	s:STATE_LOWER_DHENBASANA = {
		region_state:R01 = {
			create_building={
				building="building_government_administration"
				level=3
				reserves=1
				activate_production_methods={ "pm_horizontal_drawer_cabinets" }
			}
			create_building={
				building="building_silk_plantation"
				level=5
				reserves=1
				activate_production_methods={ "default_building_silk_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_logging_camp"
				level=2
				reserves=1
				activate_production_methods={ "pm_simple_forestry" "pm_merchant_guilds_building_logging_camp" "pm_no_equipment" "pm_road_carts" "pm_no_hardwood" }
			}
			create_building={
				building="building_barracks"
				level=15
				reserves=1
				activate_production_methods={ "pm_no_organization" }
			}
		
		}
	}
	
	s:STATE_ASCENSION_JUNGLE = {
		region_state:R04 = {
		
			create_building={
				building="building_fishing_wharf"
				level=1
				reserves=1
				activate_production_methods={ "pm_simple_fishing" "pm_unrefrigerated" "pm_merchant_guilds_building_fishing_wharf" }
			}
			create_building={
				building="building_logging_camp"
				level=1
				reserves=1
				activate_production_methods={ "pm_simple_forestry" "pm_merchant_guilds_building_logging_camp" "pm_no_equipment" "pm_road_carts" "pm_no_hardwood" }
			}
			create_building={
				building="building_shipyards"
				level=1
				reserves=1
				activate_production_methods={ "pm_basic_shipbuilding" "pm_merchant_guilds_building_shipyards"  }
			}
			create_building={
				building="building_naval_base"
				level=15
				reserves=1
				activate_production_methods={ "pm_no_naval_theory" }
			}
			create_building={
				building="building_port"
				level=3
				reserves=1
				activate_production_methods={ "pm_basic_port" }
			}
			create_building={
				building="building_barracks"
				level=15
				reserves=1
				activate_production_methods={ "pm_no_organization" }
			}
		}
	}
	
	s:STATE_TUJGAL = {
		region_state:R04 = {
		
			create_building={
				building="building_fishing_wharf"
				level=2
				reserves=1
				activate_production_methods={ "pm_simple_fishing" "pm_unrefrigerated" "pm_merchant_guilds_building_fishing_wharf" }
			}
			create_building={
				building="building_sugar_plantation"
				level=2
				reserves=1
				activate_production_methods={ "default_building_sugar_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_port"
				level=1
				reserves=1
				activate_production_methods={ "pm_basic_port" }
			}
			create_building={
				building="building_barracks"
				level=20
				reserves=1
				activate_production_methods={ "pm_no_organization" }
			}
		}
	}
	
	s:STATE_BABHAGAMA = {
		region_state:R06 = {
		
			create_building={
				building="building_cotton_plantation"
				level=2
				reserves=1
				activate_production_methods={ "default_building_cotton_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_barracks"
				level=10
				reserves=1
				activate_production_methods={ "pm_no_organization" }
			}
		}
	}
	
	s:STATE_SATARSAYA = {
		region_state:R01 = {
			create_building={
				building="building_wheat_farm"
				level=3
				reserves=1
				activate_production_methods={ "pm_simple_farming" "pm_privately_owned" "pm_tools_disabled" "pm_no_secondary" }
			}
			create_building={
				building="building_cotton_plantation"
				level=2
				reserves=1
				activate_production_methods={ "default_building_cotton_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
		}
	}
	
	s:STATE_WEST_GHAVAANAJ = {
		region_state:R72 = {
			
			create_building={
				building="building_barracks"
				level=10
				reserves=1
				activate_production_methods={ "pm_no_organization" }
			}
		}
	}
	
	s:STATE_EAST_GHAVAANAJ = {
		region_state:R72 = {
		
			create_building={
				building="building_government_administration"
				level=4
				reserves=1
				activate_production_methods={ "pm_horizontal_drawer_cabinets" }
			}
			create_building={
				building="building_dye_plantation"
				level=4
				reserves=1
				activate_production_methods={ "default_building_dye_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_barracks"
				level=15
				reserves=1
				activate_production_methods={ "pm_no_organization" }
			}
		}
		region_state:R12 = {
		
			create_building={
				building="building_barracks"
				level=5
				reserves=1
				activate_production_methods={ "pm_no_organization" }
			}
		}
	}
	
	s:STATE_TUGHAYASA = {
		region_state:R09 = {
		
		}
		region_state:R13 = {
		
		}
	}
	
	s:STATE_DHUJAT = {
		region_state:R10 = {
			create_building={
				building="building_government_administration"
				level=4
				reserves=1
				activate_production_methods={ "pm_horizontal_drawer_cabinets" }
			}
			create_building={
				building="building_dye_plantation"
				level=1
				reserves=1
				activate_production_methods={ "default_building_dye_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_glassworks"
				level=2
				reserves=1
				activate_production_methods={ "pm_forest_glass"  "pm_ceramics" "pm_manual_glassblowing" "pm_privately_owned_building_glassworks" }
			}
		}
		region_state:R11 = {
			create_building={
				building="building_dye_plantation"
				level=1
				reserves=1
				activate_production_methods={ "default_building_dye_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_glassworks"
				level=1
				reserves=1
				activate_production_methods={ "pm_forest_glass"  "pm_ceramics" "pm_manual_glassblowing" "pm_privately_owned_building_glassworks" }
			}
		}
	}
	s:STATE_SARISUNG = {
		region_state:R13 = {
			create_building={
				building="building_government_administration"
				level=7
				reserves=1
				activate_production_methods={ "pm_horizontal_drawer_cabinets" }
			}
			create_building={
				building="building_glassworks"
				level=2
				reserves=1
				activate_production_methods={ "pm_forest_glass"  "pm_ceramics" "pm_manual_glassblowing" "pm_privately_owned_building_glassworks" }
			}
		}
	}
	
	s:STATE_TILTAGHAR = {
		region_state:R07 = {
		
		}
	}
	
	s:STATE_WEST_NADIMRAJ = {
		region_state:R07 = {
		
			create_building={
				building="building_government_administration"
				level=2
				reserves=1
				activate_production_methods={ "pm_horizontal_drawer_cabinets" }
			}
		}
	}
	
	s:STATE_RAJNADHAGA = {
		region_state:R07 = {
		
			create_building={
				building="building_government_administration"
				level=7
				reserves=1
				activate_production_methods={ "pm_horizontal_drawer_cabinets" }
			}
		}
	}
	
	s:STATE_CENTRAL_NADIMRAJ = {
		region_state:R07 = {
		
			create_building={
				building="building_government_administration"
				level=4
				reserves=1
				activate_production_methods={ "pm_horizontal_drawer_cabinets" }
			}
		}
	}
	
	s:STATE_EAST_NADIMRAJ = {
		region_state:R07 = {
		
			create_building={
				building="building_government_administration"
				level=5
				reserves=1
				activate_production_methods={ "pm_horizontal_drawer_cabinets" }
			}
		}
	}
	
	s:STATE_HOBGOBLIN_HOMELANDS = {
		region_state:R08 = {
		
			create_building={
				building="building_government_administration"
				level=4
				reserves=1
				activate_production_methods={ "pm_horizontal_drawer_cabinets" }
			}
		}
	}
	
	s:STATE_GHILAKHAD = {
		region_state:R14 = {
		
		}
	}
	
	s:STATE_RAGHAJANDI = {
		region_state:R08 = {
		
			create_building={
				building="building_government_administration"
				level=7
				reserves=1
				activate_production_methods={ "pm_horizontal_drawer_cabinets" }
			}
		}
	}
	
	s:STATE_GHATASAK = {
		region_state:R17 = {
		
		}
	}
	
	s:STATE_SHAMAKHAD_PLAINS = {
		region_state:R15 = {
		
		}
		region_state:R16 = {
		
		}
		region_state:R18 = {
		
			create_building={
				building="building_government_administration"
				level=1
				reserves=1
				activate_production_methods={ "pm_horizontal_drawer_cabinets" }
			}
		}
	}
	
	s:STATE_SIR = {
		region_state:R19 = {
		
			create_building={
				building="building_government_administration"
				level=4
				reserves=1
				activate_production_methods={ "pm_horizontal_drawer_cabinets" }
			}
		}
	}
	
	s:STATE_SRAMAYA = {
		region_state:R05 = {
			create_building={
				building="building_government_administration"
				level=7
				reserves=1
				activate_production_methods={ "pm_simple_organization" }
			}
			create_building={
				building="building_paper_mills"
				level=5
				reserves=1
				activate_production_methods={ "pm_pulp_pressing" "pm_automation_disabled" "pm_merchant_guilds_building_paper_mills" }
			}
			create_building={
				building="building_port"
				level=3
				reserves=1
				activate_production_methods={ "pm_basic_port" }
			}
			create_building={
				building="building_shipyards"
				level=3
				reserves=1
				activate_production_methods={ "pm_basic_shipbuilding" "pm_merchant_guilds_building_shipyards" }
			}
			create_building={
				building="building_naval_base"
				level=15
				reserves=1
				activate_production_methods={ "pm_no_naval_theory" }
			}
			create_building={
				building="building_glassworks"
				level=2
				reserves=1
				activate_production_methods={ "pm_forest_glass" "pm_merchant_guilds_building_glassworks" "pm_manual_glassblowing" "pm_ceramics" }
			}
			create_building={
				building="building_textile_mills"
				level=8
				reserves=1
				activate_production_methods={ "pm_dye_workshops" "pm_merchant_guilds_building_textile_mills" "pm_traditional_looms" "pm_craftsman_sewing" }
			}
			create_building={
				building="building_fishing_wharf"
				level=3
				reserves=1
				activate_production_methods={ "pm_simple_fishing" "pm_unrefrigerated" "pm_merchant_guilds_building_fishing_wharf" }
			}
			create_building={
				building="building_barracks"
				level=20
				reserves=1
				activate_production_methods={ "pm_no_organization" }
			}
		}
		region_state:Y32 = {
		
			create_building={
				building="building_port"
				level=2
				reserves=1
				activate_production_methods={ "pm_basic_port" }
			}
		}
	}
}