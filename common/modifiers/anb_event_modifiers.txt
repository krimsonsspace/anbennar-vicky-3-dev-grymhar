﻿modifier_surveying_dhal_nikhuvad = {
	icon = gfx/interface/icons/timed_modifier_icons/modifier_documents_negative.dds
 	country_bureaucracy_add = -1000
 }

 dhal_nikhuvad_purchase = {
	icon = gfx/interface/icons/timed_modifier_icons/modifier_coins_negative.dds
	country_expenses_add = 33000
}

dhal_nikhuvad_sale = {
	icon = gfx/interface/icons/timed_modifier_icons/modifier_coins_positive.dds
	country_tax_income_add = 33000
}