﻿# goods types, organized by category

# prestige_factor							Base prestige for occupying the rank MIN_PRESTIGE_AWARD spot on the goods production leaderboard. x2 awarded for every rank above the minimum.

damestear = {
	texture = "gfx/interface/icons/goods_icons/damestearnew.dds"
	cost = 50 # brought down from 100 so I could experiment with the artificing economy, if that's ok. 
	# when the price is at 100 it's impossible to make damestear-using industries profitable
	# plus there's no goods in the game at 100 besides gold. this puts damestear at the same level as steel + sulfur
	category = industrial
	
	prestige_factor = 5
}

artificery_doodads = {
	texture = "gfx/interface/icons/goods_icons/artificery_doodads.dds"	#NOTE NOTE NOTE NOTE - whenever this is changed you need to change it in anb_texticons.gui
	cost = 50 # brought down from 60 for balancing reasons
	# 50 is equivalent to steel and explosives and sulfur; i think that's a good base point 
	# hard to make too many things use lots of doodads when they're expensive
	category = industrial	#so people can obsess them plus rich people want artificery
	# will implement artificery doodads as a pop-desired good at a later date
	obsession_chance = 1.0
	prestige_factor = 7
	
	consumption_tax_cost = 200
}

relics = {
	texture = "gfx/interface/icons/goods_icons/damestear.dds"
	cost = 70 # used to be 100; HAS to be lower for them to be usable anywhere
	category = luxury 
	
	prestige_factor = 5
}

magical_reagents = {
	texture = "gfx/interface/icons/goods_icons/magical_reagents.dds"
	cost = 40 # reduced from 50 b/c they're not as valuable as, e.x., doodads or damestear
	category = staple
	
	prestige_factor = 5
}

spirit_energy = {
	texture = "gfx/interface/icons/goods_icons/damestear.dds"
	cost = 30 # equal to electricity. it's cheap if you have it, impossible if you don't 
	category = industrial	
}

perfect_metal = {
	texture = "gfx/interface/icons/goods_icons/damestear.dds"
	cost = 70 # mithril was already like this
	category = industrial
	
	prestige_factor = 10
	convoy_cost_multiplier = 0.75	#its light
}

automata = {
	texture = "gfx/interface/icons/goods_icons/damestear.dds"
	cost = 70 # equal to porcelain, telephones, steamers, artillery; powerful manufactured goods
	category = industrial	#so people can obsess them plus rich people want artificery
	
	obsession_chance = 1.0
	prestige_factor = 7
	
	consumption_tax_cost = 200
}

commercial_skyships = {
	texture = "gfx/interface/icons/goods_icons/damestear.dds"
	cost = 80
	category = industrial
	
	prestige_factor = 7
	traded_quantity = 3
	convoy_cost_multiplier = 0.5	#you can fly them
}

military_skyships = {
	texture = "gfx/interface/icons/goods_icons/damestear.dds"
	cost = 100
	category = military
	
	prestige_factor = 7
	traded_quantity = 3
	convoy_cost_multiplier = 0.5	#you can fly them
}