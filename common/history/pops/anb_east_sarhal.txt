﻿POPS = {
	s:STATE_MENGABOYS_1 = {
		region_state:L01 = {
			create_pop = {
				culture = yeteferen	
				size = 3000000	#temp numbers for build stability
			}
		}
	}

	s:STATE_MENGABOYS_2 = {
		region_state:L01 = {
			create_pop = {
				culture = metobesebi
				size = 1000000 #temp numbers for build stability
			}
		}
	}

	s:STATE_MENGABOYS_3 = {
		region_state:L01 = {
			create_pop = {
				culture = yeteferen
				size = 500000 #temp numbers for build stability
			}
		}
	}

	s:STATE_MENGABOYS_4 = {
		region_state:L01 = {
			create_pop = {
				culture = metobesebi
				size = 500000 #temp numbers for build stability
			}
		}
	}

	s:STATE_MENGABOYS_4 = {
		region_state:L01 = {
			create_pop = {
				culture = metobesebi
				size = 500000 #temp numbers for build stability
			}
		}
	}

	s:STATE_MENGABOYS_5 = {
		region_state:L01 = {
			create_pop = {
				culture = metobesebi
				size = 500000 #temp numbers for build stability
			}
		}
	}

	s:STATE_MENGABOYS_6 = {
		region_state:L01 = {
			create_pop = {
				culture = yeteferen
				size = 500000 #temp numbers for build stability
			}
		}
	}

	s:STATE_MENGABOYS_7 = {
		region_state:L01 = {
			create_pop = {
				culture = yeteferen
				size = 500000 #temp numbers for build stability
			}
		}
	}

	s:STATE_MENGABOYS_8 = {
		region_state:L01 = {
			create_pop = {
				culture = yeteferen
				size = 1000000 #temp numbers for build stability
			}
		}
	}

	s:STATE_MENGABOYS_9 = {
		region_state:L01 = {
			create_pop = {
				culture = yeteferen
				size = 1000000 #temp numbers for build stability
			}
		}
	}

	s:STATE_MENGABOYS_10 = {
		region_state:L01 = {
			create_pop = {
				culture = yeteferen
				size = 1000000 #temp numbers for build stability
			}
		}
	}

	s:STATE_MENGABOYS_11 = {
		region_state:L01 = {
			create_pop = {
				culture = yeteferen
				size = 500000 #temp numbers for build stability
			}
			create_pop = {
				culture = sitewosi
				size = 500000 #temp numbers for build stability
			}
		}
	}

	s:STATE_MENGABOYS_12 = {
		region_state:L01 = {
			create_pop = {
				culture = sitewosi
				size = 500000 #temp numbers for build stability
			}
		}
	}

	s:STATE_MENGABOYS_13 = {
		region_state:L01 = {
			create_pop = {
				culture = sitewosi
				size = 500000 #temp numbers for build stability
			}
		}
	}

	s:STATE_MENGABOYS_14 = {
		region_state:L01 = {
			create_pop = {
				culture = sitewosi
				size = 500000 #temp numbers for build stability
			}
		}
	}

	s:STATE_MENGABOYS_14 = {
		region_state:L01 = {
			create_pop = {
				culture = talilibeti
				size = 500000 #temp numbers for build stability
			}
		}
	}

	s:STATE_MENGABOYS_15 = {
		region_state:L01 = {
			create_pop = {
				culture = talilibeti
				size = 500000 #temp numbers for build stability
			}
		}
	}

	s:STATE_MENGABOYS_16 = {
		region_state:L01 = {
			create_pop = {
				culture = ofehibi
				size = 1000000 #temp numbers for build stability
			}
		}
	}

	s:STATE_MENGABOYS_17 = {
		region_state:L01 = {
			create_pop = {
				culture = ofehibi
				size = 3000000 #temp numbers for build stability
			}
		}
	}

	s:STATE_MENGABOYS_18 = {
		region_state:L01 = {
			create_pop = {
				culture = ofehibi
				size = 100000 #temp numbers for build stability
			}
			create_pop = {
				culture = talilibeti
				size = 500000 #temp numbers for build stability
			}
		}
	}

	s:STATE_MENGABOYS_19 = {
		region_state:L03 = {
			create_pop = {
				culture = ofehibi
				size = 2000000 #temp numbers for build stability
			}
		}
	}

	s:STATE_MENGABOYS_20 = {
		region_state:L04 = {
			create_pop = {
				culture = ofehibi
				size = 1000000 #temp numbers for build stability
			}
			create_pop = {
				culture = nalenian_harpy
				size = 1000000 #temp numbers for build stability
			}
		}
	}

	s:STATE_MENGABOYS_22 = {
		region_state:L02 = {
			create_pop = {
				culture = ofehibi
				size = 3000000 #temp numbers for build stability
			}
		}
	}

	s:STATE_MENGABOYS_23 = {
		region_state:L02 = {
			create_pop = {
				culture = ofehibi
				size = 4000000 #temp numbers for build stability
			}
		}
	}

	s:STATE_BERI_GNIDI = {
		region_state:L03 = {
			create_pop = {
				culture = ofehibi
				size = 3000000 #temp numbers for build stability
			}
		}
	}

	s:STATE_DASMATUS = {
		region_state:L03 = {
			create_pop = {
				culture = maqeti
				size = 1000000 #temp numbers for build stability
			}
		}
	}

	s:STATE_GREATER_GIZAN = {
		region_state:L03 = {
			create_pop = {
				culture = fieldstalker_gnoll
				size = 1000000 #temp numbers for build stability
			}
		}
	}

	s:STATE_HARAAGTSEDA = {
		region_state:L03 = {
			create_pop = {
				culture = fieldstalker_gnoll
				size = 3000000 #temp numbers for build stability
			}
			create_pop = {
				culture = echenka
				size = 100000 #temp numbers for build stability
			}
		}
	}

	s:STATE_KOGZALLA = {
		region_state:L03 = {
			create_pop = {
				culture = fieldstalker_gnoll
				size = 500000 #temp numbers for build stability
			}
		}
	}

	s:STATE_MENGABOYS_29 = {
		region_state:F16 = {
			create_pop = {
				culture = fieldstalker_gnoll
				size = 500000 #temp numbers for build stability
			}
			create_pop = {
				culture = echenka
				size = 500000 #temp numbers for build stability
			}
		}
	}

	s:STATE_MENGABOYS_30 = {
		region_state:L01 = {
			create_pop = {
				culture = swamp_troll
				size = 100000 #temp numbers for build stability
			}
		}
	}

	s:STATE_MENGABOYS_31 = {
		region_state:L01 = {
			create_pop = {
				culture = swamp_troll
				size = 100000 #temp numbers for build stability
			}
			create_pop = {
				culture = echenka
				size = 10000 #temp numbers for build stability
			}
		}
	}

	s:STATE_MENGABOYS_32 = {
		region_state:L01 = {
			create_pop = {
				culture = swamp_troll
				size = 100000 #temp numbers for build stability
			}
			create_pop = {
				culture = echenka
				size = 1000000 #temp numbers for build stability
			}
		}
	}

	s:STATE_MENGABOYS_33 = {
		region_state:L01 = {
			create_pop = {
				culture = swamp_troll
				size = 300000 #temp numbers for build stability
			}
		}
	}
}