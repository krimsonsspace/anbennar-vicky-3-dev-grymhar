﻿STATE_KHARUNYANA_BOMDAN = {
    id = 460
    subsistence_building = "building_subsistence_rice_paddies"
    provinces = { "x01E61E" "x040EE1" "x26E324" "x2C45FA" "x548996" "x7CB0AF" "xA34596" "xB62185" "xB71A0A" "xCB89AF" "xF245C8" }
    prime_land = { "x01E61E" "x040EE1" "x26E324" "x7CB0AF" "xA34596" "xCB89AF" }
    traits = { state_trait_kharunyana_river state_trait_south_rahen_river_basin state_trait_porcelain_cities }
    city = ""
    farm = ""
    mine = ""
    wood = ""
    arable_land = 341
    arable_resources = { bg_rice_farms bg_livestock_ranches bg_banana_plantations bg_cotton_plantations bg_dye_plantations bg_silk_plantations bg_sugar_plantations bg_tea_plantations }
    capped_resources = {
        bg_logging = 11
        bg_haless_spirits = 1
    }
}

STATE_LOWER_TELEBEI = {
    id = 461
    subsistence_building = "building_subsistence_rice_paddies"
    provinces = { "x077923" "x76F14E" "x926F1C" "x9B3716" "xB82A45" "xF05901" }
    traits = { state_trait_bomdan_jungles state_trait_telebei_river }
    city = ""
    port = ""
    farm = ""
    mine = ""
    wood = ""
    arable_land = 69
    arable_resources = { bg_rice_farms bg_livestock_ranches bg_banana_plantations bg_cotton_plantations bg_dye_plantations bg_silk_plantations bg_sugar_plantations bg_tea_plantations }
    capped_resources = {
        bg_logging = 18
        bg_fishing = 14
        bg_haless_spirits = 1
    }
    resource = {
        type = "bg_rubber"
        undiscovered_amount = 1
    }
    naval_exit_id = 3301
}

STATE_BIM_LAU = {
    id = 462
    subsistence_building = "building_subsistence_rice_paddies"
    provinces = { "x7b0efa" "xa40ec8" "xc90ec8" "xe3107c" "xd12d3b" "x1e5cea" "x06867e" "xcc45e1" "xbf5ca9" "x2b89c8" "x05b096" "xea7a58" "xa289e1" "x53b0e1" "x99aaa5" "xf489fa" "xa1b0af" "xb8b90f" "xcab0fa" "xf3b0c8" }
    # prime_land = { "xE3107C" }
    traits = { state_trait_telebei_river }
    city = ""
    farm = ""
    mine = ""
    wood = ""
    arable_land = 282
    arable_resources = { bg_rice_farms bg_livestock_ranches bg_banana_plantations bg_cotton_plantations bg_dye_plantations bg_silk_plantations bg_sugar_plantations bg_tea_plantations }
    capped_resources = {
        bg_logging = 17
        bg_haless_spirits = 1
    }
}

STATE_JIEZHONG = {
    id = 463
    subsistence_building = "building_subsistence_rice_paddies"
    provinces = { "x003F8C" "x0689C8" "x0745FA" "x08CD80" "x2D208A" "x370EAF" "x42B0E1" "x5545C8" "x7D89E1" "xA5B0FA" "xCD0E96" "xD1CAEB" "xE91771" "xF545AF" }
    traits = { state_trait_xianjie_hills }
    city = ""
    farm = ""
    mine = ""
    wood = ""
    arable_land = 128
    arable_resources = { bg_rice_farms bg_livestock_ranches bg_cotton_plantations bg_silk_plantations bg_tea_plantations }
    capped_resources = {
        bg_logging = 17
        bg_haless_spirits = 1
        bg_iron_mining = 40
        bg_coal_mining = 40
    }
}

STATE_QIANZHAOLIN = {
    id = 464
    subsistence_building = "building_subsistence_rice_paddies"
    provinces = { "x018996" "x05D214" "x092C12" "x2E0E88" "x438996" "x53A1D4" "x560EFA" "x57B0AF" "x7E4596" "xA689AF" "xA745E1" "xC5CBF6" "xCEB0C8" "xE2C781" "xF7B096" }
    traits = { state_trait_xianjie_hills }
    city = ""
    farm = ""
    mine = ""
    wood = ""
    arable_land = 104
    arable_resources = { bg_rice_farms bg_livestock_ranches bg_silk_plantations bg_tea_plantations bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 19
        bg_haless_spirits = 1
        bg_iron_mining = 20
    }
}

STATE_WANGQIU = {
    id = 465
    subsistence_building = "building_subsistence_rice_paddies"
    provinces = { "x0E6142" "x4E03DB" "x5CFE5C" "x5E685A" "x61AC3C" "x658AB9" "x663CC1" "x8B029B" "x9333C3" "x95A358" "x95F128" "xACEF61" "xAD9611" "xB4FC51" "xBC9991" "xC59F10" "xDE8548" "xE88FDB" "xF9A2D9" }
    traits = { state_trait_xianjie_hills }
    city = ""
    farm = ""
    mine = ""
    wood = ""
    arable_land = 59
    arable_resources = { bg_rice_farms bg_livestock_ranches bg_silk_plantations bg_tea_plantations bg_tobacco_plantations bg_vineyard_plantations }
    capped_resources = {
        bg_logging = 20
        bg_haless_spirits = 1
        bg_iron_mining = 60
        bg_coal_mining = 60
    }
}

STATE_SIKAI = {
    id = 466
    subsistence_building = "building_subsistence_rice_paddies"
    provinces = { "x1C41AF" "x2348CF" "x390622" "x3CE1BB" "x494207" "x516B0E" "x51E42A" "x55393B" "x5F2A9B" "x747F98" "x8CE0B7" "x8D4895" "xA1B834" "xA2A9DF" "xA9DBF6" "xBBC7B2" "xBF52AD" "xC8AE78" "xCAFC01" "xDA6DD2" "xE172EC" "xFA403B" "xFEE704" }
    traits = { state_trait_telebei_river }
    city = ""
    farm = ""
    mine = ""
    wood = ""
    arable_land = 365
    arable_resources = { bg_rice_farms bg_livestock_ranches bg_cotton_plantations bg_opium_plantations bg_silk_plantations bg_tea_plantations }
    capped_resources = {
        bg_logging = 13
        bg_haless_spirits = 1
    }
}

STATE_DEKPHRE = {
    id = 467
    subsistence_building = "building_subsistence_rice_paddies"
    provinces = { "x37E13C" "x43516A" "x4E5E35" "x789906" "xA5389D" "xBC05A1" "xDDF9C8" }
    traits = { state_trait_telebei_river }
    city = ""
    farm = ""
    mine = ""
    wood = ""
    arable_land = 112
    arable_resources = { bg_rice_farms bg_livestock_ranches bg_cotton_plantations bg_opium_plantations bg_silk_plantations bg_sugar_plantations bg_tea_plantations }
    capped_resources = {
        bg_logging = 11
        bg_haless_spirits = 1
    }
}

STATE_KHINDI = {
    id = 468
    subsistence_building = "building_subsistence_rice_paddies"
    provinces = { "x000E96" "x2845AF" "x290EE1" "x3AC676" "x5089C8" "x516DCB" "x53505D" "x6918A5" "x78B0E1" "x798996" "x7A45C8" "xAD527F" "xC68785" "xD69D4B" "xE7B02F" "xEA4E36" }
    traits = { state_trait_hukai_river state_trait_bomdan_jungles }
    city = ""
    port = ""
    farm = ""
    mine = ""
    wood = ""
    arable_land = 128
    arable_resources = { bg_rice_farms bg_livestock_ranches bg_banana_plantations bg_cotton_plantations bg_dye_plantations bg_opium_plantations bg_sugar_plantations bg_tea_plantations }
    capped_resources = {
        bg_logging = 20
        bg_fishing = 9
        bg_haless_spirits = 1
        bg_iron_mining = 20
    }
    resource = {
        type = "bg_rubber"
        undiscovered_amount = 1
    }
    naval_exit_id = 3301
}

STATE_KHABTEI_TELENI = {
    id = 469
    subsistence_building = "building_subsistence_rice_paddies"
    provinces = { "x01B0C8" "x0289FA" "x0345AF" "x09FBA0" "x1B3E41" "x1FDF5A" "x5145FA" "x520EAF" "x83D479" "x8B1FE8" "x9DBB80" "xF145E1" "xF20E96" }
    traits = { state_trait_hukai_river state_trait_bomdan_jungles }
    city = ""
    farm = ""
    mine = ""
    wood = ""
    arable_land = 174
    arable_resources = { bg_rice_farms bg_livestock_ranches bg_banana_plantations bg_cotton_plantations bg_dye_plantations bg_opium_plantations bg_sugar_plantations bg_tea_plantations }
    capped_resources = {
        bg_logging = 12
        bg_haless_spirits = 1
    }
    resource = {
        type = "bg_rubber"
        undiscovered_amount = 1
    }
}

STATE_RONGBEK = {
    id = 470
    subsistence_building = "building_subsistence_rice_paddies"
    provinces = { "x2CD086" "x4056CF" "x4ADE0D" "x53F4EF" "x85D81C" "x9EE97F" "xA64F39" "xDB447B" "xDD43FA" "xF90991" "xFE4149" }
    traits = { state_trait_bomdan_jungles }
    city = ""
    farm = ""
    mine = ""
    wood = ""
    arable_land = 41
    arable_resources = { bg_rice_farms bg_livestock_ranches bg_cotton_plantations bg_opium_plantations bg_sugar_plantations bg_tea_plantations }
    capped_resources = {
        bg_logging = 14
        bg_haless_spirits = 1
    }
}

STATE_HINPHAT = {
    id = 471
    subsistence_building = "building_subsistence_rice_paddies"
    provinces = { "x1B85AC" "x2A686A" "x2F8DC6" "x35911D" "x42BC72" "x4465F0" "x4760A7" "x5A08EC" "x6DAFA3" "x8FDB7C" "xB4234F" "xBA1710" "xC30A44" "xC55933" "xDC3AC4" "xE1E0AE" "xF7342A" }
    impassable = { "x8fdb7c" "x6dafa3" }
    traits = { state_trait_hukai_river }
    city = ""
    farm = ""
    mine = ""
    wood = ""
    arable_land = 241
    arable_resources = { bg_rice_farms bg_livestock_ranches bg_cotton_plantations bg_opium_plantations bg_tea_plantations }
    capped_resources = {
        bg_logging = 10
        bg_haless_spirits = 1
        bg_coal_mining = 32
        bg_iron_mining = 18
    }
}

STATE_NAGON = {
    id = 472
    subsistence_building = "building_subsistence_rice_paddies"
    provinces = { "x0F650D" "x1D0A5E" "x21F9DA" "x224EC0" "x2AB096" "x419F1F" "x551FB3" "xA00EFA" "xA2087C" "xC748BB" "xC84596" "xCA0EAF" "xCFAD14" "xD53403" "xF089AF" }
    traits = { state_trait_bomdan_jungles }
    city = ""
    port = ""
    farm = ""
    mine = ""
    wood = ""
    arable_land = 86
    arable_resources = { bg_rice_farms bg_livestock_ranches bg_banana_plantations bg_cotton_plantations bg_dye_plantations bg_opium_plantations bg_sugar_plantations bg_tea_plantations bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 18
        bg_fishing = 11
        bg_haless_spirits = 1
    }
    resource = {
        type = "bg_rubber"
        undiscovered_amount = 1
    }
    naval_exit_id = 3302
}

STATE_KHOM_MA = {
    id = 473
    subsistence_building = "building_subsistence_rice_paddies"
    provinces = { "x081C54" "x17EBA0" "x1C3E37" "x212CC9" "x28C1DA" "x387411" "x479B23" "x4D440C" "x51DEF0" "x67977C" "x6E0DA9" "xA3541E" "xAA56C4" "xB19FE2" "xB7E961" "xB88B03" "xC12CA9" "xD1559E" "xDAACF6" "xE32600" "xECF8C7" "xFC6722" "xFF5C34" }
    traits = { state_trait_hukai_river state_trait_khom_ma_river }
    city = ""
    farm = ""
    mine = ""
    wood = ""
    arable_land = 366
    arable_resources = { bg_rice_farms bg_livestock_ranches bg_banana_plantations bg_cotton_plantations bg_opium_plantations bg_silk_plantations bg_sugar_plantations bg_tea_plantations bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 13
        bg_haless_spirits = 1
    }
}

STATE_PHONAN = {
    id = 474
    subsistence_building = "building_subsistence_rice_paddies"
    provinces = { "x0E1447" "x12F9A4" "x1D21DC" "x487FA3" "x61550D" "x7303AA" "x77265F" "x7C7D66" "x8917C0" "xA367B2" "xBA62C8" "xE3CE2C" "xF028E0" "xFC613B" }
    traits = { state_trait_hukai_river }
    city = ""
    farm = ""
    mine = ""
    wood = ""
    arable_land = 196
    arable_resources = { bg_rice_farms bg_livestock_ranches bg_cotton_plantations bg_opium_plantations bg_sugar_plantations bg_tea_plantations }
    capped_resources = {
        bg_logging = 9
        bg_haless_spirits = 1
    }
}

STATE_HOANGDESINH = {
    id = 475
    subsistence_building = "building_subsistence_rice_paddies"
    provinces = { "x35269A" "x35BBAD" "x562748" "x6F6083" "x98BB11" "xA842AC" "xABE662" "xB6C902" "xBA2242" "xD7049F" "xD85EFD" }
    traits = { state_trait_natural_harbors state_trait_lupulan_rainforest state_trait_khom_ma_river }
    city = ""
    port = ""
    farm = ""
    mine = ""
    wood = ""
    arable_land = 165
    arable_resources = { bg_rice_farms bg_livestock_ranches bg_banana_plantations bg_cotton_plantations bg_dye_plantations bg_opium_plantations bg_silk_plantations bg_sugar_plantations bg_tea_plantations bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 18
        bg_fishing = 16
        bg_haless_spirits = 1
        bg_lead_mining = 23
    }
    resource = {
        type = "bg_rubber"
        undiscovered_amount = 1
    }
    naval_exit_id = 3302
}

STATE_TLAGUKIT = {
    id = 476
    subsistence_building = "building_subsistence_rice_paddies"
    provinces = { "x139EAE" "x16F6CB" "x2C0C23" "x3918AF" "x4524C5" "x4B5340" "x552D3E" "x56FA7F" "x924B72" "x9DAA73" "xA65BD0" "xB537CC" "xB95EA5" "xBE294B" "xC17A73" "xE3BBCF" }
    traits = { state_trait_lupulan_rainforest state_trait_khom_ma_river }
    city = ""
    farm = ""
    mine = ""
    wood = ""
    arable_land = 48
    arable_resources = { bg_rice_farms bg_livestock_ranches bg_banana_plantations bg_dye_plantations bg_sugar_plantations bg_tea_plantations bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 25
        bg_haless_spirits = 1
    }
    resource = {
        type = "bg_rubber"
        undiscovered_amount = 1
    }
}

STATE_SIRTAN = {
    id = 477
    subsistence_building = "building_subsistence_pastures"
    provinces = { "x19A568" "x297309" "x2B719B" "x2CA69C" "x2D33CC" "x559AC7" "x570E5C" "x6F21B8" "x798913" "x8D91E0" "x8E4D87" "x9AB9A7" "xA66692" "xBE637B" "xC737ED" "xD3394B" "xEABA09" "xEC3B9B" "xFA685C" }
    traits = { state_trait_khom_ma_river }
    city = ""
    farm = ""
    mine = ""
    wood = ""
    arable_land = 43
    arable_resources = { bg_rice_farms bg_livestock_ranches bg_tea_plantations }
    capped_resources = {
        bg_haless_spirits = 1
    }
}

STATE_KUDET_KAI = {
    id = 478
    subsistence_building = "building_subsistence_rice_paddies"
    provinces = { "x1370D6" "x1ECEE2" "x2083D7" "x262294" "x320DB8" "x3B6AF0" "x49EF6B" "x4F90DD" "x5D09DD" "x620C86" "x72E0A6" "x80E41C" "x81C289" "x856994" "xA3209B" "xC97C26" "xCAF244" "xCF88D3" "xD6C08A" "xDF6440" "xEA66F0" }
    traits = { state_trait_khom_ma_river }
    city = ""
    farm = ""
    mine = ""
    wood = ""
    arable_land = 206
    arable_resources = { bg_rice_farms bg_livestock_ranches bg_coffee_plantations bg_cotton_plantations bg_dye_plantations bg_opium_plantations bg_tea_plantations }
    capped_resources = {
        bg_logging = 8
        bg_haless_spirits = 1
        bg_iron_mining = 15
        bg_coal_mining = 20
    }
}

STATE_YEMAKAIBO = {
    id = 479
    subsistence_building = "building_subsistence_rice_paddies"
    provinces = { "x04AA5B" "x14A67D" "x48CD9B" "x5F370D" "x616055" "x74F862" "xADEF04" "xC34556" }
    traits = { state_trait_thidinkai_hills }
    city = ""
    port = ""
    farm = ""
    mine = ""
    wood = ""
    arable_land = 28
    arable_resources = { bg_rice_farms bg_livestock_ranches bg_coffee_plantations bg_cotton_plantations bg_dye_plantations bg_opium_plantations bg_tea_plantations }
    capped_resources = {
        bg_logging = 11
        bg_fishing = 6
        bg_haless_spirits = 1
        bg_coal_mining = 32
    }
    naval_exit_id = 3304
}

STATE_ARAWKELIN = {
    id = 480
    subsistence_building = "building_subsistence_rice_paddies"
    provinces = { "x007BDF" "x039073" "x0C673D" "x12A154" "x237297" "x2476F7" "x3C4017" "x4AED0B" "x5D3B89" "x615D98" "x7A23D6" "x7AAD8D" "x9D474A" "xA23BD5" "xAA6EB2" "xAF1FAE" "xB43212" "xB8D1F7" "xBAE2AE" "xBCA5D7" "xC69DD3" "xD2B27C" "xD5478B" "xE97355" }
    traits = { state_trait_lupulan_rainforest state_trait_natural_harbors }
    city = "xd5478b"    #Arawkelin
    port = "xd2b27c"    #Aksa Sanuyego
    farm = "xbae2ae"   #Gubahtora
    mine = "xa23bd5"  #Mangayan
    wood = "x7aad8d"    #Pingursnai
    arable_land = 91
    arable_resources = { bg_rice_farms bg_livestock_ranches bg_banana_plantations bg_dye_plantations bg_sugar_plantations bg_tea_plantations bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 26
        bg_fishing = 20
        bg_haless_spirits = 1
        bg_coal_mining = 33
    }
    resource = {
        type = "bg_rubber"
        undiscovered_amount = 1
    }
    naval_exit_id = 3302    #Khom Sea due to Aksa Sanuyego
}

STATE_REWIRANG = {
    id = 481
    subsistence_building = "building_subsistence_rice_paddies"
    provinces = { "x06179A" "x22E4BA" "x36F19B" "x37B473" "x4A1C6A" "x4BFD9E" "x604D77" "x6CF0AB" "x7E5326" "x8A238B" "x977122" "x9B19EF" "xA501F7" "xA85B3A" "xB0744C" "xB0B98E" "xE49124" "xFB17DE" }
    traits = { state_trait_lupulan_rainforest }
    city = ""
    port = ""
    farm = ""
    mine = ""
    wood = ""
    arable_land = 28
    arable_resources = { bg_rice_farms bg_livestock_ranches bg_banana_plantations bg_dye_plantations bg_sugar_plantations bg_tea_plantations bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 29
        bg_fishing = 4
        bg_haless_spirits = 1
        bg_iron_mining = 30
    }
    resource = {
        type = "bg_rubber"
        undiscovered_amount = 1
    }
    naval_exit_id = 3302
}

STATE_MESATULEK = {
    id = 482
    subsistence_building = "building_subsistence_rice_paddies"
    provinces = { "x075C1A" "x22E94C" "x2485E9" "x510498" "x5428F2" "x56A603" "x64DBA9" "x74E849" "x94E3EC" "xA06205" "xA2AC51" "xA32923" "xA40BF9" "xA45A36" "xDCDB5F" "xF260E6" }
    traits = { state_trait_lupulan_rainforest }
    city = ""
    port = ""
    farm = ""
    mine = ""
    wood = ""
    arable_land = 38
    arable_resources = { bg_rice_farms bg_livestock_ranches bg_banana_plantations bg_dye_plantations bg_sugar_plantations bg_tea_plantations bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 21
        bg_fishing = 13
        bg_haless_spirits = 1
        bg_lead_mining = 36
        bg_sulfur_mining = 48
    }
    resource = {
        type = "bg_rubber"
        undiscovered_amount = 1
    }
    naval_exit_id = 3303
}

STATE_NON_CHIEN = {
    id = 483
    subsistence_building = "building_subsistence_rice_paddies"
    provinces = { "x0BA9BA" "x0ED218" "x103362" "x506D00" "x5B00E7" "x6456CA" "x8250D6" "x8C7DD4" "xADFFCE" "xB56E83" "xC25C7D" "xD23086" }
    traits = { state_trait_thidinkai_hills }
    city = ""
    port = ""
    farm = ""
    mine = ""
    wood = ""
    arable_land = 52
    arable_resources = { bg_rice_farms bg_livestock_ranches bg_banana_plantations bg_coffee_plantations bg_dye_plantations bg_sugar_plantations bg_tea_plantations }
    capped_resources = {
        bg_logging = 17
        bg_fishing = 15
        bg_haless_spirits = 1
        bg_lead_mining = 27
    }
    naval_exit_id = 3304
}

STATE_BINHRUNGHIN = {
    id = 484
    subsistence_building = "building_subsistence_rice_paddies"
    provinces = { "x35B5EC" "x38D3DD" "x5717DC" "x5A5A3F" "x6B5428" "x6B9EFE" "x8C2896" "x963A11" "xBAD1B0" "xE18E12" "xE375AA" "xEA15F5" }
    traits = { state_trait_thidinkai_hills state_trait_khom_ma_river }
    city = ""
    port = ""
    farm = ""
    mine = ""
    wood = ""
    arable_land = 122
    arable_resources = { bg_rice_farms bg_livestock_ranches bg_coffee_plantations bg_dye_plantations bg_tea_plantations }
    capped_resources = {
        bg_logging = 15
        bg_fishing = 7
        bg_haless_spirits = 1
        bg_coal_mining = 27
        bg_iron_mining = 24
    }
    naval_exit_id = 3304
}

STATE_VERKAL_OZOVAR = {
    id = 485
    subsistence_building = "building_subsistence_farms"
    provinces = { "x7BD798" "xD440D3" }
    traits = {}
    city = ""
    farm = ""
    mine = ""
    wood = ""
    arable_land = 30
    arable_resources = { bg_rice_farms bg_livestock_ranches }
    capped_resources = {
        bg_haless_spirits = 1
        bg_iron_mining = 24
    }
}

STATE_486 = {
    #TODO
    id = 486
    subsistence_building = "building_subsistence_farms"
    provinces = { "x46B25C" "x9BAAC5" "xF3B25C" }
    impassable = { "x46B25C" "xF3B25C" }
    traits = {}
    city = ""
    port = ""
    farm = ""
    mine = ""
    wood = ""
    arable_land = 15
    arable_resources = { bg_rice_farms bg_livestock_ranches bg_banana_plantations bg_coffee_plantations bg_dye_plantations bg_sugar_plantations bg_tea_plantations bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 1
        bg_fishing = 1
        bg_haless_spirits = 1
    }
    resource = {
        type = "bg_rubber"
        undiscovered_amount = 1
    }
    naval_exit_id = 3301
}

STATE_487 = {
    #TODO
    id = 487
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0404E7" "x11700F" "x2FDCB1" "x7CE04F" "xBDCC02" "xCD599C" "xF172D7" }
    traits = {}
    city = ""
    port = ""
    farm = ""
    mine = ""
    wood = ""
    arable_land = 15
    arable_resources = { bg_rice_farms bg_livestock_ranches bg_banana_plantations bg_coffee_plantations bg_dye_plantations bg_sugar_plantations bg_tea_plantations bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 1
        bg_fishing = 1
    }
    resource = {
        type = "bg_rubber"
        undiscovered_amount = 1
    }
    naval_exit_id = 3303
}

STATE_488 = {
    #TODO
    id = 488
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0CD3E8" "x3581CC" "x5C754A" "x9196FE" "x9572B9" "xA40EEE" "xB335F8" "xCC1C45" }
    traits = {}
    city = ""
    port = ""
    farm = ""
    mine = ""
    wood = ""
    arable_land = 15
    arable_resources = { bg_rice_farms bg_livestock_ranches bg_banana_plantations bg_coffee_plantations bg_dye_plantations bg_sugar_plantations bg_tea_plantations bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 1
        bg_fishing = 1
    }
    resource = {
        type = "bg_rubber"
        undiscovered_amount = 1
    }
    naval_exit_id = 3303
}

STATE_489 = {
    #TODO
    id = 489
    subsistence_building = "building_subsistence_farms"
    provinces = { "x183196" "x45B39A" "x530EAD" "x7B331B" "xC5AE91" "xD799EA" "xE67C88" "xEC9B3A" }
    impassable = { "x183196" }
    traits = {}
    city = ""
    port = ""
    farm = ""
    mine = ""
    wood = ""
    arable_land = 15
    arable_resources = { bg_rice_farms bg_livestock_ranches bg_banana_plantations bg_coffee_plantations bg_dye_plantations bg_sugar_plantations bg_tea_plantations bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 1
        bg_fishing = 1
    }
    resource = {
        type = "bg_rubber"
        undiscovered_amount = 1
    }
    naval_exit_id = 3303
}

STATE_490 = {
    #TODO
    id = 490
    subsistence_building = "building_subsistence_farms"
    provinces = { "x50A9C1" "x7F4290" "x845E88" "xBED849" "xC14DC1" "xD02F75" "xE15E37" "xE7F1DC" }
    impassable = { "x845E88" "xE15E37" "xC14DC1" }
    traits = {}
    city = ""
    port = ""
    farm = ""
    mine = ""
    wood = ""
    arable_land = 15
    arable_resources = { bg_rice_farms bg_livestock_ranches bg_banana_plantations bg_coffee_plantations bg_dye_plantations bg_sugar_plantations bg_tea_plantations bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 1
        bg_fishing = 1
    }
    resource = {
        type = "bg_rubber"
        undiscovered_amount = 1
    }
    naval_exit_id = 3400
}
