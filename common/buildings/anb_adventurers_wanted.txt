﻿

building_adventurers_wanted_base = {
	building_group = bg_adventurers_wanted
	texture = "gfx/interface/icons/building_icons/adventurers_wanted_base.dds"
	city_type = none
	levels_per_mesh = 5
	buildable = no
	expandable = no
	downsizeable = no

	production_method_groups = {
		pmg_base_building_adventurers_wanted
		pmg_party_composition_building_adventurers_wanted
		pmg_loot_share_building_adventurers_wanted
	}
}


building_adventurers_wanted_dungeon_ancient_catacombs = {
	building_group = bg_adventurers_wanted
	texture = "gfx/interface/icons/building_icons/adventurers_wanted_combat_ancient_catacombs.dds"
	city_type = none
	levels_per_mesh = 5
	buildable = no
	expandable = no
	downsizeable = no

	production_method_groups = {
		pmg_dungeon_ancient_catacombs_building_adventurers_wanted

		pmg_dungeon_party_composition_building_adventurers_wanted

		pmg_loot_share_building_adventurers_wanted
	}
}

building_adventurers_wanted_combat_dragon = {
	building_group = bg_adventurers_wanted
	texture = "gfx/interface/icons/building_icons/adventurers_wanted_combat_dragon.dds"
	city_type = none
	levels_per_mesh = 5
	buildable = no
	expandable = no
	downsizeable = no

	production_method_groups = {
		pmg_combat_dragon_building_adventurers_wanted

		pmg_combat_party_composition_building_adventurers_wanted
		pmg_loot_share_building_adventurers_wanted
	}
}
