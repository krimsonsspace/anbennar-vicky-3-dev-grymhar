﻿gov_absolute_magocracy = {
	transfer_of_power = dictatorial

	male_ruler = "RULER_HIGH_MAGE"
	female_ruler = "RULER_HIGH_MAGE"
	
	possible = {
		has_law = law_type:law_magocracy
		country_has_voting_franchise = no
	}

	on_government_type_change = {
		change_to_dictatorial = yes
	}
	on_post_government_type_change = {
		post_change_to_dictatorial = yes
	}
}

gov_magocratic_oligarchy = {
	transfer_of_power = dictatorial

	male_ruler = "RULER_HIGH_MAGE"
	female_ruler = "RULER_HIGH_MAGE"
	
	possible = {
		has_law = law_type:law_magocracy
		has_law = law_type:law_oligarchy
	}

	on_government_type_change = {
		change_to_dictatorial = yes
	}
	on_post_government_type_change = {
		post_change_to_dictatorial = yes
	}
}

gov_magocratic_democracy = {
	transfer_of_power = presidential_elective

	male_ruler = "RULER_HIGH_MAGE"
	female_ruler = "RULER_HIGH_MAGE"
	
	possible = {
		has_law = law_type:law_magocracy
		country_has_voting_franchise = yes
	}

	on_government_type_change = {
		change_to_presidential_elective = yes
	}
	on_post_government_type_change = {
		post_change_to_presidential_elective = yes
	}
}