﻿namespace = anb_adventurers_wanted

# Anbennar

anb_adventurers_wanted.100 = { #test spawn AW
	type = country_event
	placement = scope:anb_adventurers_wanted_state	#makes it appear on the province
	title = anb_adventurers_wanted.100.t
	desc = anb_adventurers_wanted.100.d
	flavor = anb_adventurers_wanted.100.f

	duration = 1

	hidden = yes

	event_image = {
		video = "europenorthamerica_gold_prospectors"
	}

	on_created_soundeffect = "event:/SFX/UI/Alerts/event_appear"

	icon = "gfx/interface/icons/event_icons/event_trade.dds"

	trigger = {
		any_scope_state = {
			any_scope_building = {
				owner = root
				is_building_group = bg_adventurers_wanted 	
				count = 0	#BIG NOTE: due to current limitations / knowledge, you can only ever have ONE Adventurers Wanted in a state
			}
		}
	}
	

	immediate = {
		random_scope_state = {
			limit = {
				any_scope_building = { #doesnt already have any AW building
					owner = root
					is_building_group = bg_adventurers_wanted 	
					count = 0
				}
			}
			save_scope_as = anb_adventurers_wanted_state
		}

		random_list = {

			#Dungeon
			0 = {
				modifier = {
					if = {
						limit = {
							scope:anb_adventurers_wanted_state = {
								#culture = cu:east_damerian
								#region = sr:region_eastern_dameshead
								state_is_in_americas = yes
							}
						}
						add = 50	#this sets the TRUE chance
					}
					else = {
						add = 20	#this sets the TRUE chance
					}
				}
				scope:anb_adventurers_wanted_state = { 
					trigger_event = { id = anb_adventurers_wanted.11  } 	#for now this is precursor relic sites
				}
			}

			#Dragons
			0 = {
				modifier = {
					if = {
						limit = {
							scope:anb_adventurers_wanted_state = {
								#culture = cu:esmari
								region = sr:region_castanor
							}
						}
						add = 20	#this sets the TRUE chance
					}
					else = {
						add = 10	#this sets the TRUE chance
					}
				}
				scope:anb_adventurers_wanted_state = { 
					trigger_event = { id = anb_adventurers_wanted.12  } 
				}
			}

			#Wyverns
			0 = {
				modifier = {
					if = {
						limit = {
							scope:anb_adventurers_wanted_state = {
								#culture = cu:esmari
								OR = {
									state_region = s:STATE_VERNE
									state_region = s:STATE_KHENAK
									state_region = s:STATE_LORIN_CANNO
									state_region = s:STATE_SUGAMBER
								}
							}
						}
						add = 1	#this sets the TRUE chance
					}
				}
				scope:anb_adventurers_wanted_state = { 
					trigger_event = { id = anb_adventurers_wanted.12  } 
				}
			}

			#Default
			1 = {
				scope:anb_adventurers_wanted_state = { 
					trigger_event = { id = anb_adventurers_wanted.11  } #dungeons
				}
			}
			# 2000 = {	moved this chance to do nothing to the monthly pulse, b/c having every country check every year was mega performance-intensive

			# }

		}
	}
}

#Kill AW
anb_adventurers_wanted.101 = {
	type = country_event
	placement = scope:anb_adventurers_wanted_state	#makes it appear on the province
	title = anb_adventurers_wanted.101.t
	desc = anb_adventurers_wanted.101.d
	flavor = anb_adventurers_wanted.101.f

	duration = 1

	hidden = yes

	event_image = {
		video = "europenorthamerica_gold_prospectors"
	}

	on_created_soundeffect = "event:/SFX/UI/Alerts/event_appear"

	icon = "gfx/interface/icons/event_icons/event_trade.dds"

	trigger = {
		any_scope_building = {
			owner = root
			is_building_group = bg_adventurers_wanted 	
			cash_reserves_ratio > 0.95 #can't actually be 1 
		}
	}
	

	immediate = {
		random_scope_state = {
			limit = {
				any_scope_building = {
					owner = root
					is_building_group = bg_adventurers_wanted 	#killing aw relies on full cash reserves FOR NOW!
					cash_reserves_ratio > 0.95
				}
			}
			save_scope_as = anb_adventurers_wanted_state
		}
		random_list = {
			50 = {
				scope:anb_adventurers_wanted_state = { 
					trigger_event = { id = anb_adventurers_wanted.102  } 
				}
			}
			50 = {

			}
		}
		#doesnt already have any AW building
	}
} 

#Kill AW Generic
anb_adventurers_wanted.102 = {
	type = state_event
	placement = ROOT
	title = anb_adventurers_wanted.102.t
	desc = anb_adventurers_wanted.102.d
	flavor = anb_adventurers_wanted.102.f

	duration = 3

	event_image = {
		video = "europenorthamerica_gold_prospectors"
	}

	on_created_soundeffect = "event:/SFX/UI/Alerts/event_appear"

	icon = "gfx/interface/icons/event_icons/event_trade.dds"

	trigger = {
		# any_scope_building = {
		# 	owner = root
		# 	is_building_group = bg_adventurers_wanted 	
		# 	cash_reserves_ratio = 1
		# }
	}

	immediate = {
		save_scope_as = anb_adventurers_wanted_state
		random_scope_building = {
			limit = {
				#owner = root
				is_building_group = bg_adventurers_wanted 	
				cash_reserves_ratio > 0.95
			}
			save_scope_as = anb_adventurers_wanted_building
		}
	}

	option = {
		name = anb_adventurers_wanted.102.a
		default_option = yes

		remove_all_adventurers_wanted_buildings = yes	#unfortunately in this implementation and current PDX given functionality we need to just do this. This is why we can only have one AW per state
	}
} 

#Spawn AW Dungeon
anb_adventurers_wanted.11 = {
	type = state_event
	placement = ROOT
	title = anb_adventurers_wanted.11.t
	desc = anb_adventurers_wanted.11.d
	flavor = anb_adventurers_wanted.11.f

	duration = 3

	event_image = {
		video = "europenorthamerica_gold_prospectors"
	}

	on_created_soundeffect = "event:/SFX/UI/Alerts/event_appear"

	icon = "gfx/interface/icons/event_icons/event_trade.dds"

	trigger = {

	}

	immediate = {
		save_scope_as = anb_adventurers_wanted_state
	}

	option = { #move everyone there
		name = anb_adventurers_wanted.11.a
		default_option = yes
		create_building = {
			building = building_adventurers_wanted_dungeon_ancient_catacombs
		}
	}
}

#Spawn AW Dragon
anb_adventurers_wanted.12 = {
	type = state_event
	placement = ROOT
	title = anb_adventurers_wanted.12.t
	desc = anb_adventurers_wanted.12.d
	flavor = anb_adventurers_wanted.12.f

	duration = 3

	event_image = {
		video = "europenorthamerica_gold_prospectors"
	}

	on_created_soundeffect = "event:/SFX/UI/Alerts/event_appear"

	icon = "gfx/interface/icons/event_icons/event_trade.dds"

	trigger = {

	}

	immediate = {
		save_scope_as = anb_adventurers_wanted_state
	}

	option = { #move everyone there
		name = anb_adventurers_wanted.12.a
		default_option = yes

		create_building = {
			building = building_adventurers_wanted_combat_dragon
		}
	}
}


anb_adventurers_wanted.1 = { #test spawn AW
	type = state_event
	placement = ROOT
	title = anb_adventurers_wanted.1.t
	desc = anb_adventurers_wanted.1.d
	flavor = anb_adventurers_wanted.1.f

	duration = 3

	event_image = {
		video = "europenorthamerica_gold_prospectors"
	}

	on_created_soundeffect = "event:/SFX/UI/Alerts/event_appear"

	icon = "gfx/interface/icons/event_icons/event_trade.dds"

	trigger = {

	}

	immediate = {
		save_scope_as = anb_adventurers_wanted_state
	}

	option = { #move everyone there
		name = anb_adventurers_wanted.1.a
		default_option = yes
		create_building = {
			building = building_adventurers_wanted_dungeon_ancient_catacombs
		}
	}
}

anb_adventurers_wanted.2 = { #test remove AW
	type = state_event
	placement = ROOT
	title = anb_adventurers_wanted.2.t
	desc = anb_adventurers_wanted.2.d
	flavor = anb_adventurers_wanted.1.f

	duration = 3

	event_image = {
		video = "europenorthamerica_gold_prospectors"
	}

	on_created_soundeffect = "event:/SFX/UI/Alerts/event_appear"

	icon = "gfx/interface/icons/event_icons/event_trade.dds"

	trigger = {

	}

	immediate = {
		save_scope_as = anb_adventurers_wanted_state
	}

	option = { #move everyone there
		name = anb_adventurers_wanted.1.a
		default_option = yes
		remove_building = building_adventurers_wanted_dungeon_ancient_catacombs
	}
}

anb_adventurers_wanted.54 = {
	type = state_event
	title = "Force Resource Depletion effect test"
	duration = 10

	immediate = {

	}

	option = {
		name = "Deplete resources in Gold Mines of random state"
		force_resource_depletion = bg_gold_mining
	}

	option = {
		name = "CANCEL"
		default_option = yes
	}
}

anb_adventurers_wanted.55 = {
	type = state_event
	title = "Force Resource Depletion effect test"
	duration = 10

	immediate = {

	}

	option = {
		name = "Deplete resources in Gold Mines of random state"
		force_resource_depletion = bg_gold_fields
	}

	option = {
		name = "CANCEL"
		default_option = yes
	}
}

anb_adventurers_wanted.56 = {
	type = state_event
	title = "Force Resource Depletion effect test"
	duration = 10

	immediate = {

	}

	option = {
		name = "Deplete resources in Gold Mines of random state"
		remove_building = bg_gold_mining
	}

	option = {
		name = "CANCEL"
		default_option = yes
	}
}

anb_adventurers_wanted.57 = {
	type = state_event
	title = "Force Resource Depletion effect test"
	duration = 10

	immediate = {

	}

	option = {
		name = "Deplete resources in Gold Mines of random state"
		force_resource_discovery = bg_damestear_fields
	}

	option = {
		name = "CANCEL"
		default_option = yes
	}
}

anb_adventurers_wanted.58 = {
	type = state_event
	title = "Force Resource Depletion effect test"
	duration = 10

	immediate = {

	}

	option = {
		name = "Deplete resources in Gold Mines of random state"
		force_resource_depletion = bg_damestear_fields
	}

	option = {
		name = "CANCEL"
		default_option = yes
	}
}

