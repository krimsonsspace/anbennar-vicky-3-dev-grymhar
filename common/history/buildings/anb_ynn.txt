﻿BUILDINGS = {
	s:STATE_YRISRAD = {
		region_state:B29 = {
			create_building={
				building="building_logging_camp"
				level=1
				reserves=1
				activate_production_methods={ "pm_simple_forestry" "pm_merchant_guilds_building_logging_camp" "pm_no_equipment" "pm_road_carts" "pm_no_hardwood" }
			}
			create_building={
				building="building_wheat_farm"
				level=5
				reserves=1
				activate_production_methods={ "pm_privately_owned" "pm_simple_farming" "pm_tools_disabled" }
			}
			create_building={
				building="building_livestock_ranch"
				level=3
				reserves=1
			}
			create_building={
				building="building_vineyard_plantation"
				level=1
				reserves=1
				activate_production_methods={ "default_building_vineyard_plantation" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_textile_mills"
				level=2
				reserves=1
				activate_production_methods={ "pm_handsewn_clothes" "pm_no_luxury_clothes" "pm_traditional_looms" "pm_merchant_guilds_building_textile_mills" }
			}
			create_building={
				building="building_paper_mills"
				level=2
				reserves=1
				activate_production_methods={ "pm_pulp_pressing" "pm_automation_disabled" "pm_merchant_guilds_building_paper_mills" }
			}	
			create_building={
				building="building_construction_sector"
				level=1
				reserves=1
				activate_production_methods={ "pm_wooden_buildings" }
			}
			create_building={
				building="building_government_administration"
				level=2
				reserves=1
				activate_production_methods={ "pm_horizontal_drawer_cabinets" }
			}
			create_building={
				building="building_university"
				level=1
				reserves=1
				activate_production_methods={ "pm_scholastic_education" }
			}
		}
	}
	s:STATE_VYCHVELS = {
		region_state:B29 = {
			create_building={
				building="building_wheat_farm"
				level=3
				reserves=1
				activate_production_methods={ "pm_privately_owned" "pm_simple_farming" "pm_tools_disabled" }
			}
			create_building={
				building="building_logging_camp"
				level=2
				reserves=1
				activate_production_methods={ "pm_simple_forestry" "pm_merchant_guilds_building_logging_camp" "pm_no_equipment" "pm_road_carts" "pm_hardwood" }
			}
			create_building={
				building="building_furniture_manufacturies"
				level=1
				reserves=1
				activate_production_methods={ "pm_handcrafted_furniture" "pm_merchant_guilds_building_furniture_manufacturies" "pm_automation_disabled" "pm_luxury_furniture" }
			}
			
			create_building={
				building="building_government_administration"
				level=1
				reserves=1
				activate_production_methods={ "pm_horizontal_drawer_cabinets" }
			}
			create_building={
				building="building_tobacco_plantation"
				level=1
				reserves=1
				activate_production_methods={ "default_building_tobacco_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
		}
	}
	s:STATE_YNNPADH = {
		region_state:B29 = {
			create_building={
				building="building_wheat_farm"
				level=3
				reserves=1
				activate_production_methods={ "pm_privately_owned" "pm_simple_farming" "pm_tools_disabled" }
			}
			create_building={
				building="building_livestock_ranch"
				level=1
				reserves=1
			}
			create_building={
				building="building_government_administration"
				level=1
				reserves=1
				activate_production_methods={ "pm_horizontal_drawer_cabinets" }
			}
			create_building={
				building="building_vineyard_plantation"
				level=1
				reserves=1
				activate_production_methods={ "default_building_vineyard_plantation" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_construction_sector"
				level=1
				reserves=1
				activate_production_methods={ "pm_wooden_buildings" }
			}
			
		}
	}
	s:STATE_NIZELYNN = {
		region_state:B29 = {
			create_building={
				building="building_wheat_farm"
				level=1
				reserves=1
				activate_production_methods={ "pm_privately_owned" "pm_simple_farming" "pm_tools_disabled" "pm_citrus_orchards" }
			}
		}
		region_state:B34 = {
			create_building={
				building="building_logging_camp"
				level=1
				reserves=1
				activate_production_methods={ "pm_simple_forestry" "pm_merchant_guilds_building_logging_camp" "pm_no_equipment" "pm_road_carts" "pm_no_hardwood" }
			}
			create_building={
				building="building_tobacco_plantation"
				level=1
				reserves=1
				activate_production_methods={ "default_building_tobacco_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_wheat_farm"
				level=1
				reserves=1
				activate_production_methods={ "pm_privately_owned" "pm_simple_farming" "pm_tools_disabled" }
			}
			
		}
	}
	s:STATE_NIZVELS = {
		region_state:B29 = {
			create_building={
				building="building_wheat_farm"
				level=4
				reserves=1
				activate_production_methods={ "pm_privately_owned" "pm_simple_farming" "pm_tools_disabled" }
			}
			create_building={
				building="building_government_administration"
				level=1
				reserves=1
				activate_production_methods={ "pm_horizontal_drawer_cabinets" }
			}
			create_building={
				building="building_logging_camp"
				level=1
				reserves=1
				activate_production_methods={ "pm_simple_forestry" "pm_merchant_guilds_building_logging_camp" "pm_no_equipment" "pm_road_carts" "pm_no_hardwood" }
			}
			create_building={
				building="building_glassworks"
				level=1
				reserves=1
				activate_production_methods={ "pm_forest_glass" "pm_ceramics" "pm_manual_glassblowing" "pm_privately_owned_building_glassworks" }
			}

		}
	}
	s:STATE_HRADAPOLERE = {
		region_state:B29 = {
			create_building={
				building="building_wheat_farm"
				level=6
				reserves=1
				activate_production_methods={ "pm_privately_owned" "pm_simple_farming" "pm_tools_disabled" }
			}
			create_building={
				building="building_livestock_ranch"
				level=1
				reserves=1
			}
			create_building={
				building="building_government_administration"
				level=1
				reserves=1
				activate_production_methods={ "pm_horizontal_drawer_cabinets" }
			}
			create_building={
				building="building_logging_camp"
				level=2
				reserves=1
				activate_production_methods={ "pm_simple_forestry" "pm_merchant_guilds_building_logging_camp" "pm_no_equipment" "pm_road_carts" "pm_no_hardwood" }
			}

			create_building={
				building="building_vineyard_plantation"
				level=1
				reserves=1
				activate_production_methods={ "default_building_vineyard_plantation" "pm_privately_owned_plantation" }
			}
		}
	}
	s:STATE_CORINSFIELD = {
		region_state:B33 = {
			create_building={
				building="building_wheat_farm"
				level=4
				reserves=1
				activate_production_methods={ "pm_privately_owned" "pm_simple_farming" "pm_tools_disabled" }
			}
			create_building={
				building="building_opium_plantation"
				level=1
				reserves=1
				activate_production_methods={ "default_building_opium_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}

			create_building={
				building="building_dye_plantation"
				level=1
				reserves=1
				activate_production_methods={ "default_building_dye_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
		}
		region_state:B36 = {
			create_building={
				building="building_wheat_farm"
				level=1
				reserves=1
				activate_production_methods={ "pm_privately_owned" "pm_simple_farming" "pm_tools_disabled" }
			}
		}
	}
	s:STATE_WEST_TIPNEY = {
		region_state:B32 = {
			
			create_building={
				building="building_wheat_farm"
				level=3
				reserves=1
				activate_production_methods={ "pm_privately_owned" "pm_simple_farming" "pm_tools_disabled" "pm_citrus_orchards" }
			}
			create_building={
				building="building_tobacco_plantation"
				level=4
				reserves=1
				activate_production_methods={ "default_building_tobacco_plantation" "pm_road_carts" "pm_privately_owned_plantation" }
			}
			create_building={
				building="building_food_industry"
				level=1
				reserves=1
				activate_production_methods={ "pm_sweeteners" "pm_disabled_canning" "pm_merchant_guilds_building_food_industry" "pm_manual_dough_processing" "pm_pot_stills" }
			}
		}
	}
	s:STATE_VIZANIRZAG = {
		region_state:B31 = {
			create_building={
				building="building_wheat_farm"
				level=2
				reserves=1
				activate_production_methods={ "pm_privately_owned" "pm_simple_farming" "pm_tools_disabled" }
			}
			
		}
	}
	s:STATE_LETHIR = {
		region_state:B31 = {
			create_building={
				building="building_wheat_farm"
				level=4
				reserves=1
				activate_production_methods={ "pm_privately_owned" "pm_simple_farming" "pm_tools_disabled" }
			}
			create_building={
				building="building_livestock_ranch"
				level=1
				reserves=1
			}
			create_building={
				building="building_logging_camp"
				level=1
				reserves=1
				activate_production_methods={ "pm_simple_forestry" "pm_merchant_guilds_building_logging_camp" "pm_no_equipment" "pm_road_carts" "pm_no_hardwood" }
			}
			
			create_building={
				building="building_government_administration"
				level=1
				reserves=1
				activate_production_methods={ "pm_horizontal_drawer_cabinets" }
			}
		}
	}
	s:STATE_CHIPPENGARD = {
		region_state:B30 = {
			
			create_building={
				building="building_arms_industry"
				level=1
				reserves=1
				activate_production_methods={ "pm_muskets" "pm_merchant_guilds_building_arms_industry" }
			}
			create_building={
				building="building_livestock_ranch"
				level=1
				reserves=1
			}
		}
	}
	s:STATE_ARGEZVALE = {
		region_state:B36 = {
			
			create_building={
				building="building_wheat_farm"
				level=2
				reserves=1
				activate_production_methods={ "pm_privately_owned" "pm_simple_farming" "pm_tools_disabled" }
			}
			create_building={
				building="building_iron_mine"
				level=2
				reserves=1
				activate_production_methods={ "pm_picks_and_shovels_building_iron_mine" "pm_no_explosives" "pm_no_steam_automation" "pm_road_carts" "pm_merchant_guilds_building_iron_mine" }
			}
			create_building={
				building="building_gem_mine"
				level=1
				reserves=1
			}
			create_building={
				building="building_logging_camp"
				level=2
				reserves=1
				activate_production_methods={ "pm_saw_mills" "pm_merchant_guilds_building_logging_camp" "pm_no_equipment" "pm_road_carts" "pm_hardwood" }
			}
			create_building={
				building="building_tooling_workshops"
				level=1
				reserves=1
				activate_production_methods={ "pm_pig_iron" "pm_automation_disabled" "pm_privately_owned_building_tooling_workshops" }
			}
			create_building={
				building="building_construction_sector"
				level=1
				reserves=1
				activate_production_methods={ "pm_iron_frame_buildings" }
			}	
		}
	}
	s:STATE_GOMOSENGHA = {
		region_state:B49 = {
			create_building={
				building="building_wheat_farm"
				level=1
				reserves=1
				activate_production_methods={ "pm_privately_owned" "pm_simple_farming" "pm_tools_disabled" }
			}
			create_building={
				building="building_livestock_ranch"
				level=2
				reserves=1
			}
			create_building={
				building="building_iron_mine"
				level=1
				reserves=1
				activate_production_methods={ "pm_picks_and_shovels_building_iron_mine" "pm_no_explosives" "pm_no_steam_automation" "pm_road_carts" "pm_merchant_guilds_building_iron_mine" }
			}
			create_building={
				building="building_logging_camp"
				level=1
				reserves=1
				activate_production_methods={ "pm_saw_mills" "pm_merchant_guilds_building_logging_camp" "pm_no_equipment" "pm_road_carts" "pm_hardwood" }
			}
			create_building={
				building="building_furniture_manufacturies"
				level=1
				reserves=1
				activate_production_methods={ "pm_handcrafted_furniture" "pm_merchant_guilds_building_furniture_manufacturies" "pm_automation_disabled" "pm_luxury_furniture" }
			}
		}
	}
	s:STATE_VIZKALADR = {
		region_state:B49 = {
			create_building={
				building="building_arms_industry"
				level=2
				reserves=1
				activate_production_methods={ "pm_muskets" "pm_merchant_guilds_building_arms_industry" }
			}
			create_building={
				building="building_wheat_farm"
				level=6
				reserves=1
				activate_production_methods={ "pm_privately_owned" "pm_simple_farming" "pm_tools_disabled" }
			}
			create_building={
				building="building_logging_camp"
				level=3
				reserves=1
				activate_production_methods={ "pm_saw_mills" "pm_merchant_guilds_building_logging_camp" "pm_no_equipment" "pm_road_carts" "pm_hardwood" }
			}
			create_building={
				building="building_tooling_workshops"
				level=1
				reserves=1
				activate_production_methods={ "pm_pig_iron" "pm_automation_disabled" "pm_privately_owned_building_tooling_workshops" }
			}
			create_building={
				building="building_paper_mills"
				level=1
				reserves=1
				activate_production_methods={ "pm_pulp_pressing" "pm_automation_disabled" "pm_merchant_guilds_building_paper_mills" }
			}

			create_building={
				building="building_government_administration"
				level=2
				reserves=1
				activate_production_methods={ "pm_horizontal_drawer_cabinets" }
			}
			create_building={
				building="building_university"
				level=1
				reserves=1
				activate_production_methods={ "pm_scholastic_education" }
			}
		}
	}
	s:STATE_VERZEL = {
		region_state:B49 = {
			create_building={
				building="building_wheat_farm"
				level=2
				reserves=1
				activate_production_methods={ "pm_privately_owned" "pm_simple_farming" "pm_tools_disabled" }
			}
			create_building={
				building="building_logging_camp"
				level=2
				reserves=1
				activate_production_methods={ "pm_simple_forestry" "pm_merchant_guilds_building_logging_camp" "pm_no_equipment" "pm_road_carts" "pm_no_hardwood" }
			}

		}
	}
	s:STATE_BRELAR = {
		region_state:B49 = {
			create_building={
				building="building_iron_mine"
				level=2
				reserves=1
				activate_production_methods={ "pm_picks_and_shovels_building_iron_mine" "pm_no_explosives" "pm_no_steam_automation" "pm_road_carts" "pm_merchant_guilds_building_iron_mine" }
			}
			create_building={
				building="building_logging_camp"
				level=2
				reserves=1
				activate_production_methods={ "pm_simple_forestry" "pm_merchant_guilds_building_logging_camp" "pm_no_equipment" "pm_road_carts" "pm_no_hardwood" }
			}
			create_building={
				building="building_livestock_ranch"
				level=2
				reserves=1
			}
			create_building={
				building="building_construction_sector"
				level=1
				reserves=1
				activate_production_methods={ "pm_iron_frame_buildings" }
			}
			create_building={
				building="building_arms_industry"
				level=1
				reserves=1
				activate_production_methods={ "pm_muskets" "pm_merchant_guilds_building_arms_industry" }
			}
		}
	}
	s:STATE_POMVASONN = {
		region_state:B49 = {
			create_building={
				building="building_wheat_farm"
				level=3
				reserves=1
				activate_production_methods={ "pm_privately_owned" "pm_simple_farming" "pm_tools_disabled" }
			}
			create_building={
				building="building_livestock_ranch"
				level=2
				reserves=1
			}
			create_building={
				building="building_glassworks"
				level=1
				reserves=1
				activate_production_methods={ "pm_forest_glass" "pm_disabled_ceramics" "pm_manual_glassblowing" "pm_privately_owned_building_glassworks" }
			}
			create_building={
				building="building_government_administration"
				level=1
				reserves=1
				activate_production_methods={ "pm_horizontal_drawer_cabinets" }
			}
		}
	}
	s:STATE_EKRSOKA = {
		region_state:B49 = {
			create_building={
				building="building_livestock_ranch"
				level=1
				reserves=1
			}
		}
	}
	s:STATE_JUZONDEZAN = {
		region_state:B49 = {
			create_building={
				building="building_wheat_farm"
				level=4
				reserves=1
				activate_production_methods={ "pm_privately_owned" "pm_simple_farming" "pm_tools_disabled" "pm_citrus_orchards" }
			}
			create_building={
				building="building_livestock_ranch"
				level=1
				reserves=1
			}
			create_building={
				building="building_food_industry"
				level=1
				reserves=1
				activate_production_methods={ "pm_bakery" "pm_disabled_canning" "pm_merchant_guilds_building_food_industry" "pm_manual_dough_processing" "pm_pot_stills" }
			}
			create_building={
				building="building_artillery_foundries"
				level=1
				reserves=1
				activate_production_methods={ "pm_cannons" "pm_automation_disabled" "pm_merchant_guilds_building_artillery_foundries" }
			}
			create_building={
				building="building_coal_mine"
				level=1
				reserves=1
				activate_production_methods={ "pm_atmospheric_engine_pump_building_coal_mine" "pm_no_explosives" "pm_no_steam_automation" "pm_road_carts" "pm_privately_owned_building_coal_mine" }
			}
			
		}
	}
	s:STATE_ROGAIDHA = {
		region_state:B49 = {
			create_building={
				building="building_wheat_farm"
				level=7
				reserves=1
				activate_production_methods={ "pm_privately_owned" "pm_simple_farming" "pm_tools_disabled" }
			}
			
			create_building={
				building="building_textile_mills"
				level=1
				reserves=1
				activate_production_methods={ "pm_handsewn_clothes" "pm_no_luxury_clothes" "pm_traditional_looms" "pm_merchant_guilds_building_textile_mills" }
			}
			create_building={
				building="building_government_administration"
				level=2
				reserves=1
				activate_production_methods={ "pm_horizontal_drawer_cabinets" }
			}
		}
	}
	s:STATE_MOCEPED = {
		region_state:B49 = {
			create_building={
				building="building_wheat_farm"
				level=2
				reserves=1
				activate_production_methods={ "pm_privately_owned" "pm_simple_farming" "pm_tools_disabled" }
			}
			
		}
	}
	s:STATE_USLAD = {
		region_state:B49 = {
			create_building={
				building="building_wheat_farm"
				level=1
				reserves=1
				activate_production_methods={ "pm_privately_owned" "pm_simple_farming" "pm_tools_disabled" }
			}
			create_building={
				building="building_iron_mine"
				level=1
				reserves=1
				activate_production_methods={ "pm_picks_and_shovels_building_iron_mine" "pm_no_explosives" "pm_no_steam_automation" "pm_road_carts" "pm_merchant_guilds_building_iron_mine" }
			}
		}
		region_state:B39 = { #ropes, lamp oil, and soon bombs
			create_building={
				building="building_wheat_farm"
				level=4
				reserves=1
				activate_production_methods={ "pm_privately_owned" "pm_simple_farming" "pm_tools_disabled" }
			}
			create_building={
				building="building_logging_camp"
				level=1
				reserves=1
				activate_production_methods={ "pm_saw_mills" "pm_merchant_guilds_building_logging_camp" "pm_no_equipment" "pm_road_carts" "pm_hardwood" }
			}
			create_building={
				building="building_livestock_ranch"
				level=1
				reserves=1
			}
			create_building={
				building="building_textile_mills"
				level=1
				reserves=1
				activate_production_methods={ "pm_handsewn_clothes" "pm_no_luxury_clothes" "pm_traditional_looms" "pm_merchant_guilds_building_textile_mills" }
			}
			create_building={
				building="building_furniture_manufacturies"
				level=2
				reserves=1
				activate_production_methods={ "pm_handcrafted_furniture" "pm_merchant_guilds_building_furniture_manufacturies" "pm_automation_disabled" "pm_luxury_furniture" }
			}
			create_building={
				building="building_sulfur_mine"
				level=1
				reserves=1
				activate_production_methods={ "pm_atmospheric_engine_pump_building_sulfur_mine" "pm_no_explosives" "pm_no_steam_automation" "pm_road_carts" "pm_privately_owned_building_sulfur_mine" }
			}
			
			create_building={
				building="building_government_administration"
				level=1
				reserves=1
			}
		}
		region_state:B52 = {
			create_building={
				building="building_logging_camp"
				level=1
				reserves=1
				activate_production_methods={ "pm_simple_forestry" "pm_merchant_guilds_building_logging_camp" "pm_no_equipment" "pm_road_carts" "pm_no_hardwood" }
			}
			create_building={
				building="building_wheat_farm"
				level=1
				reserves=1
				activate_production_methods={ "pm_privately_owned" "pm_simple_farming" "pm_tools_disabled" }
			}
			
		}
	}
	s:STATE_VITREYNN = {
		region_state:B38 = {
			create_building={
				building="building_wheat_farm"
				level=3
				reserves=1
				activate_production_methods={ "pm_privately_owned" "pm_simple_farming" "pm_tools_disabled" }
			}
			create_building={
				building="building_logging_camp"
				level=1
				reserves=1
				activate_production_methods={ "pm_simple_forestry" "pm_merchant_guilds_building_logging_camp" "pm_no_equipment" "pm_road_carts" "pm_no_hardwood" }
			}
			
		}
	}
	s:STATE_VARBUKLAND = {
		region_state:B53 = {
			create_building={
				building="building_wheat_farm"
				level=1
				reserves=1
				activate_production_methods={ "pm_privately_owned" "pm_simple_farming" "pm_tools_disabled" }
			}
			create_building={
				building="building_livestock_ranch"
				level=1
				reserves=1
			}
			create_building={
				building="building_logging_camp"
				level=2
				reserves=1
				activate_production_methods={ "pm_simple_forestry" "pm_merchant_guilds_building_logging_camp" "pm_no_equipment" "pm_road_carts" "pm_no_hardwood" }
			}
			
		}
	}
	s:STATE_NEW_HAVORAL = {
		region_state:B35 = {
			create_building={
				building="building_wheat_farm"
				level=2
				reserves=1
				activate_production_methods={ "pm_privately_owned" "pm_simple_farming" "pm_tools_disabled" }
			}
			
		}
	}
	s:STATE_OSINDAIN = {
		region_state:B35 = {
			create_building={
				building="building_livestock_ranch"
				level=1
				reserves=1
			}
			
		}
	}
	s:STATE_EBENMAS = {
		region_state:B91 = {
			create_building={ #auroch barons
				building="building_livestock_ranch"
				level=4
				reserves=1
				activate_production_methods={ "pm_privately_owned" "pm_simple_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_butchering_tools" }
			}
			create_building={
				building="building_wheat_farm"
				level=1
				reserves=1
				activate_production_methods={ "pm_privately_owned" "pm_simple_farming" "pm_tools" }
			}
			create_building={
				building="building_logging_camp"
				level=1
				reserves=1
				activate_production_methods={ "pm_saw_mills" "pm_merchant_guilds_building_logging_camp" "pm_no_equipment" "pm_road_carts" "pm_hardwood" }
			}
			create_building={
				building="building_textile_mills"
				level=1
				reserves=1
				activate_production_methods={ "pm_handsewn_clothes" "pm_no_luxury_clothes" "pm_traditional_looms" "pm_merchant_guilds_building_textile_mills" }
			}
			create_building={
				building="building_tooling_workshops"
				level=1
				reserves=1
				activate_production_methods={ "pm_pig_iron" "pm_automation_disabled" "pm_privately_owned_building_tooling_workshops" }
			}
			
			create_building={
				building="building_university"
				level=1
				reserves=1
				activate_production_methods={ "pm_scholastic_education" }
			}
			create_building={
				building="building_government_administration"
				level=1
				reserves=1
				activate_production_methods={ "pm_horizontal_drawer_cabinets" }
			}
		}
	}
	s:STATE_TELLUMTIR = {
		region_state:B91 = {
			create_building={
				building="building_livestock_ranch"
				level=3
				reserves=1
				activate_production_methods={ "pm_privately_owned" "pm_simple_ranch" "pm_standard_fences" "pm_unrefrigerated" "pm_butchering_tools" }
			}
			create_building={
				building="building_wheat_farm"
				level=2
				reserves=1
				activate_production_methods={ "pm_privately_owned" "pm_simple_farming" "pm_tools" }
			}
			create_building={
				building="building_iron_mine"
				level=1
				reserves=1
				activate_production_methods={ "pm_picks_and_shovels_building_iron_mine" "pm_no_explosives" "pm_no_steam_automation" "pm_road_carts" "pm_merchant_guilds_building_iron_mine" }
			}
			create_building={ #Bucks Firearms
				building="building_arms_industry"
				level=1
				reserves=1
				activate_production_methods={ "pm_muskets" "pm_merchant_guilds_building_arms_industry" }
			}
			create_building={ #newpapers!
				building="building_paper_mills"
				level=1
				reserves=1
				activate_production_methods={ "pm_pulp_pressing" "pm_automation_disabled" "pm_merchant_guilds_building_paper_mills" }
			}
		}
	}
	s:STATE_ELATHAEL = {
		region_state:B37 = {
			create_building={
				building="building_livestock_ranch"
				level=1
				reserves=1
			}
			create_building={
				building="building_wheat_farm"
				level=1
				reserves=1
				activate_production_methods={ "pm_privately_owned" "pm_simple_farming" "pm_tools_disabled" }
			}
			create_building={
				building="building_logging_camp"
				level=1
				reserves=1
				activate_production_methods={ "pm_simple_forestry" "pm_merchant_guilds_building_logging_camp" "pm_no_equipment" "pm_road_carts" "pm_no_hardwood" }
			}
			
		}
	}
	s:STATE_UZOO = {
		region_state:B91 = {
			create_building={
				building="building_gold_mine"
				level=1
				reserves=1
				activate_production_methods={ "pm_picks_and_shovels_building_gold_mine" "pm_no_explosives" "pm_no_steam_automation" "pm_road_carts" "pm_merchant_guilds_building_gold_mine" }
			}
		}
	}
	s:STATE_ARGANJUZORN = {
		region_state:B47 = {
			create_building={
				building="building_livestock_ranch"
				level=2
				reserves=1
			}
			create_building={
				building="building_logging_camp"
				level=1
				reserves=1
				activate_production_methods={ "pm_simple_forestry" "pm_merchant_guilds_building_logging_camp" "pm_no_equipment" "pm_road_carts" "pm_no_hardwood" }
			}
			create_building={
				building="building_wheat_farm"
				level=2
				reserves=1
				activate_production_methods={ "pm_privately_owned" "pm_simple_farming" "pm_tools_disabled" }
			}
			create_building={
				building="building_government_administration"
				level=1
				reserves=1
			}
		}
	}
	s:STATE_EPADARKAN = {
		region_state:B46 = {
			create_building={
				building="building_livestock_ranch"
				level=1
				reserves=1
			}
			
		}
		region_state:B45 = {
			create_building={
				building="building_logging_camp"
				level=2
				reserves=1
				activate_production_methods={ "pm_simple_forestry" "pm_merchant_guilds_building_logging_camp" "pm_no_equipment" "pm_road_carts" "pm_no_hardwood" }
			}
			create_building={
				building="building_wheat_farm"
				level=3
				reserves=1
				activate_production_methods={ "pm_privately_owned" "pm_simple_farming" "pm_tools_disabled" }
			}
			
			create_building={
				building="building_paper_mills"
				level=1
				reserves=1
				activate_production_methods={ "pm_pulp_pressing" "pm_automation_disabled" "pm_merchant_guilds_building_paper_mills" }
			}
		}
	}
	s:STATE_BEGGASLAND = {
		region_state:B34 = {
			create_building={
				building="building_wheat_farm"
				level=5
				reserves=1
				activate_production_methods={ "pm_privately_owned" "pm_simple_farming" "pm_tools_disabled" }
			}
			
			create_building={
				building="building_government_administration"
				level=1
				reserves=1
				activate_production_methods={ }
			}
		}
	}
	s:STATE_PLUMSTEAD = {
		region_state:B41 = {
			create_building={
				building="building_wheat_farm"
				level=4
				reserves=1
				activate_production_methods={ "pm_privately_owned" "pm_simple_farming" "pm_tools_disabled" "pm_citrus_orchards" }
			}
			create_building={
				building="building_livestock_ranch"
				level=1
				reserves=1
			}
			create_building={
				building="building_logging_camp"
				level=1
				reserves=1
				activate_production_methods={ "pm_simple_forestry" "pm_merchant_guilds_building_logging_camp" "pm_no_equipment" "pm_road_carts" "pm_no_hardwood" }
			}
			create_building={
				building="building_paper_mills"
				level=1
				reserves=1
				activate_production_methods={ "pm_pulp_pressing" "pm_automation_disabled" "pm_merchant_guilds_building_paper_mills" }
			}	
			create_building={
				building="building_government_administration"
				level=1
				reserves=1
				activate_production_methods={ "pm_horizontal_drawer_cabinets" }
			}
			create_building={
				building="building_university"
				level=1
				reserves=1
				activate_production_methods={ "pm_scholastic_education" }
			}
		}
	}
	s:STATE_BORUCKY = {
		region_state:B42 = {
			create_building={
				building="building_livestock_ranch"
				level=1
				reserves=1
			}
			create_building={
				building="building_logging_camp"
				level=1
				reserves=1
				activate_production_methods={ "pm_saw_mills" "pm_merchant_guilds_building_logging_camp" "pm_no_equipment" "pm_road_carts" "pm_no_hardwood" }
			}
			create_building={
				building="building_wheat_farm"
				level=1
				reserves=1
				activate_production_methods={ "pm_privately_owned" "pm_simple_farming" "pm_tools_disabled" }
			}
			
		}
	}
	s:STATE_ESIMOINE = {
		region_state:B42 = {
			create_building={
				building="building_livestock_ranch"
				level=2
				reserves=1
			}
			create_building={
				building="building_gold_mine"
				level=2
				reserves=1
				activate_production_methods={ "pm_picks_and_shovels_building_gold_mine" "pm_no_explosives" "pm_no_steam_automation" "pm_road_carts" "pm_merchant_guilds_building_gold_mine" }
			}
			
			create_building={
				building="building_tooling_workshops"
				level=1
				reserves=1
				activate_production_methods={ "pm_crude_tools" "pm_automation_disabled" "pm_privately_owned_building_tooling_workshops" }
			}
			create_building={
				building="building_food_industry"
				level=1
				reserves=1
				activate_production_methods={ "pm_sweeteners" "pm_disabled_canning" "pm_merchant_guilds_building_food_industry" "pm_manual_dough_processing" "pm_pot_stills" }
			}
			create_building={
				building="building_government_administration"
				level=1
				reserves=1
				activate_production_methods={ "pm_horizontal_drawer_cabinets" }
			}
		}
	}
	s:STATE_TUSNATA = {
		region_state:B42 = {
			create_building={
				building="building_maize_farm"
				level=1
				reserves=1
				activate_production_methods={ "pm_privately_owned" "pm_simple_farming" "pm_tools_disabled" }
			}
			
		}
	}
}