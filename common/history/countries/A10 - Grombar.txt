﻿COUNTRIES = {
	c:A10 = {
		effect_starting_technology_tier_2_tech = yes

		effect_starting_politics_traditional = yes
		
		# Laws 
		activate_law = law_type:law_racial_segregation
		activate_law = law_type:law_professional_army

		activate_law = law_type:law_interventionism
		activate_law = law_type:law_per_capita_based_taxation
		activate_law = law_type:law_tenant_farmers
		activate_law = law_type:law_dedicated_police
		activate_law = law_type:law_religious_schools
		activate_law = law_type:law_charitable_health_system

		activate_law = law_type:law_censorship
		activate_law = law_type:law_women_own_property # Not allowed women in workplace without voting
		activate_law = law_type:law_no_migration_controls
		activate_law = law_type:law_slavery_banned

		activate_law = law_type:law_all_races_allowed
		activate_law = law_type:law_traditional_magic_encouraged
			

		ig:ig_devout = {
			add_ruling_interest_group = yes
		}
		ig:ig_armed_forces = {
			add_ruling_interest_group = yes
		}

	}
}