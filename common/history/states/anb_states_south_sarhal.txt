﻿STATES = {

	s:STATE_DHAL_NIKHUVAD = {
		create_state = {
			country = c:L05
			owned_provinces = { "x637DB2" "xF94F81" "x92E66F" "x0E2C7E" "xD22CA7" }
		}
		
		
	}

	s:STATE_DHEBIJ_JANAB = {
		create_state = {
			country = c:L05
			owned_provinces = { "x56F9EC" "x842FCB" "x371F8E" "x87F71A" }
		}
		
		
	}

	s:STATE_SUHRATBA_YAJ = {
		create_state = {
			country = c:L05
			owned_provinces = { "x159F03" "x0962A6" "x7B0511" "x39FA16" "x6E50DC" }
		}
		
		
	}

	s:STATE_QASRIYINGI = {
		create_state = {
			country = c:L05
			owned_provinces = { "x64EEAC" "x046160" "x54A798" "xE0C160" "x809D71" "xBF990C" }
		}
		
		
	}
	
	# s:STATE_DHEBIJ_DHEKA = {
	# 	create_state = {
	# 		country = c:L05
	# 		owned_provinces = {  }
	# 	}
		
		
	# }

	s:STATE_BAASHI_BADDA = {
		create_state = {
			country = c:L05
			owned_provinces = { "x25E624" "xC6F466" "x14F888" "x394174" "x53A1EB" }
		}
		
		
	}
	
	s:STATE_SUHRATBA_YAHIN = {
		create_state = {
			country = c:L05
			owned_provinces = { "xF88745" "x23DAFA" "x22FB02" "x512A4E" }
		}
		
		
	}

	s:STATE_DHAL_TANIZUUD = {
		create_state = {
			country = c:L06
			owned_provinces = { "xDAB2E8" "x7B5A09" "x7F137D" "x0BFAB5" }
		}
		
		
	}

	s:STATE_JURITAQA = {
		create_state = {
			country = c:L06
			owned_provinces = { "x7A8026" "x32E2D3" "xB69B54" }
		}
		
		
	}

	s:STATE_MPAKA = {
		create_state = {
			country = c:L06
			owned_provinces = { "xF2EFEA" "x6CEA94" "x3D48BC" }
		}
		
		
	}

	s:STATE_DEBIJ_SHAR = {
		create_state = {
			country = c:L05
			owned_provinces = { "x736D8D" "x35042C" "x0CBC46" "xC977E5" }
		}
		create_state = {
			country = c:L07
			owned_provinces = { "x61C75E" }
		}
		create_state = {
			country = c:L08
			owned_provinces = { "x2F700F" }
		}
		
		
	}

	s:STATE_ASHAMAD_BARIGA = {
		create_state = {
			country = c:L08
			owned_provinces = { "x0C7167" "xBA14D0" "xF14D7F" "xD98049" "x888DF8" }
		}
		
		
	}
	
	s:STATE_DHAI_BAEIDAG = {
		create_state = {
			country = c:L08
			owned_provinces = { "xE8C9F9" "x8F515C" "xB19085" }
		}
		
		
	}

	s:STATE_QASRI_ABEESOOYINKA = {
		create_state = {
			country = c:L07
			owned_provinces = { "x896517" "x12C30C" "x23B901" "x3D0E08" "x6477B7" "xEBCB1F" "xE936A7" "x48CB0C" }
		}
		
		
	}
	
	s:STATE_QAYNSLAND = {
		create_state = {
			country = c:L09
			owned_provinces = { "x67B72F" "x4DC795" "xCDB694" "x02A0ED" "xE3773D" "x89017B" "xBDF7C5" "x6B35A3" "x9D99B5" }
		}
		
		
	}

	s:STATE_THE_OHITS = {
		create_state = {
			country = c:L09
			owned_provinces = { "x2D93E0" "x5B1BD8" "xA703FF" "xB34FB5" "xD6BD94" "xDE0DDA" "xE27C89" "xE4403D" "xF75DB2" }
		}
		
		
	}

	s:STATE_HARENMARCHES = {
		create_state = {
			country = c:L10
			owned_provinces = { "xCDE994" "xDC74C9" "x3DE367" "x560617" "x8FE18C" "x718F0D" }
		}
		
		
	}

	s:STATE_SAMAANIA = {
		create_state = {
			country = c:L11
			owned_provinces = { "x6B834D" "x680D38" "x0BDF26" "x2187DF" }
		}
		
		
	}

	s:STATE_ELIANDE = {
		create_state = {
			country = c:L12
			owned_provinces = { "xB06E77" "x996B08" "x739AD2" }
		}
		create_state = {
			country = c:L18
			owned_provinces = { "x635EA2" } 
		}
		
		
	}

	s:STATE_HADEADOL = {
		create_state = {
			country = c:L12
			owned_provinces = { "xC6667E" "x46C83A" "xF7EAB1" }
		}
		create_state = {
			country = c:L13
			owned_provinces = { "x9CC8D6" "x6FC76B" "xCB8D15" "x08606B" }
		}
		create_state = {
			country = c:L14
			owned_provinces = { "xAA4D42" }
		}
		
		
	}

	s:STATE_HARASCILDE = {
		create_state = {
			country = c:L15
			owned_provinces = { "x1C09ED" "x5F6263" "x8A5057" "xC1BD3D" "xC63F33" "xC88E52" "xD1FD51" "xE557A5" }
		}
		create_state = {
			country = c:L16
			owned_provinces = { "xEC0A7C" "x7406B0" }
		}
		
		
	}

	s:STATE_OLD_JINNAKAH = {
		create_state = {
			country = c:L05
			owned_provinces = { "x0452B0" "xB3D418" "xC9E1DE" "x397C06" }
		}
		create_state = {
			country = c:L16
			owned_provinces = { "xC25143" }
		}
		create_state = {
			country = c:L19
			owned_provinces = { "x49247E" }
		}
		
		
	}

	s:STATE_SSIPPANSEK = {
		create_state = {
			country = c:L07
			owned_provinces = { "xC974B1" "xDFBCEA" "x102158" "x9EAD2A" }
		}
		
		
	}


	s:STATE_LIZARDFOLK_1 = { 
		create_state = {
			country = c:L20
			owned_provinces = { "x1927F2" "x3458EA" "x6CB2AC" "x6F03E2" "x7502F7" "x799A8D" "x9BD28A" "xAA4132" "xAB0B4E" "xAB5FEB" "xC135CB" "xE40C7C" }
		}
		
		#add_homeland = cu:imperial_lizardfolk #to sort out, this is to just stop errors
		add_homeland = cu:zuvunwa #this place is owned by these two cultures in eu4
		add_homeland = cu:kuivanhi #this place is owned by these two cultures in eu4
	}
	s:STATE_LIZARDFOLK_2 = { 
		create_state = {
			country = c:L20
			owned_provinces = {  "x15F8DE" "x5457A7" "x551208" "x5C8DFA" "x832455" "x903D13" "x90B9EF" "xC929AF" "xD00D66" }
		}
		
		#add_homeland = cu:imperial_lizardfolk #to sort out, this is to just stop errors
		add_homeland = cu:ikilshebe #in EU4 these guys are the onlycountry here
	}
	s:STATE_LIZARDFOLK_3 = { 
		create_state = {
			country = c:L20
			owned_provinces = { "x044BF3" "x115C75" "x231782" "x28089B" "x432115" "x465C56" "x4C1B13" "x561B89" "x5743B9" "x5D1D9A" "x6DB940" "x77E5E0" "x8C2593" "x8F4CE1" "x937036" "x9FB8FE" "xA681D7" "xAC9807" "xB4DCC4" "xBBD7EC" "xD55571" "xD9744E" "xE6EAF3" "xEEF99B" }
		}
		
		add_homeland = cu:imperial_lizardfolk #to sort out, this is to just stop errors
	}
	s:STATE_LIZARDFOLK_4 = { 
		create_state = {
			country = c:L20
			owned_provinces = { "x20566B" "x3C54B0" "x4F5410" "x5DE532" "x6CFC28" "x8E9C72" "xCB6403" "xDEDF3B" "xF5C72F" "xF82DC9" }
		}
		
		add_homeland = cu:imperial_lizardfolk #to sort out, this is to just stop errors
	}
	s:STATE_LIZARDFOLK_5 = { 
		create_state = {
			country = c:L20
			owned_provinces = { "x0FE5AF" "x13E98D" "x1E6C9F" "x2DCD84" "x466C49" "x48B296" "x5021A2" "xC25C95" "xE09953" "xE5B3B5" "xE720F5" "xED90B9" "xF97261" }
		}
		
		add_homeland = cu:imperial_lizardfolk #to sort out, this is to just stop errors
	}
	s:STATE_LIZARDFOLK_6 = { 
		create_state = {
			country = c:L20
			owned_provinces = { "x04456B" "x0ACBF0" "xA487F1" "xB43223" "xCB83AA" "xFF1E08" }
		}
		
		add_homeland = cu:imperial_lizardfolk #to sort out, this is to just stop errors
	}
	s:STATE_LIZARDFOLK_7 = { 
		create_state = {
			country = c:L20
			owned_provinces = { "x27BBE1" "x2B0C7F" "x434E19" "x4F1337" "x60BA3A" "x653F94" "x7131CC" "x7A6BD1" "x97C61D" "xC98245" "xCDE9C3" "xF5532C" }
		}
		
		add_homeland = cu:imperial_lizardfolk #to sort out, this is to just stop errors
	}
	s:STATE_LIZARDFOLK_8 = { 
		create_state = {
			country = c:L20
			owned_provinces = { "x091BC1" "x0CE5D6" "x35A361" "x3EBB01" "x6BB684" "x6C8AE0" "x773071" "x93170A" "x9BACE5" "xE98BEB" }
		}
		
		add_homeland = cu:imperial_lizardfolk #to sort out, this is to just stop errors
	}
	s:STATE_LIZARDFOLK_9 = { 
		create_state = {
			country = c:L20
			owned_provinces = { "x1737C4" "x20B4B2" "x31455A" "x612C58" "x756CE6" "x9F42E8" "xA49011" "xE218E1" }
		}
		
		add_homeland = cu:imperial_lizardfolk #to sort out, this is to just stop errors
	}
	s:STATE_LIZARDFOLK_10 = { 
		create_state = {
			country = c:L20
			owned_provinces = { "x065652" "x0787B9" "x104785" "x13F8AE" "x3C5388" "x527AA4" "x66EE9B" "x8A756A" "xA949C5" "xC2FD33" "xC5E6C0" "xCCAED4" }
		}
		
		add_homeland = cu:imperial_lizardfolk #to sort out, this is to just stop errors
	}
	s:STATE_LIZARDFOLK_11 = { 
		create_state = {
			country = c:L21
			owned_provinces = { "x1080FA" "x222075" "x3D2EE3" "x509A66" "x57F214" "x842B61" "x854479" "x87BF62" "x8ED460" "x99F64C" "xBBD01B" "xBF7674" "xC485DD" }
		}
		
		add_homeland = cu:ashama_lizardfolk #to sort out, this is to just stop errors
	}
	s:STATE_LIZARDFOLK_12 = { 
		create_state = {
			country = c:L21
			owned_provinces = { "x1A8E5D" "x4305F7" "x5D1CC7" "x6F6188" "x820BAC" "x8668FA" "x89F9CB" "x9103F4" "xC1F133" "xC5027C" "xCD9120" }
		}
		
		add_homeland = cu:ashama_lizardfolk #to sort out, this is to just stop errors
	}
	s:STATE_LIZARDFOLK_13 = { 
		create_state = {
			country = c:L21
			owned_provinces = { "x0111A5" "x20DFF1" "x50C7A3" "x786C08" "x8A5138" "xAC74A1" }
		}
		
		add_homeland = cu:ashama_lizardfolk #to sort out, this is to just stop errors
	}
	s:STATE_LIZARDFOLK_14 = { #Split state
		create_state = {
			country = c:L21
			owned_provinces = { "xD6FB95" "xFFA5BA" }
		}
		create_state = {
			country = c:L06
			owned_provinces = { "x1E72D7" }
		}
		
		#add_homeland = cu:ashama_lizardfolk #to sort out, this is to just stop errors
		add_homeland = cu:tanizu #its just tanizu majority in eu4
	}

	s:STATE_LIZARDFOLK_15 = { 
		create_state = {
			country = c:L21
			owned_provinces = { "x1CFB9C" "x337512" "x49EE6D" "x510B6A" "x6BD65C" "x9CC172" "xD25350" "xD32FDB" }
		}
		
		#add_homeland = cu:ashama_lizardfolk #to sort out, this is to just stop errors
		add_homeland = cu:tanizu #its just tanizu majority in eu4
	}
	s:STATE_LIZARDFOLK_16 = { 
		create_state = {
			country = c:L21
			owned_provinces = { "x3E3049" "x41B615" "x455D6D" "x48BA21" "x7B8FE4" "xA5E523" "xB6F0A0" }
		}
		
		add_homeland = cu:ashama_lizardfolk #to sort out, this is to just stop errors
		add_homeland = cu:tanizu #its just tanizu majority in eu4
	}
	s:STATE_LIZARDFOLK_17 = {
		create_state = {
			country = c:L21
			owned_provinces = { "x24C462" "x3809DB" "x3F10F6""x72AF42" "x9E77BF" "xAA280F" "xD8DFFB" }
		}
		
		add_homeland = cu:ashama_lizardfolk #to sort out, this is to just stop errors
	}
}