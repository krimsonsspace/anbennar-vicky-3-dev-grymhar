﻿COUNTRIES = {
	c:C07 = {
		effect_starting_technology_tier_3_tech = yes
		add_technology_researched = empiricism
		add_technology_researched = stock_exchange
		
		# Laws 
		activate_law = law_type:law_presidential_republic
		activate_law = law_type:law_census_voting
		activate_law = law_type:law_cultural_exclusion
		activate_law = law_type:law_total_separation
		activate_law = law_type:law_hereditary_bureaucrats
		activate_law = law_type:law_peasant_levies
		activate_law = law_type:law_no_home_affairs
		
		activate_law = law_type:law_interventionism
		activate_law = law_type:law_free_trade
		activate_law = law_type:law_consumption_based_taxation
		activate_law = law_type:law_tenant_farmers
		activate_law = law_type:law_charitable_health_system
		
		activate_law = law_type:law_right_of_assembly
		activate_law = law_type:law_womens_suffrage #Harpy?
		activate_law = law_type:law_slavery_banned
		
		activate_law = law_type:law_all_races_allowed
		activate_law = law_type:law_dark_arts_banned
		activate_law = law_type:law_artifice_encouraged
		
		add_taxed_goods = g:dye
		add_taxed_goods = g:tobacco
		add_taxed_goods = g:sugar
		add_taxed_goods = g:fine_art
		add_taxed_goods = g:services
		add_taxed_goods = g:luxury_clothes
	}
}