﻿COUNTRIES = {
	c:A02 = {
		effect_starting_technology_tier_3_tech = yes
		
		effect_starting_politics_traditional = yes
		
		activate_law = law_type:law_professional_army
		
		activate_law = law_type:law_frontier_colonization
		
		activate_law = law_type:law_censorship
		activate_law = law_type:law_serfdom
		activate_law = law_type:law_slavery_banned

		activate_law = law_type:law_artifice_encouraged	#due to ravelian influence

		ig:ig_landowners = {
			add_ruling_interest_group = yes
		}
		
		add_journal_entry = { type = je_settle_the_folly }
	}
}