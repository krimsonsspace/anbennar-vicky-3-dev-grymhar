﻿COUNTRIES = {
	c:C16 = {
		effect_starting_technology_tier_5_tech = yes
		
		effect_starting_politics_traditional = yes
		activate_law = law_type:law_national_supremacy
		
		activate_law = law_type:law_ruinborn_group_only
		
		activate_law = law_type:law_mundane_production
	}
}