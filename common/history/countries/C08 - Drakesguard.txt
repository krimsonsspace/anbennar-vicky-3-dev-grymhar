﻿COUNTRIES = {
	c:C08 = {
		effect_starting_technology_tier_3_tech = yes
		add_technology_researched = empiricism
		
		# Laws 
		activate_law = law_type:law_monarchy
		activate_law = law_type:law_landed_voting
		activate_law = law_type:law_racial_segregation
		activate_law = law_type:law_freedom_of_conscience
		activate_law = law_type:law_hereditary_bureaucrats
		activate_law = law_type:law_national_militia
		activate_law = law_type:law_no_home_affairs
		
		activate_law = law_type:law_agrarianism
		activate_law = law_type:law_per_capita_based_taxation
		activate_law = law_type:law_tenant_farmers
		activate_law = law_type:law_local_police
		
		activate_law = law_type:law_censorship
		activate_law = law_type:law_migration_controls
		activate_law = law_type:law_legacy_slavery
		
		activate_law = law_type:law_same_race_only
		activate_law = law_type:law_pragmatic_application
		activate_law = law_type:law_traditional_magic_only
	}
}