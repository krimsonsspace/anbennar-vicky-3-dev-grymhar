﻿STATE_TRITHEMAR = {
    id = 295
    subsistence_building = "building_subsistence_farms"
    provinces = { "x2A2DF9" "x374BEF" "x78955F" "x858CBF" "x939F2A" "x977A70" "xD81D58" "xDB29E1" "xE07B7B" "x0700F0" "x1A741C" "x1B3D7D" "x3ABC20" "x3B5C7E" "x6D6334" "x8F517F" "xAC9D76" "xB24BF6" "xBA56AD" "xD9F500" }
    traits = {}
    city = "x374bef" #NEW PLACE



















    port = "xd81d58" #Random



















    farm = "x78955f" #Random



















    wood = "x2a2df9" #Random









    arable_land = 37
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_fishing = 1
        bg_logging = 1
    }
    naval_exit_id = 3112
}

STATE_SILDARBAD = {
    id = 297
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0500E8" "x08B647" "x0EAE45" "x3D3ED8" "x3F5A17" "x434C7E" "x4DBF0E" "x5D6A63" "x83D701" "x882F94" "x8D3309" "x996A84" "xB1EAC9" "xC3524F" "xD1F604" "xD48CEB" "xDAC4ED" "xFDEBB0" }
    traits = {}
    city = "x0500e8" #Sildarbad



















    port = "x434c7e" #Random



















    farm = "xd1f604" #Random



















    wood = "x8d3309" #Random



















    arable_land = 12
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_fishing = 1
        bg_logging = 1
    }
    naval_exit_id = 3113
}

STATE_FARNOR = {
    id = 299
    subsistence_building = "building_subsistence_farms"
    provinces = { "x24E0C7" "x456C7E" "x471938" "x55FB49" "x644D92" "x708011" "xA70154" "xA9253D" "xAA1FC4" "xBFD046" "xC408A8" "xC56A4F" "x14D998" "x2B5C94" "x38388D" "x435820" "x445820" "x445C7E" "x59D654" "x68C13E" "x74682C" "x911741" "xA7CAA4" "xAEE2A5" "xC356AD" "xC45A4F" "xE3B084" "xE78B63" "xE93407" "xF2BD5E" "xFB18E8" "x142AEF" "x30613B" "x5BAFDC" "xB5E1FE" }
    traits = {}
    city = "xc356ad" #Random



















    port = "x445820" #Random



















    farm = "xc45a4f" #Random



















    wood = "x59d654" #Random









    arable_land = 39
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_fishing = 1
        bg_logging = 1
    }
    naval_exit_id = 3113
}

STATE_DALAIRRSILD = {
    id = 502
    subsistence_building = "building_subsistence_farms"
    provinces = { "x001AA1" "x0D35DB" "x1F1FE9" "x2D8020" "x2E9020" "x347C7E" "x35FC7E" "x369C7E" "x36E150" "x39447E" "x39AC20" "x5F409A" "x633145" "x70B700" "x78B616" "x7B8DDF" "x7D8DDF" "xAEA850" "xB2743E" "xB69EAD" "xB946AD" "xB94A4F" "xCCB30D" "xD7DA53" "xE82A37" "xF782AD" "xFA6F4A" }
    traits = {}
    city = "xb94a4f" #Random



















    farm = "x39447e" #Random



















    wood = "x0d35db" #Random



















    arable_land = 24
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 24
        bg_sulfur_mining = 12
    }
}

STATE_SJAVARRUST = {
    id = 503
    subsistence_building = "building_subsistence_pastures"
    provinces = { "x15E1C1" "x18C602" "x2B8B08" "x33947E" "x362020" "x37247E" "x372820" "x372C7E" "x376DC1" "x383020" "x5B54A0" "x818DDF" "x8588DF" "x89FE58" "x984927" "xB43C83" "xB7224F" "xB72A4F" "xB7F6AD" "xCA6546" "xCE9158" "xD0956F" "xE2617C" "xEBC180" }
    traits = {}
    city = "xce9158" #Random



















    port = "x383020" #Random



















    farm = "x372c7e" #Random



















    wood = "x362020" #Random



















    arable_land = 21
    arable_resources = { bg_livestock_ranches }
    capped_resources = {
        bg_fishing = 5
        bg_whaling = 2
        bg_logging = 8
        bg_iron_mining = 8
    }
    naval_exit_id = 3110
}

STATE_HJORDAL = {
    id = 504
    subsistence_building = "building_subsistence_farms"
    provinces = { "x348020" "x3DE150" "x47B201" "x9E6EF7" "xC91DA7" "xD60BE9" "xDCDA53" "xE222ED" }
    traits = {}
    city = "x348020" #Noo Oddansbay



















    port = "xd60be9" #Random



















    farm = "xdcda53" #Random



















    wood = "x3de150" #Random



















    arable_land = 34
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_tobacco_plantations }
    capped_resources = {
        bg_fishing = 6
        bg_logging = 12
        bg_whaling = 2
        bg_iron_mining = 20
    }
    naval_exit_id = 3109
}

STATE_FLOTTNORD = {
    id = 505
    subsistence_building = "building_subsistence_pastures"
    provinces = { "x17C84F" "x244684" "x32847E" "x358820" "x369820" "x65409A" "x6D4261" "x700614" "x773F30" "x90A764" "x9DB41B" "xB696AD" "xB69A4F" "xC51DA7" "xCA0C83" }
    traits = {}
    city = "x358820" #Random



















    port = "xb696ad" #Random



















    farm = "x369820" #Random



















    wood = "xb69a4f" #Random



















    arable_land = 15
    arable_resources = { bg_livestock_ranches }
    capped_resources = {
        bg_fishing = 8
        bg_logging = 6
        bg_whaling = 6
        bg_iron_mining = 28
    }
    naval_exit_id = 3110
}

STATE_BROKEN_BAY = {
    id = 507
    subsistence_building = "building_subsistence_pastures"
    provinces = { "x0C5609" "x383C7E" "x4012C9" "x5527CD" "x5ED155" "xB384FF" "xB6D665" "xB836AD" "xB83A4F" "xB93EAD" "xD7A21E" "xE40539" "x15EAE2" "x1C97AE" "x3A4C7E" "x3A547E" "x500F71" "x615E25" "x806333" "xBA4EAD" "xBA524F" "xCBC752" "xF882AD" "xFBADE7" "x38FC7E" "xB8324F" }
    traits = {}
    city = "xb93ead" #Random



















    port = "x383c7e" #Random



















    farm = "xb83a4f" #Random



















    wood = "xb836ad" #Random









    arable_land = 23
    arable_resources = { bg_livestock_ranches }
    capped_resources = {
        bg_fishing = 8
        bg_logging = 10
    }
    naval_exit_id = 3111
}

STATE_NYHOFN = {
    id = 508
    subsistence_building = "building_subsistence_pastures"
    provinces = { "x11D353" "x59F99F" "xB784FF" "xBECF32" "xCE6546" "xD512FA" "x32AB98" "x3D815B" "x50A652" "x61D155" "x7E6BFF" "xBD0969" "xC25969" "xC2AAA3" "xC53E76" "xD3A32E" }
    traits = {}
    city = "xce6546" #Random



















    port = "xb784ff" #Random



















    farm = "x11d353" #Random



















    wood = "xbecf32" #Random









    arable_land = 17
    arable_resources = { bg_livestock_ranches }
    capped_resources = {
        bg_fishing = 6
        bg_whaling = 4
    }
    naval_exit_id = 3110
}

STATE_DALAIREY_WASTES = {
    id = 509
    subsistence_building = "building_subsistence_pastures"
    provinces = { "x04DB73" "x19A814" "x2C2F3C" "x3002FF" "x31C090" "x3BEB2E" "x443E92" "x4A347E" "x4B3C7E" "x6CF948" "x745CF8" "x8018A3" "x828DDF" "x85E56B" "x8D241F" "x90FE7E" "xB04190" "xB07F50" "xC99759" "xD2956F" "xDC67F7" "xF74125" }
    traits = { state_trait_dalairey_wastes } #to prevent early colonization
























    city = "xc99759" #Random



















    port = "x4b3c7e" #Random



















    farm = "xd2956f" #Random



















    wood = "x828ddf" #Random



















    arable_land = 5
    arable_resources = { bg_livestock_ranches }
    capped_resources = {
        bg_fishing = 2
        bg_whaling = 2
        bg_sulfur_mining = 50
    }
    naval_exit_id = 3109
}

STATE_FIORGAM = {
    id = 511
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0C547E" "x105609" "x108820" "x1D00F8" "x3282DE" "x618B1C" "x638267" "x7A6731" "x8A56AD" "x9086AD" "x908A4F" "xA0C2BF" "xA874E1" "xB586AD" "xCA11DD" "xD1953D" "xD571A8" }
    traits = {}
    city = "x0c547e" #Random



















    port = "x9086ad" #Random



















    farm = "x108820" #Random



















    wood = "x908a4f" #Random



















    arable_land = 18
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_tobacco_plantations }
    capped_resources = {
        bg_fishing = 11
        bg_logging = 4
        bg_whaling = 7
    }
    naval_exit_id = 3101
}

STATE_MITTANWEK = {
    id = 512
    subsistence_building = "building_subsistence_pastures"
    provinces = { "x0A6BE6" "x107763" "x1617DD" "x309661" "x30E94A" "x347744" "x3EDD9A" "x42F2E9" "x6CEE91" "xB00090" "xB40258" "xC37F66" "xCB424F" "xE70539" "xE76C62" "xE855B1" "xF68D31" "xFEE834" "x383820" "x39A420" "x5B5500" "x878A7C" "xFD577F" "x1F405F" "x2D0CA4" "x5D0E64" "x672B93" "x690DB5" "x7E59EB" "xD4ABBA" "x2FAE4E" "x35D461" "x6BB828" "x7DF383" "x84176B" "x894B71" "xA5EDD1" "xABCC1E" "x00821E" "x8848C6" "x8B33D9" "x90C050" "xA7AE88" "xF0CA34" "xF2263F" }
    traits = {}
    city = "x894B71" #NEW PLACE









    port = "x2fae4e" #Farplott









    farm = "x35d461" #Arbcyrr









    wood = "xabcc1e" #Inuvuorlak









    mine = "x5D0E64" #Sterngcottopcott









    arable_land = 34
    arable_resources = { bg_livestock_ranches }
    capped_resources = {
        bg_fishing = 1
        bg_logging = 1
    }
    naval_exit_id = 3112
}

STATE_FARPLOTT = {
    id = 517
    subsistence_building = "building_subsistence_pastures"
    provinces = { "x1A5ED2" "x0F1339" "x2B18E0" "x43DAD0" "x4A0914" "x6D25DC" "x788B48" "x807115" "x87DFC3" "x8DF5C5" "x97865C" "xFC2A57" "x0078F4" "x21A4C8" "x31F03F" "x5249FC" "x604D15" "x6ED74B" "x93EF4F" "x9DA2FB" "xA0A058" "xB72711" "xDC5D80" "xF8B9BC" }
    traits = {}
    city = "x0078f4" #Random



















    port = "x21a4c8" #Random



















    farm = "xa0a058" #Random



















    wood = "xb72711" #Random









    arable_land = 15
    arable_resources = { bg_livestock_ranches }
    capped_resources = {
        bg_fishing = 1
        bg_logging = 1
    }
    naval_exit_id = 3114
}

STATE_GRACOST = {
    id = 518
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0700F1" "x393FDF" "x3CBC24" "x7939FB" "xAA6512" "xBB5A4F" "xBB5EAD" "x0F72F3" "x1C93E4" "x35839B" "x3C651C" "x3F05E8" "x555B2E" "x71CAD8" "x7C41A8" "x9806EE" "xD23718" "xEB348A" }
    traits = {}
    city = "xbb5ead" #Random



















    port = "x393fdf" #Random



















    farm = "xbb5a4f" #Random



















    wood = "xaa6512" #Random









    arable_land = 31
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_fishing = 1
        bg_logging = 1
    }
    naval_exit_id = 3113
}

STATE_GROONCAMB = {
    id = 520
    subsistence_building = "building_subsistence_farms"
    provinces = { "x34847E" "x66409A" "x7E1301" "xB4824F" "xBE8BB1" "xD92880" "xE8923D" "xE99E3B" }
    traits = {}
    city = "xb4824f" #Random



















    port = "x66409a" #Random



















    farm = "xd92880" #Random



















    wood = "x34847e" #Random



















    arable_land = 33
    arable_resources = { bg_wheat_farms bg_livestock_ranches bg_tobacco_plantations }
    capped_resources = {
        bg_fishing = 4
        bg_logging = 12
        bg_whaling = 2
        bg_coal_mining = 28
    }
    naval_exit_id = 3109
}

STATE_CERRICK = {
    id = 521
    subsistence_building = "building_subsistence_farms"
    provinces = { "x1BC602" "x2C42EF" "x358C7E" "x359020" "x517BD1" "x6F83AD" "x907631" "x9BF036" "xB58EAD" "xEBCAEF" "xEE2A37" "xEE7497" "xFF82AD" }
    traits = {}
    city = "xebcaef" #Random



















    farm = "x9bf036" #Random



















    wood = "x359020" #Random



















    arable_land = 47
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 14
        bg_iron_mining = 16
    }
}

STATE_SILDGEELDROY = {
    id = 522
    subsistence_building = "building_subsistence_farms"
    provinces = { "x05D45E" "x264684" "x273D46" "x362CB9" "x36947E" "x48265B" "x522FE7" "x693145" "x869250" "x989414" "xB47EAD" "xB5924F" "xB736C4" "xC3E78C" "xCE90FF" "xD63E8B" "xEFC180" }
    traits = {}
    city = "x693145" #Random



















    farm = "xb5924f" #Random



















    wood = "x362cb9" #Random



















    arable_land = 16
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 18
        bg_iron_mining = 14
    }
}
