﻿pm_basic_mage_academy = {
	texture = "gfx/interface/icons/production_method_icons/basic_mage_academy.dds"
	# state_pollution_generation_add = 10
	
	building_modifiers = { 
		workforce_scaled = {
			goods_input_paper_add = 5
		}

		level_scaled = {
			building_employment_laborers_add = 2000
			building_employment_clerks_add = 2000
			building_employment_mages_add = 900
		}
	}
}

pm_advanced_mage_academy = {
	texture = "gfx/interface/icons/production_method_icons/advanced_mage_academy.dds"
	# state_pollution_generation_add = 50
	
	unlocking_technologies = {
		empiricism
	}				
	
	building_modifiers = { 
		workforce_scaled = {
			goods_input_paper_add = 10
			goods_input_porcelain_add = 15
		}

		level_scaled = {
			building_employment_laborers_add = 1500
			building_employment_clerks_add = 2500
			building_employment_mages_add = 900
		}
	}
}

pm_more_advanced_mage_academy = {
	texture = "gfx/interface/icons/production_method_icons/more_advanced_mage_academy.dds"
	# state_pollution_generation_add = 75
	
	unlocking_technologies = {
		dialectics
	}
	
	building_modifiers = { 
		workforce_scaled = {
			goods_input_paper_add = 15
			goods_input_porcelain_add = 30
		}

		level_scaled = {
			building_employment_laborers_add = 1000
			building_employment_clerks_add = 3000
			building_employment_mages_add = 900
		}
	}
}		

pm_no_damestear = {
	texture = "gfx/interface/icons/production_method_icons/no_damestear.dds"
}

pm_damestear_mage_academy = {
	texture = "gfx/interface/icons/production_method_icons/damestear.dds"
	
	building_modifiers = {
		workforce_scaled = {
			goods_input_damestear_add = 20
		}
	}
}

pm_independent_mages = {
	texture = "gfx/interface/icons/production_method_icons/independent_mages.dds"
	
	disallowing_laws = {
		law_command_economy
	}

	building_modifiers = {
		level_scaled = {
			building_employment_mages_add = 100
		}
		unscaled = {
			building_mages_shares_add = 5
		}
	}
}

pm_government_mages = {
	texture = "gfx/interface/icons/production_method_icons/government_mages.dds"

	building_modifiers = {
		level_scaled = {
			building_employment_bureaucrats_add = 100
		}
		unscaled = {
			building_bureaucrats_shares_add = 5
		}
	}
}

pm_religious_mages = {
	texture = "gfx/interface/icons/production_method_icons/religious_mages.dds"
	
	disallowing_laws = {
		law_command_economy
		law_total_separation
	}

	building_modifiers = {
		level_scaled = {
			building_employment_clergymen_add = 100
		}
		unscaled = {
			building_clergymen_shares_add = 5
		}
	}
}