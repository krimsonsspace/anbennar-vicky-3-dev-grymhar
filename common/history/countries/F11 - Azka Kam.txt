﻿COUNTRIES = {
	c:F11 = {
		effect_starting_technology_tier_2_tech = yes
		
		activate_law = law_type:law_monarchy
		activate_law = law_type:law_autocracy
		activate_law = law_type:law_national_supremacy
		activate_law = law_type:law_state_religion
		activate_law = law_type:law_appointed_bureaucrats
		
		activate_law = law_type:law_land_based_taxation
		#activate_law = law_type:law_religious_schools
		
		activate_law = law_type:law_censorship
		activate_law = law_type:law_women_own_property #Women in workplace not possible without feminism
		activate_law = law_type:law_migration_controls
		
		activate_law = law_type:law_same_race_only
		activate_law = law_type:law_dark_arts_banned
		activate_law = law_type:law_amoral_artifice_banned
		activate_law = law_type:law_traditional_magic_encouraged
	}
}