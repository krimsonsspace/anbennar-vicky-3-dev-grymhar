﻿CHARACTERS = {
	c:B89 = {
		create_character = {
			first_name = "Avery"
			last_name = "Fischer"
			historical = yes
			ruler = yes
			age = 62
			interest_group = ig_rural_folk
			ig_leader = yes
			ideology = ideology_moderate
			traits = {
				senile demagogue
			}
		}
	}
}
